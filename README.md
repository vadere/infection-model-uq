# Vadere infection model - uncertainty quantification

This project collects a series of numerical experiments in which methods of 
uncertainty quantification (UQ), that is forward propagation and global 
sensitivity analysis, are applied to the infection model in 
[Vadere](https://gitlab.lrz.de/vadere/vadere). The algorithm is designed for 
Vadere, version 2.2 (and higher), but it can also be applied to other models.
The sampling techniques and the analysis of model outputs are based on 
[SALib](https://salib.readthedocs.io/en/latest/). For evaluating the model, this
project embeds the Surrogate Model and Uncertainty Quantification Controller 
([SUQ-Controller](https://gitlab.lrz.de/vadere/suq-controller "SUQ-Controller")), 
which facilitates running the Vadere model in parallel.


## Alternative UQ frameworks for Vadere
The following projects implement more UQ methods and provide comfortable 
analysis tools, but they are partly incompatible with the latest development of 
Vadere or the SUQ-Controller. Also, the EMAworkbench may not support all 
data processors implemented in Vadere:
* Vadere [uncertainty quantification framework](https://gitlab.lrz.de/vadere/uncertainty-quantification):
  designed for global sensitivity analysis, inversion methods, and forward 
  propagation; tested for python 3.7
* [EMAworkbench](https://github.com/quaquel/EMAworkbench/pull/145): Vadere 
  connector, tested on Linux

## Usage
The package is designed for forward propagation and sensitivity analysis.
Inverse methods are best applied with Vadere's [UQ framework](https://gitlab.lrz.de/vadere/uncertainty-quantification).
The process for forward UQ and sensitivty analysis is visualized in the 
following figure. 

![UQ Process](resources/process.png)

Both `run_experiment.py` and `run_analysis.py` can be executed from the command
line.
The entrypoint for running an experiment, that is evaluating the model, is 
`run_experiment.py`.
The analysis can be conducted as a separate step.

Alternatively, the main routines can be called from Python scripts, which
contain the definition of numerical experiments.
To keep the numerical experiments organized, it is recommended to adhere to one
directory layout.

### Directory layout
The model files, definition of experiments, etc. are organized as follows: 

<pre>
.
├── suq-controller              # git submodule
├── vadere                      # git submodule, contains the vadere.jar if not defined otherwise
├── vimuq                       # source code for vadere infection model uncertainty quantification
└── experiments
    ├── analysis                # Defines the analysis of the experiments
    │    ├── <var>EXPERIMENT_XY</var>.py   # Can be run from terminal or IDE 
    │    └── ...  
    ├── definition              # Definition of numerical experiments
    │    ├── <var>EXPERIMENT_XY</var>.py   # Can be run from terminal or IDE 
    │    └── ...  
    ├── model                   # Add alternative models here
    │   └── vadere              # Vadere model (consists of a `.jar` and a `.scenario` file)
    │       ├── scenarios       # Scenario files
    │       ├── simulator       # Vadere console JAR files, e.g., `vadere-console.jar`
    │       └── vadere.project  # Vadere project file (optional)
    └── output/<var>EXPERIMENT_XY</var>/   # Output for each numerical experiment
        ├── figures             # Figures created when the experiment is evaluated/analyzed
        ├── scenario            # Contains a copy of the scenario file (this should not be altered)
        ├── simulation          # (Raw) simulation data
        └── summary             # Dumped (`.pkl`) experiment setup, processed simulation data, analysis
                                # results, and meta data
</pre>




### Supported settings, model parameters and output formats
#### Input parameter distributions
- Uniform
- Normal
- Truncated normal
- Log-normal (denoted in ln-space)

#### UQ-Methods
- Forward UQ
  - Monte Carlo
- Sensitivity analysis
  - Sobol' sensitivity indices (with SALib's implementation of the Sobol'
    sequence)
    
#### Vadere model output / data keys
- Scalar (noDataKey)
- Time series (timeStepKey)
- Other array outputs, e.g., per pedestrian (pedIdKey)
- Other multi-dimensional outputs, e.g., per pedestrian and time step
  (timeStepPedIdKey)

## System setup

### Requirements
* Python~=3.8.2
* See requirements.txt

### 1. Clone the project including submodules
```
git clone git@gitlab.lrz.de:vadere/infection-model-uq.git
git submodule update --init --recursive
```

### 2. Set up a virtual environment
#### On Windows
```
cd infection-model-uq
python3 -m venv .venv
.venv\Scripts\activate
python3 -m pip install --upgrade pip
```
The virtual environment should now be active, i.e. `where python` should return 
`C:\...\infection-model-uq\venv\Scripts\python.exe`.

#### On Ubuntu
See [here](#Installation-instructions-for-Ubuntu-22.04) how to set up python3.8
on ubuntu 22.04. If python is installed correctly, create a virtual environment:

Verify that pip is installed
```
python3.8 -m pip --version
```

If not installed
```
sudo apt install python3.8
sudo apt install python3.8-distutils

wget https://bootstrap.pypa.io/get-pip.py
sudo python3.8 get-pip.py
```

Install venv package
```
sudo apt install python3.8-venv
```

Create virtual environment
```
cd path/to/infection-model-uq
python3.8 -m venv .venv
```

Activate the virtual environment
```
cd path/to/infection-model-uq
source .venv/bin/activate

pip install --upgrade pip
```

(Deactivate venv after running experiments)
```
deactivate
```

### 3. Install vimuq in development mode
<pre>
pip3 install -e .
</pre>

### 4. Install requirements
This should automatically install the correct versions of the dependencies,
e.g., of the suq-controller. There is no need to check out a specific commit of
the submodules (vadere and suq-controller) because they get updated
together with the project.
<pre>
pip3 install -r requirements.txt
</pre>

### 5. Handling the submodules suqc and Vadere
Install suqc in development mode:
<pre>
pip3 install -e .\suq-controller
</pre>

Build Vadere (further
[information](https://gitlab.lrz.de/vadere/vadere#build-instructions)):
<pre>
cd vadere
mvn clean
mvn -Dmaven.test.skip=true package
</pre>

## Continuous integration and testing

### Coverage
Get the coverage report:
<pre>
coverage run -m --data-file .\tools\report\.coverage unittest discover -v
coverage html --omit "*tests*","*suq-controller*" --skip-empty -d .\tools\report --data-file .\tools\report\.coverage
</pre>
Open `.\tools\report\index.html`

### Pre-commit
If you want to contribute, please use [pre-commit](https://pre-commit.com/):
<pre>
pip3 install pre-commit
pre-commit install
</pre>

## Installation instructions for Python 3.8 on Ubuntu 22.04
See [how2shout](https://www.how2shout.com/linux/install-python-3-9-or-3-8-on-ubuntu-22-04-lts-jammy-jellyfish/) for more information.

Start with the system update
```
sudo apt update && sudo apt upgrade
```

Add PPA for Python old versions
```
sudo apt install software-properties-common
sudo add-apt-repository ppa:deadsnakes/ppa
```

Check Python Versions you want
```
sudo apt-cache policy python3.8
```

Install Python 3.8 on Ubuntu 22.04
```
sudo apt install python3.8
```

(Set the default Python version)
Check what python versions are available on your systems:
```
ls /usr/bin/python*
```
To know whether any version is configured as python alternatives or not. For
that run:
```
sudo update-alternatives --list python
```

If the output is "update-alternatives: error: no alternatives for python"
```
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3.8 1
```

else switch the default Python version
```
sudo update-alternatives --config python
```

check python version
```
python -V
```

## Authors and acknowledgment
Simon Rahn

## License
This software is licensed under the GNU Lesser General Public License.
