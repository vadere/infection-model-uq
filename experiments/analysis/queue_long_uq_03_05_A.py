#!/usr/bin/env python3

from imports import relPath2output

from vimuq.experiments.analysis import run_analysis

argv = [
    "--experimentid queue_long_uq_03_05",
    "--outputpath {}".format(relPath2output),
    "--quantitiesofinterest "
    "pedIdKey_degExposure "
    "pedIdKey_degExposure_sum "
    "pedIdKey_degExposure_max "
    "pedIdKey_degExposure_rank "
    "pedIdKey_degExposure_count_>_0 "
    "pedIdKey_degExposure_mean_>_0",
    "--averagerepetitions A",
    "--applyfunction pedIdKey_degExposure sum id",
    "--applyfunction pedIdKey_degExposure max id",
    "--applyfunction pedIdKey_degExposure rank id",
    "--applyfunction pedIdKey_degExposure count_>_0 id",
    "--applyfunction pedIdKey_degExposure mean_>_0 id",
]

if __name__ == "__main__":
    run_analysis.main(argv)
