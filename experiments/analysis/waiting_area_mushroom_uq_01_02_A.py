#!/usr/bin/env python3

# Important note:
# It is debatable if "--averagerepetitions A" makes sense in this case because
# the simulation results can vary greatly for each pedestrian and simulation
# seed. I think that this setting makes sense, if at all, for quantities which
# consider the total or average exposure of all pedestrians. Therefore, the
# "individual" quantities are commented out.

from imports import relPath2output

from vimuq.experiments.analysis import run_analysis

argv = [
    "--experimentid waiting_area_mushroom_uq_01_02",
    "--outputpath {}".format(relPath2output),
    "--quantitiesofinterest "
    # "pedIdKey_degExposure "
    "pedIdKey_degExposure_sum "
    # "pedIdKey_degExposure_max "
    # "pedIdKey_degExposure_rank "
    "pedIdKey_degExposure_count_>_0 " "pedIdKey_degExposure_mean_>_0",
    "--averagerepetitions A",
    "--applyfunction pedIdKey_degExposure sum id",
    # "--applyfunction pedIdKey_degExposure max id",
    # "--applyfunction pedIdKey_degExposure rank id",
    "--applyfunction pedIdKey_degExposure count_>_0 id",
    "--applyfunction pedIdKey_degExposure mean_>_0 id",
]

if __name__ == "__main__":
    run_analysis.main(argv)
