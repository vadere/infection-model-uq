#!/usr/bin/env python3

from imports import relPath2output

from vimuq.experiments.analysis import run_analysis

argv = [
    "--experimentid waiting_area_mushroom_uq_01_03",
    "--outputpath {}".format(relPath2output),
    "--quantitiesofinterest "
    "pedIdKey_degExposure "
    "pedIdKey_degExposure_sum "
    "pedIdKey_degExposure_max "
    "pedIdKey_degExposure_rank "
    "pedIdKey_degExposure_count_>_0 "
    "pedIdKey_degExposure_mean_>_0",
    "--averagerepetitions D",
    "--applyfunction pedIdKey_degExposure sum id",
    "--applyfunction pedIdKey_degExposure max id",
    "--applyfunction pedIdKey_degExposure rank id",
    "--applyfunction pedIdKey_degExposure count_>_0 id",
    "--applyfunction pedIdKey_degExposure mean_>_0 id",
    # checked already by waiting_area_mushroom_uq_01_03_C
    # "--controlqoi pedIdKey_pedEndTime",
    # "--checkendtimes True",
    # "--checkservicetimes 90 0.05",
]

if __name__ == "__main__":
    run_analysis.main(argv)
