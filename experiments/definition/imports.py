import os.path

relPath2experiment = ".."
relPath2simulator = os.path.join(relPath2experiment, "model", "vadere", "simulator")
relPath2scenarios = os.path.join(relPath2experiment, "model", "vadere", "scenarios")
relPath2output = os.path.join(relPath2experiment, "output")
