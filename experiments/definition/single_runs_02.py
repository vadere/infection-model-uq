# Defines the evaluation of several scenarios for parameter combinations:
# - mean values of uncertain ranges of the input parameters
# - worst case parameter combinations
# This is not a real UQ experiment.
#
# In contrast to singe_runs_01, this experiment uses the same settings but, the
# pathogen load is is altered and set to a "real" value. The scenario files are
# the same, but the pathogen load is altered by suqc_post_changes.

import os.path
import shutil
import pickle as pkl

from suqc import PostScenarioChangesBase

from imports import relPath2output, relPath2scenarios, relPath2simulator
from vimuq.util.postchanges_helper import ChangeInitialPathogenLoad
from vimuq.uq.model import VadereModel
from vimuq.uq.parameter_space import ParameterSpace
from vimuq.uq.uq_experiment import UQExperiment
from vimuq.util.git import get_git_revision_hash

if __name__ == "__main__":

    # input
    vadere_console_path = os.path.join(relPath2simulator, "vadere-console_899fffc3.jar")

    # scenario name and corresponding number of repetitions
    scenarios = {
        "single_run_close_contact.scenario": 1,
        "single_run_queue_long.scenario": 1,
        "single_run_queue_long_distancing.scenario": 1,
        "single_run_waiting_area_line.scenario": 1,
        "single_run_waiting_area_line_distancing.scenario": 1,
        "single_run_waiting_area_block.scenario": 1,
        "single_run_waiting_area_block_distancing.scenario": 1,
        # here, we need more repetitions because the trajectories are not as
        # pre-defined as in the other scenarios, i.e. peds can move freely.
        # Consequently, the output varies depending on the seed and in the post
        # processing we take the average result.
        "single_run_waiting_area_mushroom.scenario": 10,
        "single_run_waiting_area_queueing.scenario": 10,  # dyn. floor field!
        "single_run_walkway_wide.scenario": 1,
        "single_run_walkway_single_file.scenario": 1,
    }

    for scenario, n_repetitions in scenarios.items():
        # input
        scenario_path = os.path.join(relPath2scenarios, scenario)

        # output
        filename = os.path.basename(__file__)
        experiment_out_dir = filename.split(".")[0]
        output_dir = os.path.join(relPath2output, experiment_out_dir)
        scenario_output_path = os.path.join(output_dir, scenario.split(".")[0])

        # sub-folders and files of scenario_output_path
        # 1. log file
        log_file = os.path.join(scenario_output_path, ".log")
        # 2. copied scenario file
        scenario_copy_path = os.path.join(
            scenario_output_path, UQExperiment.SCENARIO_SUBDIR
        )
        # 3. subdir for simulation results
        scenario_summary_path = os.path.join(scenario_output_path, UQExperiment.SUBDIR)
        results_file_path = os.path.join(scenario_summary_path, "result.pkl")
        meta_info_file_path = os.path.join(scenario_summary_path, "meta_info.pkl")

        # make target and all intermediate dirs, trow error if target dir exists
        os.makedirs(scenario_copy_path, exist_ok=False)
        os.makedirs(scenario_summary_path, exist_ok=False)

        # VadereModel() requires an input, but for this simulation we need no
        # further information;
        dummy_param_space = ParameterSpace()

        # Parameter variations:
        param_var = []

        # Mean values for ranges of uncertainty
        # half life:        600 ... 10000
        # initial radius:   0.5 ... 1.5
        # air disp. factor: 0   ... 0.006
        param_mean = {
            "halfLife": 5300.0,
            "initialRadius": 1.0,
            "airDispersionFactor": 0.003,
        }
        param_var.append(param_mean)

        # Expected extreme cases
        # spatially compact spread -> few agents but high concentrations
        param_extreme_case_1 = {
            "halfLife": 10000.0,
            "initialRadius": 0.5,
            "airDispersionFactor": 0,
        }
        param_var.append(param_extreme_case_1)

        # spatially wide spread -> large area / many agents affected
        param_extreme_case_2 = {
            "halfLife": 10000.0,
            "initialRadius": 1.5,
            "airDispersionFactor": 0.006,
        }
        param_var.append(param_extreme_case_2)

        # Define quantities of interest
        if scenario in ["single_run_close_contact.scenario"]:
            qoi = ["timeStepPedIdKey_degExposure"]
        else:
            qoi = ["pedIdKey_degExposure"]

        # this defines changes to the scenario that are not part of the sampling
        initialPathogenLoadPostChange = ChangeInitialPathogenLoad(
            init_pathogen_load=1000.0
        )
        suqc_post_changes = PostScenarioChangesBase(apply_default=True)
        suqc_post_changes.add_scenario_change(initialPathogenLoadPostChange)

        vm = VadereModel(
            parameter_space=dummy_param_space,
            quantities_of_interest=qoi,
            path2model=vadere_console_path,
            path2scenario=scenario_path,
            run_local=True,
            repetitions=n_repetitions,
            suqc_post_changes=suqc_post_changes,
        )

        # this is important, otherwise we get the same seed/result for each run
        vm.apply_suqc_rand_number_per_repetition()

        meta_info, model_output = vm.evaluate(
            model_input=param_var, path2output=scenario_output_path
        )

        # Store simulation results
        with open(results_file_path, "wb") as outp:
            pkl.dump(model_output, outp, pkl.HIGHEST_PROTOCOL)

        # Store meta info
        # information about seeds is not required, because the seeds are not
        # altered. Seed is defined in the scenario file
        with open(meta_info_file_path, "wb") as outp:
            pkl.dump(meta_info, outp, pkl.HIGHEST_PROTOCOL)

        # copy scenario
        shutil.copy(scenario_path, scenario_copy_path)

    # save commit hash to info file
    cwd = os.getcwd()
    path = os.path.join(*cwd.split("\\")[-3:], filename)
    info_file = os.path.join(output_dir, "info.txt")
    with open(info_file, "w") as f:
        f.write(
            f"Simulation results created with\n"
            f"{path}\n"
            f"commit hash: {get_git_revision_hash()}"
        )
