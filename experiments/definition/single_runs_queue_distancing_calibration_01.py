# Defines the evaluation of several scenarios for one single (fixed) parameter
# combination. That is, this is not a real UQ experiment. This is just used to
# simulate the defined scenarios several times (n_repetitions) with different
# seeds.
# The exposure model is actually not of interest here. These simulations are
# used to calibrating the distancing behavior in the queue scenario.

import os.path
import shutil
import pickle as pkl

from imports import relPath2output, relPath2scenarios, relPath2simulator
from vimuq.uq.model import VadereModel
from vimuq.uq.parameter_space import ParameterSpace
from vimuq.uq.uq_experiment import UQExperiment
from vimuq.util.git import get_git_revision_hash

if __name__ == "__main__":

    # input
    vadere_console_path = os.path.join(relPath2simulator, "vadere-console_899fffc3.jar")

    # scenario name and corresponding number of repetitions
    scenarios = {
        "single_run_queue_long.scenario": 10,
        "single_run_queue_long_distancing.scenario": 10,
    }

    for scenario, n_repetitions in scenarios.items():
        # input
        scenario_path = os.path.join(relPath2scenarios, scenario)

        # output
        filename = os.path.basename(__file__)
        experiment_out_dir = filename.split(".")[0]
        output_dir = os.path.join(relPath2output, experiment_out_dir)
        scenario_output_path = os.path.join(output_dir, scenario.split(".")[0])

        # sub-folders and files of scenario_output_path
        # 1. log file
        log_file = os.path.join(scenario_output_path, ".log")
        # 2. copied scenario file
        scenario_copy_path = os.path.join(
            scenario_output_path, UQExperiment.SCENARIO_SUBDIR
        )
        # 3. subdir for simulation results
        scenario_summary_path = os.path.join(scenario_output_path, UQExperiment.SUBDIR)
        results_file_path = os.path.join(scenario_summary_path, "result.pkl")
        meta_info_file_path = os.path.join(scenario_summary_path, "meta_info.pkl")

        # make target and all intermediate dirs, trow error if target dir exists
        os.makedirs(scenario_copy_path, exist_ok=False)
        os.makedirs(scenario_summary_path, exist_ok=False)

        # VadereModel() requires an input, but for this simulation we need no
        # further information;
        dummy_param_space = ParameterSpace()

        # Parameter variation (this is actually only a dummy parameter variation
        # since, in this experiment, we are not interested in the exposure of
        # pedestrians)
        param_mean = {
            "halfLife": 5300.0,
            "initialRadius": 1.0,
            "airDispersionFactor": 0.003,
        }
        param_var = [param_mean]

        # Define quantities of interest
        qoi = ["pedIdKey_degExposure"]

        vm = VadereModel(
            parameter_space=dummy_param_space,
            quantities_of_interest=qoi,
            path2model=vadere_console_path,
            path2scenario=scenario_path,
            run_local=True,
            repetitions=n_repetitions,
        )
        vm.apply_suqc_rand_number_per_repetition()

        meta_info, model_output = vm.evaluate(
            model_input=param_var, path2output=scenario_output_path
        )

        # store simulation results
        with open(results_file_path, "wb") as outp:
            pkl.dump(model_output, outp, pkl.HIGHEST_PROTOCOL)

        # store information about seeds and other meta info
        meta_info.insert(loc=0, column="simulationSeed", value=vm.repetitions_seeds)
        with open(meta_info_file_path, "wb") as outp:
            pkl.dump(meta_info, outp, pkl.HIGHEST_PROTOCOL)

        # copy scenario
        shutil.copy(scenario_path, scenario_copy_path)

    # save commit hash to info file
    cwd = os.getcwd()
    path = os.path.join(*cwd.split("\\")[-3:], filename)
    info_file = os.path.join(output_dir, "info.txt")
    with open(info_file, "w") as f:
        f.write(
            f"Simulation results created with\n"
            f"{path}\n"
            f"commit hash: {get_git_revision_hash()}"
        )
