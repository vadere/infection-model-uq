#!/usr/bin/env python3

# UQ study for queue scenario with three uncertain parameters:
# pedestrianDispersionWeight = 0 (fixed/deactivated)
# pedestrianRespiratoryCyclePeriod = 3 (fixed)
# aerosolCloudParameters.absorptionRate = 5 * 10^-4 (fixed)
#
# This corresponds to the queue_long_uq_03_03 experiment but
# - reduced sample size
# - different scenario
# - no second order sensitivity indices

import os.path

from imports import relPath2output, relPath2scenarios, relPath2simulator

from vimuq.experiments.definition import run_experiment

# vadere-console_%commit hash%.jar; commit hash indicates vadere version
vadere_console_path = os.path.join(relPath2simulator, "vadere-console_899fffc3.jar")
scenario_path = os.path.join(relPath2scenarios, "waiting_area_block_uq_01.scenario")

filename = os.path.basename(__file__)
experiment_id = filename.split(".")[0]

argv = [
    f"--experimentid {experiment_id}",
    "--model vadere",
    f"--modelfile {vadere_console_path}",
    f"--scenario {scenario_path}",
    "--parameter halfLife UniformDistribution 600 10000",
    "--parameter initialRadius UniformDistribution 0.5 1.5",
    "--parameter airDispersionFactor UniformDistribution 0 0.006",
    "--changeinitpathogenload 1000.0",
    "--output pedIdKey_degExposure",
    "--output pedIdKey_pedEndTime",
    f"--outputpath {relPath2output}",
    "--runlocal True",
    "--repetitions 1",
    "--uqmethod SALibSaltelliSobolSequence",
    "--samplesize 4096",  # power of 2 for better performance of sobol sequence
    "--secondorder False",
]

if __name__ == "__main__":
    run_experiment.main(argv)
