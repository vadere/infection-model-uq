from typing import Union
import matplotlib.pyplot as plt

import numpy as np


def find_nearest(array: np.array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx], idx


def pathogen_concentration(
    time: Union[float, np.array],
    half_life: float,
    init_radius: float,
    dispersion_factor: float,
    init_pathogen_load: float = 1000.0,
):

    init_volume = aerosol_cloud_volume(
        time=0, init_radius=init_radius, dispersion_factor=dispersion_factor
    )
    init_pathogen_load = pathogen_load(
        time=0, init_pathogen_load=init_pathogen_load, half_life=half_life
    )
    init_concentration = init_pathogen_load / init_volume
    lifetime_end_concentration = 0.01 * init_concentration

    volume = aerosol_cloud_volume(
        time=time, init_radius=init_radius, dispersion_factor=dispersion_factor
    )
    load = pathogen_load(
        time=time, init_pathogen_load=init_pathogen_load, half_life=half_life
    )
    concentration = load / volume

    if isinstance(concentration, np.ndarray):
        nearest, idx = find_nearest(
            array=concentration, value=lifetime_end_concentration
        )
        if nearest >= 0:
            idx += 1
        concentration[idx:] = np.NaN
        life_time = time[idx]
        max_radius = init_radius + dispersion_factor * life_time

    else:
        if concentration < lifetime_end_concentration:
            concentration = np.NaN
        life_time = np.NaN
        max_radius = np.NaN

    return concentration, init_concentration, life_time, max_radius


def aerosol_cloud_volume(time, init_radius, dispersion_factor):
    radius = dispersion_factor * time + init_radius
    return 4.0 / 3.0 * np.pi * radius**3


def pathogen_load(time, init_pathogen_load, half_life):
    return init_pathogen_load * np.exp(-np.log(2) / half_life * time)  # np.log = ln


if __name__ == "__main__":
    time = np.arange(start=0, stop=10**5, step=1)

    half_life = [5.3 * 10**3, 10**4, 10**4, 10**4]
    init_radius = [1.0, 0.5, 0.5, 1.5]
    dispersion_factor = [0.003, 0, 0.006, 0.006]

    for hl, ir, df in zip(half_life, init_radius, dispersion_factor):
        c, ipc, lt, r = pathogen_concentration(
            time=time,
            half_life=hl,
            init_radius=ir,
            dispersion_factor=df,
            init_pathogen_load=1000,
        )
        parameter_variation = f"HL={hl}, IR={ir}, DF={df}"
        plt.plot(time, c, label=parameter_variation)
        print(parameter_variation)
        print(f"Init. particle concentration:\t{ipc} p/m^3")
        print(f"Lifetime:\t\t\t\t\t\t{lt} s")
        print(f"Max. radius:\t\t\t\t\t{r} m")
        print("\n")
    plt.legend()
    plt.show()

    initial_radius = np.linspace(start=0.5, stop=1.5, endpoint=True, num=10)
    lt = np.zeros(len(initial_radius))
    r = np.zeros(len(initial_radius))
    for i, ir in enumerate(initial_radius):
        _, lt[i], r[i] = pathogen_concentration(
            time=time,
            half_life=10**4,
            init_radius=ir,
            dispersion_factor=0.006,
            init_pathogen_load=1000,
        )

    plt.plot(initial_radius, lt)
    plt.xlabel("initial radius")
    plt.ylabel("lifetime")
    plt.show()

    plt.plot(initial_radius, r)
    plt.xlabel("initial radius")
    plt.ylabel("max. radius")
    plt.show()
