# Vadere scenarios

## Base scenarios for UQ studies
Suffix: "uq"

These scenarios are optimized for many model evaluations (short run time,
parametrized topography, etc.). However, they are just the basis from which one
can derive a scenario for a numerical experiment because they contain all 
data processors that any of the numerical experiments applies. In addition, they
contain postvis data for pedestrian trajectories, aerosol clouds, health status,
etc. 

* **queue_uq**: the queue scenario but with efficient optimizationType
  'DISCRETE' and adapted / simplified topography. These optimizations affect the 
  locomotion behavior.

* **queue_long_uq**: in principle same as the queue_uq scenario but with a 
  longer corridor.
  
## Scenarios for numerical experiments
Any scenario with a trailing number is part of a numerical experiment. Altering
the scenarios in this vadere project is allowed. When a numerical experiment is
executed, the scenario is copied to the output folder to allow for reproducing
the experiment at a later point in time.

Steps to create a scenario for a numerical experiment:
1. Create a base scenario (if the base scenario already exists, just add the 
   data processors and output you need)
2. Run the base scenario with all necessary data processors (including the ones
   for the postvis)
3. Copy and rename the base scenario; maybe adapt some parameters
4. Remove all unnecessary data output / processors
5. Run the scenario once to make sure it really works
6. Use it for a numerical UQ experiment

Important notes:
* Use fixed seed is true
* Maybe use discrete optimization type (instead of nelder mead) for faster model 
  evaluations
* Stick to the naming convention for data outputs
* Do not forget to invoke a control model output, e.g. pedestrian end time. This
  allows to check easily if all simulations ran successfully.

Most important differences between the queue scenarios:

| scenario          | agents (#S, #I, #S)   | waiting time  | topography    | note  |
|------------------ |---------------------: | -------------:| ------------: |------ |
| queue_uq_01       | 4, 1, 5               | 120           | 25 x 1.8      |       |
| queue_long_uq_01  | 26, 1, 24             | 40            | 50 x 1.8      |       |
| queue_long_uq_02  | 10, 1, 10             | 90            | 65 x 1.8      | fewer agents and larger topography allows to apply social distancing measures |
| queue_long_uq_03  | 10, 1, 10             | 90            | 65 x 1.8      | exposure model parameters deviate from default |

## Tutorial / simple examples
Suffix: "example"

Scenarios used exemplary for UQ experiments, but not for real analyses.

## Scenarios as a pre study for UQ analyses
The scenarios with prefix "single_run" are used for a pre study of thorough UQ studies.

## Scenarios for testing
Prefix: "test"
* **test_queue**: The 'original' queue scenario but with different model outputs (timeStepPedestrianIdKey, timeStepKey, pedestrianIdKey, noDataKey). The UQ algorithm can be tested using these outputs.
* **test_queue_uq**: Same outputs as in the test_queue.scenario but optimized parameters and topography as in the queue_uq.scenario

# Original scenarios
* **queue**: "original" queue scenario that has been created for a use case study / parameter study before more elaborate UQ methods came into play. This is not optimized for and therefore not used for UQ experiments.
