{
  "name" : "single_run_waiting_area_block",
  "description" : "This scenario corresponds to the queue_long_uq_03.scenario and single_run_queue_long.scenario. It represents an alternative way of how to serve a given number of agents, each one with the same service time.\nThe horizontal distance between the waiting positions (seats) corresponds to the average distance in the queue scenarios if no distancing measure is applied.",
  "release" : "2.2",
  "commithash" : "899fffc323944765bac02d8556dcd517ccc67b85",
  "processWriters" : {
    "files" : [ {
      "type" : "org.vadere.simulator.projects.dataprocessing.outputfile.EventtimePedestrianIdOutputFile",
      "filename" : "postvis.traj",
      "processors" : [ 1, 2, 7 ]
    }, {
      "type" : "org.vadere.simulator.projects.dataprocessing.outputfile.TimestepPedestrianIdOutputFile",
      "filename" : "timeStepPedIdKey_degExposure.txt",
      "processors" : [ 5 ]
    }, {
      "type" : "org.vadere.simulator.projects.dataprocessing.outputfile.PedestrianIdOutputFile",
      "filename" : "pedIdKey_degExposure.txt",
      "processors" : [ 8 ]
    }, {
      "type" : "org.vadere.simulator.projects.dataprocessing.outputfile.TimestepIdDataOutputFile",
      "filename" : "aerosolCloudData.txt",
      "processors" : [ 12 ]
    }, {
      "type" : "org.vadere.simulator.projects.dataprocessing.outputfile.PedestrianIdOutputFile",
      "filename" : "pedIdKey_pedEndTime.txt",
      "processors" : [ 13 ]
    } ],
    "processors" : [ {
      "type" : "org.vadere.simulator.projects.dataprocessing.processor.FootStepProcessor",
      "id" : 1
    }, {
      "type" : "org.vadere.simulator.projects.dataprocessing.processor.FootStepTargetIDProcessor",
      "id" : 2
    }, {
      "type" : "org.vadere.simulator.projects.dataprocessing.processor.PedestrianDegreeOfExposureProcessor",
      "id" : 5
    }, {
      "type" : "org.vadere.simulator.projects.dataprocessing.processor.FootStepHealthStatusProcessor",
      "id" : 7
    }, {
      "type" : "org.vadere.simulator.projects.dataprocessing.processor.PedestrianMaxDegreeOfExposureProcessor",
      "id" : 8
    }, {
      "type" : "org.vadere.simulator.projects.dataprocessing.processor.AerosolCloudDataProcessor",
      "id" : 12,
      "attributesType" : "org.vadere.state.attributes.processor.AttributesAerosolCloudDataProcessor",
      "attributes" : {
        "sampleEveryNthSimStep" : 1
      }
    }, {
      "type" : "org.vadere.simulator.projects.dataprocessing.processor.PedestrianEndTimeProcessor",
      "id" : 13
    } ],
    "isTimestamped" : false,
    "isWriteMetaData" : true
  },
  "scenario" : {
    "mainModel" : "org.vadere.simulator.models.osm.OptimalStepsModel",
    "attributesModel" : {
      "org.vadere.state.attributes.models.AttributesOSM" : {
        "stepCircleResolution" : 4,
        "numberOfCircles" : 1,
        "optimizationType" : "NELDER_MEAD",
        "varyStepDirection" : true,
        "movementType" : "ARBITRARY",
        "stepLengthIntercept" : 0.4625,
        "stepLengthSlopeSpeed" : 0.2345,
        "stepLengthSD" : 0.036,
        "movementThreshold" : 0.0,
        "minStepLength" : 0.1,
        "minimumStepLength" : true,
        "maxStepDuration" : 1.7976931348623157E308,
        "dynamicStepLength" : true,
        "updateType" : "EVENT_DRIVEN",
        "seeSmallWalls" : true,
        "targetPotentialModel" : "org.vadere.simulator.models.potential.fields.PotentialFieldTargetGrid",
        "pedestrianPotentialModel" : "org.vadere.simulator.models.potential.PotentialFieldPedestrianCompactSoftshell",
        "obstaclePotentialModel" : "org.vadere.simulator.models.potential.PotentialFieldObstacleCompactSoftshell",
        "submodels" : [ "org.vadere.simulator.models.infection.AirTransmissionModel" ]
      },
      "org.vadere.state.attributes.models.AttributesPotentialCompactSoftshell" : {
        "pedPotentialIntimateSpaceWidth" : 0.1,
        "pedPotentialPersonalSpaceWidth" : 0.15,
        "pedPotentialHeight" : 50.0,
        "obstPotentialWidth" : 0.25,
        "obstPotentialHeight" : 6.0,
        "intimateSpaceFactor" : 1.2,
        "personalSpacePower" : 1,
        "intimateSpacePower" : 1
      },
      "org.vadere.state.attributes.models.AttributesFloorField" : {
        "createMethod" : "HIGH_ACCURACY_FAST_MARCHING",
        "potentialFieldResolution" : 0.1,
        "obstacleGridPenalty" : 0.1,
        "targetAttractionStrength" : 1.0,
        "cacheType" : "NO_CACHE",
        "cacheDir" : "",
        "timeCostAttributes" : {
          "standardDeviation" : 0.7,
          "type" : "UNIT",
          "obstacleDensityWeight" : 3.5,
          "pedestrianSameTargetDensityWeight" : 3.5,
          "pedestrianOtherTargetDensityWeight" : 3.5,
          "pedestrianWeight" : 3.5,
          "queueWidthLoading" : 1.0,
          "pedestrianDynamicWeight" : 6.0,
          "loadingType" : "CONSTANT",
          "width" : 0.2,
          "height" : 1.0
        }
      },
      "org.vadere.state.attributes.models.infection.AttributesAirTransmissionModel" : {
        "exposureModelSourceParameters" : [ {
          "sourceId" : -1,
          "infectious" : false
        }, {
          "sourceId" : 11,
          "infectious" : true
        } ],
        "infectiousPedestrianIdsNoSource" : [ ],
        "pedestrianRespiratoryCyclePeriod" : 3.0,
        "aerosolCloudsActive" : true,
        "aerosolCloudParameters" : {
          "halfLife" : 5300.0,
          "initialRadius" : 1.0,
          "initialPathogenLoad" : 1.0,
          "airDispersionFactor" : 0.003,
          "pedestrianDispersionWeight" : 0.0,
          "absorptionRate" : 5.0E-4
        },
        "dropletsActive" : false,
        "dropletParameters" : {
          "emissionFrequency" : 0.016666666666666666,
          "distanceOfSpread" : 1.5,
          "angleOfSpreadInDeg" : 30.0,
          "lifeTime" : 1.5,
          "pathogenLoad" : 10000.0,
          "absorptionRate" : 0.1
        }
      }
    },
    "attributesSimulation" : {
      "finishTime" : 2300.0,
      "simTimeStepLength" : 0.4,
      "realTimeSimTimeRatio" : 0.0,
      "writeSimulationData" : true,
      "visualizationEnabled" : true,
      "printFPS" : false,
      "digitsPerCoordinate" : 2,
      "useFixedSeed" : true,
      "fixedSeed" : 1436250873317888407,
      "simulationSeed" : 1436250873317888407
    },
    "attributesPsychology" : {
      "usePsychologyLayer" : false,
      "psychologyLayer" : {
        "perception" : "SimplePerceptionModel",
        "cognition" : "SimpleCognitionModel",
        "attributesModel" : {
          "org.vadere.state.attributes.models.psychology.perception.AttributesSimplePerceptionModel" : {
            "priority" : {
              "1" : "InformationStimulus",
              "2" : "ChangeTargetScripted",
              "3" : "ChangeTarget",
              "4" : "Threat",
              "5" : "Wait",
              "6" : "WaitInArea",
              "7" : "DistanceRecommendation"
            }
          },
          "org.vadere.state.attributes.models.psychology.cognition.AttributesSimpleCognitionModel" : { }
        }
      }
    },
    "topography" : {
      "attributes" : {
        "bounds" : {
          "x" : 0.0,
          "y" : 0.0,
          "width" : 15.85,
          "height" : 5.8
        },
        "boundingBoxWidth" : 0.5,
        "bounded" : true,
        "referenceCoordinateSystem" : null
      },
      "obstacles" : [ {
        "shape" : {
          "x" : 0.5,
          "y" : 1.3,
          "width" : 3.025,
          "height" : 4.0,
          "type" : "RECTANGLE"
        },
        "id" : 22
      }, {
        "shape" : {
          "x" : 12.325,
          "y" : 1.3,
          "width" : 3.025,
          "height" : 4.0,
          "type" : "RECTANGLE"
        },
        "id" : 23
      }, {
        "shape" : {
          "x" : 4.875,
          "y" : 2.1,
          "width" : 5.9,
          "height" : 0.3,
          "type" : "RECTANGLE"
        },
        "id" : 27
      }, {
        "shape" : {
          "x" : 4.875,
          "y" : 3.7,
          "width" : 5.9,
          "height" : 0.3,
          "type" : "RECTANGLE"
        },
        "id" : 28
      } ],
      "measurementAreas" : [ ],
      "stairs" : [ ],
      "targets" : [ {
        "id" : 3,
        "absorbing" : true,
        "shape" : {
          "x" : 63.0,
          "y" : 0.5,
          "width" : 1.5,
          "height" : 0.8,
          "type" : "RECTANGLE"
        },
        "waitingTime" : 90.0,
        "waitingTimeYellowPhase" : 0.0,
        "parallelWaiters" : 1,
        "individualWaiting" : true,
        "deletionDistance" : 0.1,
        "startingWithRedLight" : false,
        "nextSpeed" : -1.0
      }, {
        "id" : 100,
        "absorbing" : true,
        "shape" : {
          "x" : 14.7,
          "y" : 0.65,
          "width" : 0.5,
          "height" : 0.5,
          "type" : "RECTANGLE"
        },
        "waitingTime" : 90.0,
        "waitingTimeYellowPhase" : 0.0,
        "parallelWaiters" : 0,
        "individualWaiting" : true,
        "deletionDistance" : 0.1,
        "startingWithRedLight" : false,
        "nextSpeed" : -1.0
      }, {
        "id" : 1,
        "absorbing" : false,
        "shape" : {
          "x" : 5.15,
          "y" : 1.575,
          "width" : 0.25,
          "height" : 0.25,
          "type" : "RECTANGLE"
        },
        "waitingTime" : 1734.0,
        "waitingTimeYellowPhase" : 0.0,
        "parallelWaiters" : 0,
        "individualWaiting" : true,
        "deletionDistance" : 0.1,
        "startingWithRedLight" : false,
        "nextSpeed" : -1.0
      }, {
        "id" : 2,
        "absorbing" : false,
        "shape" : {
          "x" : 6.0,
          "y" : 1.575,
          "width" : 0.25,
          "height" : 0.25,
          "type" : "RECTANGLE"
        },
        "waitingTime" : 1645.0,
        "waitingTimeYellowPhase" : 0.0,
        "parallelWaiters" : 0,
        "individualWaiting" : true,
        "deletionDistance" : 0.1,
        "startingWithRedLight" : false,
        "nextSpeed" : -1.0
      }, {
        "id" : 3,
        "absorbing" : false,
        "shape" : {
          "x" : 6.85,
          "y" : 1.575,
          "width" : 0.25,
          "height" : 0.25,
          "type" : "RECTANGLE"
        },
        "waitingTime" : 1557.0,
        "waitingTimeYellowPhase" : 0.0,
        "parallelWaiters" : 0,
        "individualWaiting" : true,
        "deletionDistance" : 0.1,
        "startingWithRedLight" : false,
        "nextSpeed" : -1.0
      }, {
        "id" : 4,
        "absorbing" : false,
        "shape" : {
          "x" : 7.7,
          "y" : 1.575,
          "width" : 0.25,
          "height" : 0.25,
          "type" : "RECTANGLE"
        },
        "waitingTime" : 1470.0,
        "waitingTimeYellowPhase" : 0.0,
        "parallelWaiters" : 0,
        "individualWaiting" : true,
        "deletionDistance" : 0.1,
        "startingWithRedLight" : false,
        "nextSpeed" : -1.0
      }, {
        "id" : 5,
        "absorbing" : false,
        "shape" : {
          "x" : 8.55,
          "y" : 1.575,
          "width" : 0.25,
          "height" : 0.25,
          "type" : "RECTANGLE"
        },
        "waitingTime" : 1384.0,
        "waitingTimeYellowPhase" : 0.0,
        "parallelWaiters" : 0,
        "individualWaiting" : true,
        "deletionDistance" : 0.1,
        "startingWithRedLight" : false,
        "nextSpeed" : -1.0
      }, {
        "id" : 6,
        "absorbing" : false,
        "shape" : {
          "x" : 9.4,
          "y" : 1.575,
          "width" : 0.25,
          "height" : 0.25,
          "type" : "RECTANGLE"
        },
        "waitingTime" : 1299.0,
        "waitingTimeYellowPhase" : 0.0,
        "parallelWaiters" : 0,
        "individualWaiting" : true,
        "deletionDistance" : 0.1,
        "startingWithRedLight" : false,
        "nextSpeed" : -1.0
      }, {
        "id" : 7,
        "absorbing" : false,
        "shape" : {
          "x" : 10.25,
          "y" : 1.575,
          "width" : 0.25,
          "height" : 0.25,
          "type" : "RECTANGLE"
        },
        "waitingTime" : 1215.0,
        "waitingTimeYellowPhase" : 0.0,
        "parallelWaiters" : 0,
        "individualWaiting" : true,
        "deletionDistance" : 0.1,
        "startingWithRedLight" : false,
        "nextSpeed" : -1.0
      }, {
        "id" : 8,
        "absorbing" : false,
        "shape" : {
          "x" : 5.15,
          "y" : 3.175,
          "width" : 0.25,
          "height" : 0.25,
          "type" : "RECTANGLE"
        },
        "waitingTime" : 1132.0,
        "waitingTimeYellowPhase" : 0.0,
        "parallelWaiters" : 0,
        "individualWaiting" : true,
        "deletionDistance" : 0.1,
        "startingWithRedLight" : false,
        "nextSpeed" : -1.0
      }, {
        "id" : 9,
        "absorbing" : false,
        "shape" : {
          "x" : 6.0,
          "y" : 3.175,
          "width" : 0.25,
          "height" : 0.25,
          "type" : "RECTANGLE"
        },
        "waitingTime" : 1044.0,
        "waitingTimeYellowPhase" : 0.0,
        "parallelWaiters" : 0,
        "individualWaiting" : true,
        "deletionDistance" : 0.1,
        "startingWithRedLight" : false,
        "nextSpeed" : -1.0
      }, {
        "id" : 10,
        "absorbing" : false,
        "shape" : {
          "x" : 6.85,
          "y" : 3.175,
          "width" : 0.25,
          "height" : 0.25,
          "type" : "RECTANGLE"
        },
        "waitingTime" : 957.0,
        "waitingTimeYellowPhase" : 0.0,
        "parallelWaiters" : 0,
        "individualWaiting" : true,
        "deletionDistance" : 0.1,
        "startingWithRedLight" : false,
        "nextSpeed" : -1.0
      }, {
        "id" : 11,
        "absorbing" : false,
        "shape" : {
          "x" : 7.7,
          "y" : 3.175,
          "width" : 0.25,
          "height" : 0.25,
          "type" : "RECTANGLE"
        },
        "waitingTime" : 870.0,
        "waitingTimeYellowPhase" : 0.0,
        "parallelWaiters" : 0,
        "individualWaiting" : true,
        "deletionDistance" : 0.1,
        "startingWithRedLight" : false,
        "nextSpeed" : -1.0
      }, {
        "id" : 12,
        "absorbing" : false,
        "shape" : {
          "x" : 8.55,
          "y" : 3.175,
          "width" : 0.25,
          "height" : 0.25,
          "type" : "RECTANGLE"
        },
        "waitingTime" : 784.0,
        "waitingTimeYellowPhase" : 0.0,
        "parallelWaiters" : 0,
        "individualWaiting" : true,
        "deletionDistance" : 0.1,
        "startingWithRedLight" : false,
        "nextSpeed" : -1.0
      }, {
        "id" : 13,
        "absorbing" : false,
        "shape" : {
          "x" : 9.4,
          "y" : 3.175,
          "width" : 0.25,
          "height" : 0.25,
          "type" : "RECTANGLE"
        },
        "waitingTime" : 699.0,
        "waitingTimeYellowPhase" : 0.0,
        "parallelWaiters" : 0,
        "individualWaiting" : true,
        "deletionDistance" : 0.1,
        "startingWithRedLight" : false,
        "nextSpeed" : -1.0
      }, {
        "id" : 14,
        "absorbing" : false,
        "shape" : {
          "x" : 10.25,
          "y" : 3.175,
          "width" : 0.25,
          "height" : 0.25,
          "type" : "RECTANGLE"
        },
        "waitingTime" : 614.0,
        "waitingTimeYellowPhase" : 0.0,
        "parallelWaiters" : 0,
        "individualWaiting" : true,
        "deletionDistance" : 0.1,
        "startingWithRedLight" : false,
        "nextSpeed" : -1.0
      }, {
        "id" : 15,
        "absorbing" : false,
        "shape" : {
          "x" : 5.15,
          "y" : 4.775,
          "width" : 0.25,
          "height" : 0.25,
          "type" : "RECTANGLE"
        },
        "waitingTime" : 527.0,
        "waitingTimeYellowPhase" : 0.0,
        "parallelWaiters" : 0,
        "individualWaiting" : true,
        "deletionDistance" : 0.1,
        "startingWithRedLight" : false,
        "nextSpeed" : -1.0
      }, {
        "id" : 16,
        "absorbing" : false,
        "shape" : {
          "x" : 6.0,
          "y" : 4.775,
          "width" : 0.25,
          "height" : 0.25,
          "type" : "RECTANGLE"
        },
        "waitingTime" : 438.0,
        "waitingTimeYellowPhase" : 0.0,
        "parallelWaiters" : 0,
        "individualWaiting" : true,
        "deletionDistance" : 0.1,
        "startingWithRedLight" : false,
        "nextSpeed" : -1.0
      }, {
        "id" : 17,
        "absorbing" : false,
        "shape" : {
          "x" : 6.85,
          "y" : 4.775,
          "width" : 0.25,
          "height" : 0.25,
          "type" : "RECTANGLE"
        },
        "waitingTime" : 349.0,
        "waitingTimeYellowPhase" : 0.0,
        "parallelWaiters" : 0,
        "individualWaiting" : true,
        "deletionDistance" : 0.1,
        "startingWithRedLight" : false,
        "nextSpeed" : -1.0
      }, {
        "id" : 18,
        "absorbing" : false,
        "shape" : {
          "x" : 7.7,
          "y" : 4.775,
          "width" : 0.25,
          "height" : 0.25,
          "type" : "RECTANGLE"
        },
        "waitingTime" : 261.0,
        "waitingTimeYellowPhase" : 0.0,
        "parallelWaiters" : 0,
        "individualWaiting" : true,
        "deletionDistance" : 0.1,
        "startingWithRedLight" : false,
        "nextSpeed" : -1.0
      }, {
        "id" : 19,
        "absorbing" : false,
        "shape" : {
          "x" : 8.55,
          "y" : 4.775,
          "width" : 0.25,
          "height" : 0.25,
          "type" : "RECTANGLE"
        },
        "waitingTime" : 174.0,
        "waitingTimeYellowPhase" : 0.0,
        "parallelWaiters" : 0,
        "individualWaiting" : true,
        "deletionDistance" : 0.1,
        "startingWithRedLight" : false,
        "nextSpeed" : -1.0
      }, {
        "id" : 20,
        "absorbing" : false,
        "shape" : {
          "x" : 9.4,
          "y" : 4.775,
          "width" : 0.25,
          "height" : 0.25,
          "type" : "RECTANGLE"
        },
        "waitingTime" : 87.0,
        "waitingTimeYellowPhase" : 0.0,
        "parallelWaiters" : 0,
        "individualWaiting" : true,
        "deletionDistance" : 0.1,
        "startingWithRedLight" : false,
        "nextSpeed" : -1.0
      }, {
        "id" : 21,
        "absorbing" : false,
        "shape" : {
          "x" : 10.25,
          "y" : 4.775,
          "width" : 0.25,
          "height" : 0.25,
          "type" : "RECTANGLE"
        },
        "waitingTime" : 0.0,
        "waitingTimeYellowPhase" : 0.0,
        "parallelWaiters" : 0,
        "individualWaiting" : true,
        "deletionDistance" : 0.1,
        "startingWithRedLight" : false,
        "nextSpeed" : -1.0
      }, {
        "id" : 31,
        "absorbing" : false,
        "shape" : {
          "x" : 4.6,
          "y" : 4.15,
          "width" : 0.2,
          "height" : 0.6,
          "type" : "RECTANGLE"
        },
        "waitingTime" : 0.0,
        "waitingTimeYellowPhase" : 0.0,
        "parallelWaiters" : 0,
        "individualWaiting" : true,
        "deletionDistance" : 0.1,
        "startingWithRedLight" : false,
        "nextSpeed" : -1.0
      }, {
        "id" : 33,
        "absorbing" : false,
        "shape" : {
          "x" : 4.6,
          "y" : 2.55,
          "width" : 0.2,
          "height" : 0.6,
          "type" : "RECTANGLE"
        },
        "waitingTime" : 0.0,
        "waitingTimeYellowPhase" : 0.0,
        "parallelWaiters" : 0,
        "individualWaiting" : true,
        "deletionDistance" : 0.1,
        "startingWithRedLight" : false,
        "nextSpeed" : -1.0
      }, {
        "id" : 34,
        "absorbing" : false,
        "shape" : {
          "x" : 10.85,
          "y" : 2.55,
          "width" : 0.2,
          "height" : 0.6,
          "type" : "RECTANGLE"
        },
        "waitingTime" : 0.0,
        "waitingTimeYellowPhase" : 0.0,
        "parallelWaiters" : 0,
        "individualWaiting" : true,
        "deletionDistance" : 0.1,
        "startingWithRedLight" : false,
        "nextSpeed" : -1.0
      }, {
        "id" : 32,
        "absorbing" : false,
        "shape" : {
          "x" : 10.85,
          "y" : 4.15,
          "width" : 0.2,
          "height" : 0.6,
          "type" : "RECTANGLE"
        },
        "waitingTime" : 0.0,
        "waitingTimeYellowPhase" : 0.0,
        "parallelWaiters" : 0,
        "individualWaiting" : true,
        "deletionDistance" : 0.1,
        "startingWithRedLight" : false,
        "nextSpeed" : -1.0
      } ],
      "targetChangers" : [ ],
      "absorbingAreas" : [ ],
      "aerosolClouds" : [ ],
      "droplets" : [ ],
      "sources" : [ {
        "id" : 21,
        "shape" : {
          "x" : 0.65,
          "y" : 0.65,
          "width" : 0.5,
          "height" : 0.5,
          "type" : "RECTANGLE"
        },
        "interSpawnTimeDistribution" : "constant",
        "distributionParameters" : {
          "updateFrequency" : 1.0
        },
        "spawnNumber" : 1,
        "maxSpawnNumberTotal" : 1,
        "startTime" : 200.0,
        "endTime" : 201.0,
        "spawnAtRandomPositions" : false,
        "spawnAtGridPositionsCA" : false,
        "useFreeSpaceOnly" : true,
        "targetIds" : [ 1, 100 ],
        "groupSizeDistribution" : [ 1.0 ],
        "dynamicElementType" : "PEDESTRIAN",
        "attributesPedestrian" : null
      }, {
        "id" : 20,
        "shape" : {
          "x" : 0.65,
          "y" : 0.65,
          "width" : 0.5,
          "height" : 0.5,
          "type" : "RECTANGLE"
        },
        "interSpawnTimeDistribution" : "constant",
        "distributionParameters" : {
          "updateFrequency" : 1.0
        },
        "spawnNumber" : 1,
        "maxSpawnNumberTotal" : 1,
        "startTime" : 190.0,
        "endTime" : 191.0,
        "spawnAtRandomPositions" : false,
        "spawnAtGridPositionsCA" : false,
        "useFreeSpaceOnly" : true,
        "targetIds" : [ 2, 100 ],
        "groupSizeDistribution" : [ 1.0 ],
        "dynamicElementType" : "PEDESTRIAN",
        "attributesPedestrian" : null
      }, {
        "id" : 19,
        "shape" : {
          "x" : 0.65,
          "y" : 0.65,
          "width" : 0.5,
          "height" : 0.5,
          "type" : "RECTANGLE"
        },
        "interSpawnTimeDistribution" : "constant",
        "distributionParameters" : {
          "updateFrequency" : 1.0
        },
        "spawnNumber" : 1,
        "maxSpawnNumberTotal" : 1,
        "startTime" : 180.0,
        "endTime" : 181.0,
        "spawnAtRandomPositions" : false,
        "spawnAtGridPositionsCA" : false,
        "useFreeSpaceOnly" : true,
        "targetIds" : [ 3, 100 ],
        "groupSizeDistribution" : [ 1.0 ],
        "dynamicElementType" : "PEDESTRIAN",
        "attributesPedestrian" : null
      }, {
        "id" : 18,
        "shape" : {
          "x" : 0.65,
          "y" : 0.65,
          "width" : 0.5,
          "height" : 0.5,
          "type" : "RECTANGLE"
        },
        "interSpawnTimeDistribution" : "constant",
        "distributionParameters" : {
          "updateFrequency" : 1.0
        },
        "spawnNumber" : 1,
        "maxSpawnNumberTotal" : 1,
        "startTime" : 170.0,
        "endTime" : 171.0,
        "spawnAtRandomPositions" : false,
        "spawnAtGridPositionsCA" : false,
        "useFreeSpaceOnly" : true,
        "targetIds" : [ 4, 100 ],
        "groupSizeDistribution" : [ 1.0 ],
        "dynamicElementType" : "PEDESTRIAN",
        "attributesPedestrian" : null
      }, {
        "id" : 17,
        "shape" : {
          "x" : 0.65,
          "y" : 0.65,
          "width" : 0.5,
          "height" : 0.5,
          "type" : "RECTANGLE"
        },
        "interSpawnTimeDistribution" : "constant",
        "distributionParameters" : {
          "updateFrequency" : 1.0
        },
        "spawnNumber" : 1,
        "maxSpawnNumberTotal" : 1,
        "startTime" : 160.0,
        "endTime" : 161.0,
        "spawnAtRandomPositions" : false,
        "spawnAtGridPositionsCA" : false,
        "useFreeSpaceOnly" : true,
        "targetIds" : [ 5, 100 ],
        "groupSizeDistribution" : [ 1.0 ],
        "dynamicElementType" : "PEDESTRIAN",
        "attributesPedestrian" : null
      }, {
        "id" : 16,
        "shape" : {
          "x" : 0.65,
          "y" : 0.65,
          "width" : 0.5,
          "height" : 0.5,
          "type" : "RECTANGLE"
        },
        "interSpawnTimeDistribution" : "constant",
        "distributionParameters" : {
          "updateFrequency" : 1.0
        },
        "spawnNumber" : 1,
        "maxSpawnNumberTotal" : 1,
        "startTime" : 150.0,
        "endTime" : 151.0,
        "spawnAtRandomPositions" : false,
        "spawnAtGridPositionsCA" : false,
        "useFreeSpaceOnly" : true,
        "targetIds" : [ 6, 100 ],
        "groupSizeDistribution" : [ 1.0 ],
        "dynamicElementType" : "PEDESTRIAN",
        "attributesPedestrian" : null
      }, {
        "id" : 15,
        "shape" : {
          "x" : 0.65,
          "y" : 0.65,
          "width" : 0.5,
          "height" : 0.5,
          "type" : "RECTANGLE"
        },
        "interSpawnTimeDistribution" : "constant",
        "distributionParameters" : {
          "updateFrequency" : 1.0
        },
        "spawnNumber" : 1,
        "maxSpawnNumberTotal" : 1,
        "startTime" : 140.0,
        "endTime" : 141.0,
        "spawnAtRandomPositions" : false,
        "spawnAtGridPositionsCA" : false,
        "useFreeSpaceOnly" : true,
        "targetIds" : [ 7, 100 ],
        "groupSizeDistribution" : [ 1.0 ],
        "dynamicElementType" : "PEDESTRIAN",
        "attributesPedestrian" : null
      }, {
        "id" : 14,
        "shape" : {
          "x" : 0.65,
          "y" : 0.65,
          "width" : 0.5,
          "height" : 0.5,
          "type" : "RECTANGLE"
        },
        "interSpawnTimeDistribution" : "constant",
        "distributionParameters" : {
          "updateFrequency" : 1.0
        },
        "spawnNumber" : 1,
        "maxSpawnNumberTotal" : 1,
        "startTime" : 130.0,
        "endTime" : 131.0,
        "spawnAtRandomPositions" : false,
        "spawnAtGridPositionsCA" : false,
        "useFreeSpaceOnly" : true,
        "targetIds" : [ 33, 8, 34, 100 ],
        "groupSizeDistribution" : [ 1.0 ],
        "dynamicElementType" : "PEDESTRIAN",
        "attributesPedestrian" : null
      }, {
        "id" : 13,
        "shape" : {
          "x" : 0.65,
          "y" : 0.65,
          "width" : 0.5,
          "height" : 0.5,
          "type" : "RECTANGLE"
        },
        "interSpawnTimeDistribution" : "constant",
        "distributionParameters" : {
          "updateFrequency" : 1.0
        },
        "spawnNumber" : 1,
        "maxSpawnNumberTotal" : 1,
        "startTime" : 120.0,
        "endTime" : 121.0,
        "spawnAtRandomPositions" : false,
        "spawnAtGridPositionsCA" : false,
        "useFreeSpaceOnly" : true,
        "targetIds" : [ 33, 9, 34, 100 ],
        "groupSizeDistribution" : [ 1.0 ],
        "dynamicElementType" : "PEDESTRIAN",
        "attributesPedestrian" : null
      }, {
        "id" : 12,
        "shape" : {
          "x" : 0.65,
          "y" : 0.65,
          "width" : 0.5,
          "height" : 0.5,
          "type" : "RECTANGLE"
        },
        "interSpawnTimeDistribution" : "constant",
        "distributionParameters" : {
          "updateFrequency" : 1.0
        },
        "spawnNumber" : 1,
        "maxSpawnNumberTotal" : 1,
        "startTime" : 110.0,
        "endTime" : 111.0,
        "spawnAtRandomPositions" : false,
        "spawnAtGridPositionsCA" : false,
        "useFreeSpaceOnly" : true,
        "targetIds" : [ 33, 10, 34, 100 ],
        "groupSizeDistribution" : [ 1.0 ],
        "dynamicElementType" : "PEDESTRIAN",
        "attributesPedestrian" : null
      }, {
        "id" : 11,
        "shape" : {
          "x" : 0.65,
          "y" : 0.65,
          "width" : 0.5,
          "height" : 0.5,
          "type" : "RECTANGLE"
        },
        "interSpawnTimeDistribution" : "constant",
        "distributionParameters" : {
          "updateFrequency" : 1.0
        },
        "spawnNumber" : 1,
        "maxSpawnNumberTotal" : 1,
        "startTime" : 100.0,
        "endTime" : 101.0,
        "spawnAtRandomPositions" : false,
        "spawnAtGridPositionsCA" : false,
        "useFreeSpaceOnly" : true,
        "targetIds" : [ 33, 11, 34, 100 ],
        "groupSizeDistribution" : [ 1.0 ],
        "dynamicElementType" : "PEDESTRIAN",
        "attributesPedestrian" : null
      }, {
        "id" : 10,
        "shape" : {
          "x" : 0.65,
          "y" : 0.65,
          "width" : 0.5,
          "height" : 0.5,
          "type" : "RECTANGLE"
        },
        "interSpawnTimeDistribution" : "constant",
        "distributionParameters" : {
          "updateFrequency" : 1.0
        },
        "spawnNumber" : 1,
        "maxSpawnNumberTotal" : 1,
        "startTime" : 90.0,
        "endTime" : 91.0,
        "spawnAtRandomPositions" : false,
        "spawnAtGridPositionsCA" : false,
        "useFreeSpaceOnly" : true,
        "targetIds" : [ 33, 12, 34, 100 ],
        "groupSizeDistribution" : [ 1.0 ],
        "dynamicElementType" : "PEDESTRIAN",
        "attributesPedestrian" : null
      }, {
        "id" : 9,
        "shape" : {
          "x" : 0.65,
          "y" : 0.65,
          "width" : 0.5,
          "height" : 0.5,
          "type" : "RECTANGLE"
        },
        "interSpawnTimeDistribution" : "constant",
        "distributionParameters" : {
          "updateFrequency" : 1.0
        },
        "spawnNumber" : 1,
        "maxSpawnNumberTotal" : 1,
        "startTime" : 80.0,
        "endTime" : 81.0,
        "spawnAtRandomPositions" : false,
        "spawnAtGridPositionsCA" : false,
        "useFreeSpaceOnly" : true,
        "targetIds" : [ 33, 13, 34, 100 ],
        "groupSizeDistribution" : [ 1.0 ],
        "dynamicElementType" : "PEDESTRIAN",
        "attributesPedestrian" : null
      }, {
        "id" : 8,
        "shape" : {
          "x" : 0.65,
          "y" : 0.65,
          "width" : 0.5,
          "height" : 0.5,
          "type" : "RECTANGLE"
        },
        "interSpawnTimeDistribution" : "constant",
        "distributionParameters" : {
          "updateFrequency" : 1.0
        },
        "spawnNumber" : 1,
        "maxSpawnNumberTotal" : 1,
        "startTime" : 70.0,
        "endTime" : 71.0,
        "spawnAtRandomPositions" : false,
        "spawnAtGridPositionsCA" : false,
        "useFreeSpaceOnly" : true,
        "targetIds" : [ 33, 14, 34, 100 ],
        "groupSizeDistribution" : [ 1.0 ],
        "dynamicElementType" : "PEDESTRIAN",
        "attributesPedestrian" : null
      }, {
        "id" : 7,
        "shape" : {
          "x" : 0.65,
          "y" : 0.65,
          "width" : 0.5,
          "height" : 0.5,
          "type" : "RECTANGLE"
        },
        "interSpawnTimeDistribution" : "constant",
        "distributionParameters" : {
          "updateFrequency" : 1.0
        },
        "spawnNumber" : 1,
        "maxSpawnNumberTotal" : 1,
        "startTime" : 60.0,
        "endTime" : 61.0,
        "spawnAtRandomPositions" : false,
        "spawnAtGridPositionsCA" : false,
        "useFreeSpaceOnly" : true,
        "targetIds" : [ 31, 15, 32, 100 ],
        "groupSizeDistribution" : [ 1.0 ],
        "dynamicElementType" : "PEDESTRIAN",
        "attributesPedestrian" : null
      }, {
        "id" : 6,
        "shape" : {
          "x" : 0.65,
          "y" : 0.65,
          "width" : 0.5,
          "height" : 0.5,
          "type" : "RECTANGLE"
        },
        "interSpawnTimeDistribution" : "constant",
        "distributionParameters" : {
          "updateFrequency" : 1.0
        },
        "spawnNumber" : 1,
        "maxSpawnNumberTotal" : 1,
        "startTime" : 50.0,
        "endTime" : 51.0,
        "spawnAtRandomPositions" : false,
        "spawnAtGridPositionsCA" : false,
        "useFreeSpaceOnly" : true,
        "targetIds" : [ 31, 16, 32, 100 ],
        "groupSizeDistribution" : [ 1.0 ],
        "dynamicElementType" : "PEDESTRIAN",
        "attributesPedestrian" : null
      }, {
        "id" : 5,
        "shape" : {
          "x" : 0.65,
          "y" : 0.65,
          "width" : 0.5,
          "height" : 0.5,
          "type" : "RECTANGLE"
        },
        "interSpawnTimeDistribution" : "constant",
        "distributionParameters" : {
          "updateFrequency" : 1.0
        },
        "spawnNumber" : 1,
        "maxSpawnNumberTotal" : 1,
        "startTime" : 40.0,
        "endTime" : 41.0,
        "spawnAtRandomPositions" : false,
        "spawnAtGridPositionsCA" : false,
        "useFreeSpaceOnly" : true,
        "targetIds" : [ 31, 17, 32, 100 ],
        "groupSizeDistribution" : [ 1.0 ],
        "dynamicElementType" : "PEDESTRIAN",
        "attributesPedestrian" : null
      }, {
        "id" : 4,
        "shape" : {
          "x" : 0.65,
          "y" : 0.65,
          "width" : 0.5,
          "height" : 0.5,
          "type" : "RECTANGLE"
        },
        "interSpawnTimeDistribution" : "constant",
        "distributionParameters" : {
          "updateFrequency" : 1.0
        },
        "spawnNumber" : 1,
        "maxSpawnNumberTotal" : 1,
        "startTime" : 30.0,
        "endTime" : 31.0,
        "spawnAtRandomPositions" : false,
        "spawnAtGridPositionsCA" : false,
        "useFreeSpaceOnly" : true,
        "targetIds" : [ 31, 18, 32, 100 ],
        "groupSizeDistribution" : [ 1.0 ],
        "dynamicElementType" : "PEDESTRIAN",
        "attributesPedestrian" : null
      }, {
        "id" : 3,
        "shape" : {
          "x" : 0.65,
          "y" : 0.65,
          "width" : 0.5,
          "height" : 0.5,
          "type" : "RECTANGLE"
        },
        "interSpawnTimeDistribution" : "constant",
        "distributionParameters" : {
          "updateFrequency" : 1.0
        },
        "spawnNumber" : 1,
        "maxSpawnNumberTotal" : 1,
        "startTime" : 20.0,
        "endTime" : 21.0,
        "spawnAtRandomPositions" : false,
        "spawnAtGridPositionsCA" : false,
        "useFreeSpaceOnly" : true,
        "targetIds" : [ 31, 19, 32, 100 ],
        "groupSizeDistribution" : [ 1.0 ],
        "dynamicElementType" : "PEDESTRIAN",
        "attributesPedestrian" : null
      }, {
        "id" : 2,
        "shape" : {
          "x" : 0.65,
          "y" : 0.65,
          "width" : 0.5,
          "height" : 0.5,
          "type" : "RECTANGLE"
        },
        "interSpawnTimeDistribution" : "constant",
        "distributionParameters" : {
          "updateFrequency" : 1.0
        },
        "spawnNumber" : 1,
        "maxSpawnNumberTotal" : 1,
        "startTime" : 10.0,
        "endTime" : 11.0,
        "spawnAtRandomPositions" : false,
        "spawnAtGridPositionsCA" : false,
        "useFreeSpaceOnly" : true,
        "targetIds" : [ 31, 20, 32, 100 ],
        "groupSizeDistribution" : [ 1.0 ],
        "dynamicElementType" : "PEDESTRIAN",
        "attributesPedestrian" : null
      }, {
        "id" : 1,
        "shape" : {
          "x" : 0.65,
          "y" : 0.65,
          "width" : 0.5,
          "height" : 0.5,
          "type" : "RECTANGLE"
        },
        "interSpawnTimeDistribution" : "constant",
        "distributionParameters" : {
          "updateFrequency" : 1.0
        },
        "spawnNumber" : 1,
        "maxSpawnNumberTotal" : 1,
        "startTime" : 0.0,
        "endTime" : 0.0,
        "spawnAtRandomPositions" : false,
        "spawnAtGridPositionsCA" : false,
        "useFreeSpaceOnly" : true,
        "targetIds" : [ 31, 21, 32, 100 ],
        "groupSizeDistribution" : [ 1.0 ],
        "dynamicElementType" : "PEDESTRIAN",
        "attributesPedestrian" : null
      } ],
      "dynamicElements" : [ ],
      "attributesPedestrian" : {
        "radius" : 0.2,
        "densityDependentSpeed" : false,
        "speedDistributionMean" : 1.34,
        "speedDistributionStandardDeviation" : 0.0,
        "minimumSpeed" : 0.5,
        "maximumSpeed" : 2.2,
        "acceleration" : 2.0,
        "footstepHistorySize" : 4,
        "searchRadius" : 1.0,
        "walkingDirectionCalculation" : "BY_TARGET_CENTER",
        "walkingDirectionSameIfAngleLessOrEqual" : 45.0
      },
      "teleporter" : null,
      "attributesCar" : {
        "id" : -1,
        "radius" : 0.2,
        "densityDependentSpeed" : false,
        "speedDistributionMean" : 1.34,
        "speedDistributionStandardDeviation" : 0.0,
        "minimumSpeed" : 0.5,
        "maximumSpeed" : 2.2,
        "acceleration" : 2.0,
        "footstepHistorySize" : 4,
        "searchRadius" : 1.0,
        "walkingDirectionCalculation" : "BY_TARGET_CENTER",
        "walkingDirectionSameIfAngleLessOrEqual" : 45.0,
        "length" : 4.5,
        "width" : 1.7,
        "direction" : {
          "x" : 1.0,
          "y" : 0.0
        }
      }
    },
    "stimulusInfos" : [ ]
  }
}