from typing import List

from matplotlib.pyplot import *
import matplotlib.pyplot as plt
import os.path
import pandas as pd
import math as m

from vimuq.experiments.plot.style import cm_to_inches
from vimuq.uq.uq_experiment import UQExperiment, Stage
import matplotlib.patches as mpatches


def read_and_aggregate_experiments(
    path2dumped_experiment, analysis_suffix, avrg, qoi_map, param_map
):
    data = collect_experiments(analysis_suffix, avrg, path2dumped_experiment)
    data = order_data_frame_levels(data, param_map, qoi_map)

    return data


def order_data_frame_levels(data, param_map, qoi_map):
    # in case "run_id" is not an index, add it as a dummy because this structure
    # simplifies generalized processing
    if "run_id" not in data.index.names.copy():
        data = add_index(df=data, index_name="run_id", value=np.nan)
    # 2) drop all data that is not relevant
    # 2a) quantities of interest
    data = select_by_index_or_column_values(
        df=data, level="qoi", select_values=list(qoi_map.keys())
    )
    # 2b) parameters
    data = select_by_index_or_column_values(
        df=data, level="param", select_values=list(param_map.keys())
    )
    # 3) reorder levels of data
    idx_fixed = ["avrg", "qoi"]
    idx_data = data.index.names.copy()
    # assure that 'idx_order' are the first indices
    for x in idx_fixed:
        idx_data.remove(x)
    idx_srt = idx_fixed + idx_data
    data = data.reorder_levels(order=idx_srt)
    return data


def collect_experiments(analysis_suffix, avrg, path2dumped_experiment):
    data = pd.DataFrame
    for a in avrg:
        exp = UQExperiment.read_experiment(
            path2dumped_experiment,
            stage=Stage.POST_ANALYSIS,
            suffix=analysis_suffix,
            avrg_technique=a,
        )

        # initialize temp dataframe that contains all data of one experiment
        exp_df = pd.DataFrame

        # convert dict of dataframes to one dataframe
        for qoi, qoi_df in exp.uq_output.items():
            qoi_df = add_index(df=qoi_df, index_name="qoi", value=qoi)
            qoi_df = remove_none_index_levels(qoi_df)
            exp_df = concat_unequal_dataframes(exp_df, qoi_df)

        exp_df = add_index(df=exp_df, index_name="avrg", value=a)
        data = concat_unequal_dataframes(data, exp_df)
    return data


def plot_sensitivity_index_wo_interactions(
    df: pd.DataFrame,
    s_index: str,
    qoi_map: dict,
    avrg: List[str],
    param_map: dict,
    y_label: str,
    y_limit: List[float] = [0, 1],
    y_ticks: List[float] = [0, 1],
    n_subplt_cols: int = 2,
    subplt_row_wise: bool = True,
    x_tick_rot: float = 0,
    sup_title: str = None,
    colors=None,
    patches=None,
    legend_loc=[],
    legend_title=None,
    legend_ncols: int = 1,
):
    """
    Generates a plot for first order or total sensitivity indices (SALib output
    based). Second order indices are not supported.
    Creates a bar plot (x-axis: parameter, y-axis: sensitivity) or a line plot
    if the sensitivity is given for each pedestrian.

    Each ...            gets a separate     ...
    QoI                 =>                  subplot
    Parameter           =>                  x-tick
    Averaging techniuqe =>                  color / bar / line
    Run                 =>                  bar / line (same color)
    Pedestrian Id       =>                  data point on the x-axis

    Arguments
    ----------
    df:                 Dataframe with all data to be plotted; indices must
                        contain 'avrg', 'qoi', 'run_id', 'pedestrianId'. If
                        one / several of these indices do not match the given
                        data, their values should be np.nan
                        Columns must contain 'param' and at least the column
                        that contains the data to be plotted, e.g., 'ST'
    s_index:            Sensitivity index to be plotted, i.e. 'ST' or 'S1'
    qoi_map:            Dictionary mapping the original quantities of interest
                        (key) to the displayed name as subtitle in the plot
                        (value). This also defines which parameters are plotted
                        at all.
    avrg:               Averaging technique(s); corresponds to the analysis
                        setting "--averagerepetitions", i.e. 'A', 'B', 'C', 'D'
    param_map:          Dictionary mapping the original parameter names
                        (key) to the displayed name in the plot (value)
    y_label:            Label of the y-axis
    y_limit:            Limits of the y-axis
    n_subplt_cols:      Number of columns among the subplots; cannot be greater
                        than the total number of subplots
    subplt_row_wise:    If True: arranges subplots row-wise, i.e. fills row x
                        before row x + 1 is filled; If False: arranges subplots
                        column-wise
    x_tick_rot:         Rotation of x-tick labels
    sup_title:          Super / main title
    colors:             Color map / color list per dataset (avrg)

    Returns
    ----------
    Matplotlib pyplot figure.

    """
    df_ = df.copy()

    # plot settings
    if not colors:
        colors = matplotlib.cm.get_cmap("tab10").colors

    # subplot settings
    n_subplots = len(qoi_map)
    n_subplt_cols = min([n_subplt_cols, n_subplots])
    n_subplot_rows = m.ceil(n_subplots / n_subplt_cols)

    # bar plot
    bar_width = 0.9

    fig, ax = plt.subplots(
        # figsize=fig_size,
        # dpi=dpi,
        nrows=n_subplot_rows,
        ncols=n_subplt_cols,
    )

    if sup_title:
        fig.suptitle(sup_title)

    # index variables
    row = 0
    column = 0

    patches_ = []

    for qoi in qoi_map.keys():
        data_qoi = select_by_index_or_column_values(
            df=df_, level="qoi", select_values=[qoi]
        )

        ax_ = get_axes(
            ax,
            n_subplot_rows=n_subplot_rows,
            n_subplot_cols=n_subplt_cols,
            row=row,
            col=column,
        )

        n_a = len(avrg)
        for a, a_ in zip(avrg, range(n_a)):
            col = colors[a_]

            data_avrg = select_by_index_or_column_values(
                df=data_qoi, level="avrg", select_values=[a]
            )

            plot_per_run = not np.isnan(
                data_avrg.index.get_level_values("run_id")
            ).all()
            if plot_per_run:
                alpha = 0.3
            else:
                alpha = 1

            n_params = len(param_map.keys())
            x_super = list(range(n_params))

            for p, p_ in zip(param_map.keys(), range(n_params)):
                data_param = select_by_index_or_column_values(
                    df=data_avrg, level="param", select_values=[p]
                )

                for run in data_param.index.get_level_values(
                    "run_id"
                ).drop_duplicates():
                    data_run = select_by_index_or_column_values(
                        df=data_param, level="run_id", select_values=[run]
                    )

                    plot_per_pedestrian = not np.isnan(
                        data_run.index.get_level_values("pedestrianId")
                    ).all()
                    if plot_per_pedestrian:  # create line plot
                        data_run = data_run.sort_values("pedestrianId", ascending=True)
                        y = data_run[s_index].copy().__array__()
                        x = (
                            data_run.index.get_level_values("pedestrianId")
                            .copy()
                            .__array__()
                        )

                        # This does not work if the missing pedestrian has an id
                        # that is < x.min or > x.max
                        fill_missing_peds_with_nan = False
                        if fill_missing_peds_with_nan:
                            x_ = list(range(int(x.min()), int(x.max() + 1)))
                            for x__, i in zip(x_, range(len(x_))):
                                if x__ not in x:
                                    y = np.concatenate(
                                        (y[:i], np.array([np.nan]), y[i:]), axis=0
                                    )
                            x = np.array(x_)

                        # normalize to values between 0 and 1
                        x = (x - x.min()) / (x - x.min()).max()

                        # separate graphs for each parameter (same width as
                        # bar plot)
                        x = x * bar_width + (1 - bar_width) / 2

                        x = x_super[p_] - 0.5 + x
                        ax_.plot(x, y, color=col, lw=0.5, alpha=alpha)
                        x_bounds = [x.min(), x.max()]

                    else:  # create bar plot

                        x = p_ + bar_width * (-0.5 + 1 / (n_a * 2) + a_ / n_a)
                        y = data_run[s_index].copy()
                        ax_.bar(
                            x=x, height=y, width=bar_width / n_a, color=col, alpha=alpha
                        )
                        x_bounds = [
                            x_super[p_] - bar_width / 2,
                            x_super[p_] + bar_width / 2,
                        ]

                # plot only for the first time in the loop
                if a_ == 0:
                    ax_.fill_betweenx(
                        y=y_limit,
                        x1=x_bounds[0],
                        x2=x_bounds[1],
                        facecolor="k",
                        edgecolor="none",
                        alpha=0.1,
                    )
            if patches and len(patches_) < n_a:
                patches_.append(mpatches.Patch(color=col, label=patches[a_]))
        if qoi in legend_loc:
            ax_.legend(handles=patches_, title=legend_title, ncol=legend_ncols)

        ax_.set_xticks(x_super)
        ax_.set_xticklabels(param_map.values(), rotation=x_tick_rot)

        ax_.set_yticks(y_ticks)
        ax_.set_ylim(y_limit)
        ax_.set_ylabel(y_label)

        ax_.set_title(qoi_map[qoi])

        row, column = get_next_row_col_pair(
            row, column, n_subplot_rows, n_subplt_cols, subplt_row_wise
        )

    fig.tight_layout()

    return fig


def select_by_index_or_column_values(df: pd.DataFrame, level: str, select_values: list):
    df_ = df.copy()

    if level in df_.columns.to_list():
        df_ = df_[df_[level].isin(select_values)]
    elif level in df_.index.names.copy():
        df_.reset_index(level=level, inplace=True)
        df_ = df_[df_[level].isin(select_values)]
        df_.set_index(keys=level, append=True, drop=True, inplace=True)
    else:
        raise UserWarning(
            f"Column or index {level} not found in dataframe\n\n{df_.head()}."
        )

    return df_


def get_axes(ax, n_subplot_rows: int, n_subplot_cols: int, row: int, col: int):
    if n_subplot_rows + n_subplot_cols == 2:
        ax_ = ax
    elif n_subplot_rows <= 1 or n_subplot_cols <= 1:
        ax_ = ax[row + col]
    else:
        ax_ = ax[row, col]
    return ax_


def harmonize_indices(df1: pd.DataFrame, df2: pd.DataFrame, value=np.nan):
    """Brings indices of df1 and df2 in alignment. That is, adds indices from
    df2 that are not in df1 to df1 and vice versa. The added index values are
    defined by value.
    The indices are sorted by the final order of the indices of df1.
    """
    df_1 = df1.copy()
    df_2 = df2.copy()

    idx1 = set(df_1.index.names.copy())
    idx2 = set(df_2.index.names.copy())

    idx1_to_2 = [x for x in idx1 if x not in idx2]
    idx2_to_1 = [x for x in idx2 if x not in idx1]

    for idx in idx2_to_1:
        df_1 = add_index(df_1, idx, value=value)
    for idx in idx1_to_2:
        df_2 = add_index(df_2, idx, value=value)

    idx1 = df_1.index.names.copy()
    df_2 = df_2.reorder_levels(order=idx1)
    idx2 = df_2.index.names.copy()

    # This error should only occur if a dataframe has multiple indices with the
    # same name. In this case, the dataframe should be corrected.
    if idx1 != idx2:
        raise UserWarning(
            f"Indices are not equal {idx1} and {idx2}. Check dataframes for duplicated indices."
        )

    return df_1, df_2


def concat_unequal_dataframes(df1: pd.DataFrame, df2: pd.DataFrame):
    """Harmonizes dataframes and concatenates them. df1 is the base dataframe
    into which df2 is integrated."""

    if df1.empty:
        df_1 = df2.copy()
    else:
        df_1, df_2 = harmonize_indices(df1.copy(), df2.copy())
        df_1 = pd.concat([df_1.copy(), df_2.copy()])

    return df_1


def add_index(df: pd.DataFrame, index_name: str, value):
    df_ = df.copy()
    df_.insert(loc=0, column=index_name, value=value)
    df_.set_index(keys=index_name, append=True, drop=True, inplace=True)
    return df_


def remove_none_index_levels(df: pd.DataFrame):
    df_ = df.copy()
    idx_names = df_.index.names.copy()
    while None in idx_names:
        none_level = idx_names.index(None)
        df_ = df_.droplevel(level=none_level)
        idx_names = df_.index.names.copy()
    return df_


def get_fig_file_name(
    quantity, main, avrg_technique="", suffix="", file_extension="pdf"
):
    name = quantity.lower() + "_" + main.lower().replace(" ", "_")
    if avrg_technique != "":
        name += "_" + avrg_technique
    if suffix != "":
        name += "-" + suffix
    name += "." + file_extension
    return name


def save_fig(
    path2dumped_experiment,
    si,
    sup_title,
    avrg_technique="",
    analysis_suffix="",
    file_type="pdf",
):
    path2figs = os.path.join(path2dumped_experiment, fig_dir)
    if not os.path.exists(path2figs):
        os.makedirs(path2figs)
    file_name = get_fig_file_name(
        quantity=si,
        main=sup_title,
        avrg_technique=avrg_technique,
        suffix=analysis_suffix,
        file_extension=file_type,
    )
    path2file = os.path.join(path2figs, file_name)
    plt.savefig(path2file)


def get_next_row_col_pair(row, col, n_rows, n_cols, increase_row_wise=True):
    if increase_row_wise:
        col += 1
        if col == n_cols:
            col = 0
            row += 1
    else:
        row += 1
        if row == n_rows:
            row = 0
            col += 1

    return row, col


def pt_to_cm(a):
    return a * 0.0352778


# Paths
relPath2experiment = ".."
relPath2output = os.path.join(relPath2experiment, "output")
fig_dir = "figures"

# ToDo move this to vimuq/experiments/plot/style.py
# Settings related to plots
# other aspect ratios
ar_default = 6.4 / 4.8  # pyplot default (1.333)
ar_gold = 1.6181  # golden ratio a / b ~ phi
ar_wide = 5 / 3  # wide screen (1.667)
ar_wide_hd = 16 / 9  # wide screen hd (1.778)

# thesis
thesis = dict()
thesis["text.width"] = cm_to_inches(14.6979)
thesis["text.height"] = cm_to_inches(20.9370)
# figure width if multiple sub-figures are placed in the same row
thesis["fig.oneup"] = 1 * thesis["text.width"]
thesis["fig.twoup"] = 0.48 * thesis["text.width"]
thesis["fig.threeup"] = 0.32 * thesis["text.width"]
thesis["fig.fourup"] = 0.24 * thesis["text.width"]

# other
paper = dict()
paper["text.width"] = cm_to_inches(13.3331)

# often used titles
qoi_str = {
    "rank": "Individual exposure ranked in descending order",
    # "rank$(-E_{ped})$"
    "rank1": "Indiv.~exposure ranked in descending order",
    "rank2": "Rank of individual exposures in descending order",
    "rank3": "Rank of individual exposures",
    "rank4": "Rank of individual exposures\nin descending order",
    "pos": "Individual exposure by position in ascending order",
    "pos1": "Indiv. exposure by position in ascending order",
    "pos2": "Position in the queue",
    "max": "Maximum exposure",
    # "max$(E_{ped})$"
    "mean": "Average exposure where exposure $>0$",
    # "mean$(E_{p,\,0}), E_{p,\,0} = \{x \in E_{ped}:\, x > 0\}$"
    "counts": "Number of persons with exposure $>0$",
    # "$\mid E_{p,\,0} \mid, E_{p,\,0} = \{x \in E_{ped}:\, x > 0\}$"
}
