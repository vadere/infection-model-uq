# Plots total sensitivity indices (SI)
# - for averaged SI (A)
# - for non averaged (D), runs r=0,1,2

import os

from matplotlib.pyplot import *
from experiments.plot.imports import ar_wide, qoi_str

import vimuq
from vimuq.experiments.plot.plot import (
    PlotDataFrame,
    SubplotDescriptions,
    Plot,
    SensitivityPlot,
    MarkerDict,
)
from vimuq.uq.uq_experiment import UQExperiment, Stage


if __name__ == "__main__":
    # ---------------------------------
    # Experiment
    # ---------------------------------
    path2output = os.path.join("..", "output")
    experiment_id = "queue_long_uq_03_03"
    avrgs = ["A", "D"]

    file_ext = "pdf"

    # sub titles
    sub_titles = [
        "",  # qoi_str["pos"],
        "",  # qoi_str["max"],
        "",  # qoi_str["mean"],
        "",  # qoi_str["counts"],
    ]

    for avrg in avrgs:
        # ---------------------------------
        # Aggregate data
        # ---------------------------------
        path2dumped_experiment = vimuq.get_experiment_dir_name(
            dir_name=os.path.join(path2output, experiment_id), suffix=""
        )

        exp = UQExperiment.read_experiment(
            path2dumped_experiment, stage=Stage.POST_ANALYSIS, avrg_technique=avrg
        )

        exp_df = PlotDataFrame.uq_output_dict_to_df(uq_output_dict=exp.uq_output)

        # replace parameter values and set column "param" to index
        parameter_abbr = {
            "halfLife": "Half-life",  # "Half life",
            "initialRadius": "Init.~radius",  # "Init. radius",
            "airDispersionFactor": "Disp.~factor",  # "Air disp. factor",
        }
        for key, value in parameter_abbr.items():
            exp_df.replace({"param": key}, value, inplace=True)

        exp_df.set_index(keys="param", drop=True, append=True, inplace=True)

        d_base = SubplotDescriptions(yticks=[0, 1], ylim=[0, 1], ylabel="$S_{T}$")

        # ---------------------------------
        # Create plot
        # ---------------------------------
        sensitivity_plot = Plot(
            data=exp_df,
            # title="Degree of exposure",
            n_subplot_cols=2,
        )

        # Define index and color for one/multiple data sets in one subplot
        if "run_id" in sensitivity_plot.data.index.names:
            lvl = "run_id"  # for avrg D
            n_colors = (
                sensitivity_plot.data.index.get_level_values("run_id")
                .drop_duplicates()
                .size
            )
            # colors = [
            #     matplotlib.colors.to_rgb(x)
            #     for x in matplotlib.cm.Blues(np.linspace(0.6, 1, n_colors))
            # ]

            # three times the same color is on purpose, color matches with color
            # code for the competitive queue / mushroom scenario in
            # summary_sa_01.py
            colors = [matplotlib.colors.to_rgb(matplotlib.cm.GnBu(0.75))] * n_colors
        else:
            lvl = None  # for avrg A
            colors = matplotlib.cm.get_cmap("tab10").colors

        # Define subplots
        d = d_base.copy()
        d.update(title=sub_titles[0])
        sensitivity_plot.add_subplot(
            SensitivityPlot(
                main_value="pedIdKey_degExposure",
                type="line",
                idx_minor_x="pedestrianId",
                idx_data_series=lvl,
                colors_data_series=colors,
                descriptions=d,
                fill_minor_x_vals=[11],
                marker=MarkerDict(),
                show_xticks="minor",
            )
        )

        d = d_base.copy()
        d.update(title=sub_titles[1])
        sensitivity_plot.add_subplot(
            SensitivityPlot(
                main_value="pedIdKey_degExposure_max",
                type="bar",
                idx_data_series=lvl,
                colors_data_series=colors,
                descriptions=d,
            )
        )

        d = d_base.copy()
        d.update(title=sub_titles[2])
        sensitivity_plot.add_subplot(
            SensitivityPlot(
                main_value="pedIdKey_degExposure_mean_>_0",
                type="bar",
                idx_data_series=lvl,
                colors_data_series=colors,
                descriptions=d,
            )
        )

        d = d_base.copy()
        d.update(title=sub_titles[3])
        sensitivity_plot.add_subplot(
            SensitivityPlot(
                main_value="pedIdKey_degExposure_count_>_0",
                type="bar",
                idx_data_series=lvl,
                colors_data_series=colors,
                descriptions=d,
            )
        )

        sensitivity_plot.make_plot(
            figsize=[
                rcParams["figure.figsize"][0],
                rcParams["figure.figsize"][0] / ar_wide,
            ]
        )

        if avrg == "A":
            n_data_series = 1
        if avrg == "D":
            n_data_series = 3

        subplot_map = {
            0: {
                "xlabel": qoi_str["pos2"],
                "show_minor_x": True,
                "n_data_series": n_data_series,
                "n_xticks": 21,
            }
        }
        sensitivity_plot.shift_xlabels_to_top(subplot_map=subplot_map)

        # add items a), b), c), d)
        for n, ax in enumerate(sensitivity_plot.figure.get_axes()):
            ax.text(
                -0.125,
                1.15,  # string.ascii_lowercase[n] +
                "b)",
                transform=ax.transAxes,
            )

        axes = sensitivity_plot.figure.get_axes()
        for ax in axes:
            # set y-legend
            ax.set_ylabel("Total sensitivity index")

        sensitivity_plot.figure.subplots_adjust(
            bottom=0.05, top=0.9, wspace=0.25, hspace=0.65, right=0.98, left=0.06
        )

        sensitivity_plot.figure.show()

        sensitivity_plot.save(
            parent_dir=path2dumped_experiment,
            name="st_degree_of_exposure_" + avrg,
            file_extension=file_ext,
        )

        # calculate min max ranges obtained for each qoi for repeated runs
        if avrg == "D":

            groups = exp_df.groupby(by="qoi")

            # exposure by position in the queue
            qoi = "pedIdKey_degExposure"
            g = groups.get_group(qoi)
            g = g[["ST"]]
            g.reset_index(["rank", "qoi"], drop=True, inplace=True)

            min = g.groupby(["param", "pedestrianId"]).min()
            max = g.groupby(["param", "pedestrianId"]).max()
            mean = g.groupby(["param", "pedestrianId"]).mean()

            diff = max.subtract(min, axis="index", level=["param", "pedestrianId"])
            diff.rename(columns={"ST": "max-min"}, inplace=True)

            diff["mean-min"] = mean.subtract(min, axis="index", level=["param"])
            diff["max-mean"] = max.subtract(mean, axis="index", level=["param"])

            repetitions = g.index.get_level_values("run_id").drop_duplicates().__len__()

            print("\n\n---------------------------------------------------")
            print(f"Max difference in {str(repetitions)} repetitions of {qoi}:")
            print("---------------------------------------------------")
            print(diff.groupby(by="param").max())
            #                     ST
            # param
            # Half-life     0.003573 --> fine
            # Init.~radius  0.097274 --> fine
            # Disp.~factor  0.403223 --> large difference, so I check whether this is an exception:
            print(
                diff.groupby("param")
                .get_group("Disp.~factor")
                .sort_values("max-min", ascending=False)
                .dropna()
            )

            # maximum exposure, average exposure, e > 0, number of exposures, e > 0
            for qoi in [
                "pedIdKey_degExposure_max",
                "pedIdKey_degExposure_mean_>_0",
                "pedIdKey_degExposure_count_>_0",
            ]:
                g = groups.get_group(qoi)
                g = g[["ST"]]
                g.reset_index(["rank", "qoi", "pedestrianId"], drop=True, inplace=True)
                repetitions = (
                    g.index.get_level_values("run_id").drop_duplicates().__len__()
                )
                min = g.groupby(["param"]).min()
                max = g.groupby(["param"]).max()
                diff = max.subtract(min, axis="index", level=["param"])
                print("\n\n---------------------------------------------------")
                print(f"Max difference in {str(repetitions)} repetitions of {qoi}:")
                print("---------------------------------------------------")
                print(diff.groupby(by="param").max())
