# Plots three different plots to evaluate the convergence of the total
# sensitivity indices (averaged simulation results (A)).
# - plot 1: results for each sample size n (n=128, ..., 4096)
# - plot 2: results for sample size n_{i+1} subtracted from the results for
#           sample size n_{i} (absolute value)
# - plot 3: results for sample size n_{i+1} subtracted from the results for
#           sample size n=4096 (absolute value)

import os
import pandas as pd
import math as m

from matplotlib.pyplot import *
from experiments.plot.imports import ar_wide, qoi_str

import vimuq
from vimuq.experiments.plot.plot import (
    PlotDataFrame,
    SubplotDescriptions,
    Plot,
    SensitivityPlot,
    LegendDict,
    MarkerDict,
)
from vimuq.uq.uq_experiment import UQExperiment, Stage


if __name__ == "__main__":
    # ---------------------------------
    # Experiment
    # ---------------------------------
    path2output = os.path.join("..", "output")
    experiment_id = "queue_long_uq_03_03"
    avrg = "A"
    n_samples = [
        "128",
        "256",
        "512",
        "1024",
        "2048",
        "4096",
    ]

    # red, horizontal line (stopping criterion for convergence)
    xstop = [-0.5, 2.5]
    ystop = [0.01, 0.01]

    # color range (opacity 0 ... 1)
    col_range = [0.4, 1]

    file_ext = "pdf"

    # add sup titles or not
    print_titles = False

    # sub titles
    sub_titles = [
        qoi_str["pos"],
        qoi_str["max"],
        qoi_str["mean"],
        qoi_str["counts"],
    ]

    # ---------------------------------
    # Aggregate data
    # ---------------------------------
    agg_level = "n_samples"
    path2dumped_experiment = vimuq.get_experiment_dir_name(
        dir_name=os.path.join(path2output, experiment_id), suffix=""
    )
    exp_df = pd.DataFrame
    for a in n_samples:
        exp = UQExperiment.read_experiment(
            path2dumped_experiment,
            stage=Stage.POST_ANALYSIS,
            suffix="",
            n_samples=a,
            avrg_technique=avrg,
        )

        temp = PlotDataFrame.uq_output_dict_to_df(uq_output_dict=exp.uq_output)
        temp = PlotDataFrame.add_index(df=temp, index_name=agg_level, value=a)
        exp_df = PlotDataFrame.concat_unequal_dataframes(exp_df, temp)

    # replace parameter values and set column "param" to index
    parameter_abbr = {
        "halfLife": "Half-life",  # "Half life",
        "initialRadius": "Init.~radius",  # "Init. radius",
        "airDispersionFactor": "Disp.~factor",  # "Air disp. factor",
    }
    for key, value in parameter_abbr.items():
        exp_df.replace({"param": key}, value, inplace=True)

    exp_df.set_index(keys="param", drop=True, append=True, inplace=True)

    # ---------------------------------
    # Calculate measure for convergence (Difference previous dataset)
    # ---------------------------------
    conv_groupby = "n_samples"
    conv_idx = "difference"
    exp_g = exp_df.groupby(by=conv_groupby)
    conv1 = pd.DataFrame
    conv2 = pd.DataFrame
    n_ref = n_samples[-1]
    n_ref_log2 = int(m.log2(int(n_ref)))
    ref = exp_g.get_group(n_ref).reset_index(level=conv_groupby, drop=True).copy()
    for n in range(len(n_samples) - 1):
        temp1 = (
            exp_g.get_group(n_samples[n])
            .reset_index(level=conv_groupby, drop=True)
            .copy()
        )
        temp2 = (
            exp_g.get_group(n_samples[n + 1])
            .reset_index(level=conv_groupby, drop=True)
            .copy()
        )

        # calc convergence measure 1
        diff1 = temp1.subtract(temp2).abs()

        diff1.insert(loc=0, column=conv_idx, value=f"conv_{n_samples[n]}")
        diff1.set_index(conv_idx, append=True, drop=True, inplace=True)

        conv1 = PlotDataFrame.concat_unequal_dataframes(conv1, diff1)

        # calc convergence measure 2
        diff2 = temp1.subtract(ref).abs()

        diff2.insert(loc=0, column=conv_idx, value=f"conv_{n_samples[n]}")
        diff2.set_index(conv_idx, append=True, drop=True, inplace=True)

        conv2 = PlotDataFrame.concat_unequal_dataframes(conv2, diff2)

    # ---------------------------------
    # Create plot - results for various sample sizes
    # ---------------------------------
    plot_level = "n_samples"
    n_level_vals = exp_df.index.get_level_values(plot_level).drop_duplicates().__len__()

    d_base = SubplotDescriptions(yticks=[0, 1], ylim=[0, 1], ylabel="$S_{T}$")

    if print_titles:
        title = (
            "Total sensitivity of the degree of exposure $E$ for different sample sizes $n$, $n_{skip}=2^{"
            + str(n_ref_log2)
            + "}$"
        )
    else:
        title = None
    sensitivity_plot = Plot(
        data=exp_df,
        title=title,
        n_subplot_cols=2,
    )

    # Legend
    n_samples_log2 = [int(m.log2(int(i))) for i in n_samples]
    n_samples_dict = dict(zip(n_samples, n_samples_log2))
    legend = LegendDict(
        title="Sample size $n = 2^a$, where a:",
        ncols=n_level_vals,
        entrydict=n_samples_dict,
    )

    colors = [
        matplotlib.colors.to_rgb(x)
        for x in matplotlib.cm.Blues(
            np.linspace(col_range[0], col_range[1], n_level_vals)
        )
    ]

    # Define subplots
    d = d_base.copy()
    d.update(title=sub_titles[0])
    sensitivity_plot.add_subplot(
        SensitivityPlot(
            main_value="pedIdKey_degExposure",
            type="line",
            idx_minor_x="pedestrianId",
            idx_data_series=plot_level,
            colors_data_series=colors,
            descriptions=d,
            fill_minor_x_vals=[11],
            marker=MarkerDict(size=1),
            show_xticks="minor",
        )
    )

    d = d_base.copy()
    d.update(title=sub_titles[1])
    sensitivity_plot.add_subplot(
        SensitivityPlot(
            main_value="pedIdKey_degExposure_max",
            type="bar",
            idx_data_series=plot_level,
            colors_data_series=colors,
            descriptions=d,
            legend=legend,
        )
    )

    d = d_base.copy()
    d.update(title=sub_titles[2])
    sensitivity_plot.add_subplot(
        SensitivityPlot(
            main_value="pedIdKey_degExposure_mean_>_0",
            type="bar",
            idx_data_series=plot_level,
            colors_data_series=colors,
            descriptions=d,
        )
    )

    d = d_base.copy()
    d.update(title=sub_titles[3])
    sensitivity_plot.add_subplot(
        SensitivityPlot(
            main_value="pedIdKey_degExposure_count_>_0",
            type="bar",
            idx_data_series=plot_level,
            colors_data_series=colors,
            descriptions=d,
        )
    )

    sensitivity_plot.make_plot(
        figsize=[
            rcParams["figure.figsize"][0],
            rcParams["figure.figsize"][0] / ar_wide,
        ]
    )

    sensitivity_plot.figure.show()

    sensitivity_plot.save(
        parent_dir=path2dumped_experiment,
        name="st_convergence_1_" + avrg,
        file_extension=file_ext,
    )

    # ---------------------------------
    # Create plot - convergence 1
    # ---------------------------------
    plot_level = conv_idx
    n_level_vals = conv1.index.get_level_values(plot_level).drop_duplicates().__len__()

    d_base = SubplotDescriptions(
        yticks=[0, ystop[0], 0.1],
        ylim=[0, 0.1],
        ylabel="$\mid S_{T}(n_1) - S_{T}(n_2) \mid$",
    )

    if print_titles:
        title = (
            "Difference of $S_{T}$ for degree of exposure $E$, where $n_1=2^a$, $n_2=2^{a+1}$, $n_{skip}=2^{"
            + str(n_ref_log2)
            + "}$"
        )
    else:
        title = None
    sensitivity_plot = Plot(
        data=conv1,
        title=title,
        n_subplot_cols=2,
    )

    # Legend
    ldg = conv1.index.get_level_values(level=conv_idx).drop_duplicates().to_list()
    ldg_log2 = [int(m.log2(int(i.split(sep="_")[1]))) for i in ldg]
    ldg_dict = dict(zip(ldg, ldg_log2))
    legend = LegendDict(
        title="Sample size $n_1=2^a$, where a:", ncols=n_level_vals, entrydict=ldg_dict
    )

    colors = [
        matplotlib.colors.to_rgb(x)
        for x in matplotlib.cm.Blues(
            np.linspace(col_range[0], col_range[1], n_level_vals)
        )
    ]

    # Define subplots
    d = d_base.copy()
    d.update(title=sub_titles[0])
    sensitivity_plot.add_subplot(
        SensitivityPlot(
            main_value="pedIdKey_degExposure",
            type="line",
            idx_minor_x="pedestrianId",
            idx_data_series=plot_level,
            colors_data_series=colors,
            descriptions=d,
            fill_minor_x_vals=[11],
            marker=MarkerDict(size=1),
            show_xticks="minor",
        )
    )

    d = d_base.copy()
    d.update(title=sub_titles[1])
    sensitivity_plot.add_subplot(
        SensitivityPlot(
            main_value="pedIdKey_degExposure_max",
            type="bar",
            idx_data_series=plot_level,
            colors_data_series=colors,
            descriptions=d,
            legend=legend,
        )
    )

    d = d_base.copy()
    d.update(title=sub_titles[2])
    sensitivity_plot.add_subplot(
        SensitivityPlot(
            main_value="pedIdKey_degExposure_mean_>_0",
            type="bar",
            idx_data_series=plot_level,
            colors_data_series=colors,
            descriptions=d,
        )
    )

    d = d_base.copy()
    d.update(title=sub_titles[3])
    sensitivity_plot.add_subplot(
        SensitivityPlot(
            main_value="pedIdKey_degExposure_count_>_0",
            type="bar",
            idx_data_series=plot_level,
            colors_data_series=colors,
            descriptions=d,
        )
    )

    sensitivity_plot.make_plot(
        figsize=[
            rcParams["figure.figsize"][0],
            rcParams["figure.figsize"][0] / ar_wide,
        ]
    )

    fig2 = sensitivity_plot.get_fig()
    for ax in fig2.axes:
        ax.plot(xstop, ystop, color="r", lw=0.5, ls="--")

    sensitivity_plot.figure.show()

    sensitivity_plot.save(
        parent_dir=path2dumped_experiment,
        name="st_convergence_2_" + avrg,
        file_extension=file_ext,
    )

    # ---------------------------------
    # Create plot - convergence 3
    # ---------------------------------
    plot_level = conv_idx
    n_level_vals = conv2.index.get_level_values(plot_level).drop_duplicates().__len__()

    d_base = SubplotDescriptions(
        yticks=[
            0,
            ystop[0],
            0.1,
        ],
        ylim=[0, 0.1],
        ylabel="$\mid S_{T}(n_1) - S_{T}(n_2) \mid$",
    )

    if print_titles:
        title = (
            "Difference of $S_{T}$ for degree of exposure $E$, where $n_1=2^a$, $n_2=2^{"
            + str(n_ref_log2)
            + "}$, $n_{skip}=2^{"
            + str(n_ref_log2)
            + "}$"
        )
    else:
        title = None
    sensitivity_plot = Plot(
        data=conv2,
        title=title,
        n_subplot_cols=2,
    )

    # Legend
    ldg = conv2.index.get_level_values(level=conv_idx).drop_duplicates().to_list()
    ldg_log2 = [int(m.log2(int(i.split(sep="_")[1]))) for i in ldg]
    ldg_dict = dict(zip(ldg, ldg_log2))
    legend = LegendDict(
        title="Sample size $n_1=2^a$, where a:", ncols=n_level_vals, entrydict=ldg_dict
    )

    colors = [
        matplotlib.colors.to_rgb(x)
        for x in matplotlib.cm.Blues(
            np.linspace(col_range[0], col_range[1], n_level_vals)
        )
    ]

    # Define subplots
    d = d_base.copy()
    d.update(title=sub_titles[0])
    sensitivity_plot.add_subplot(
        SensitivityPlot(
            main_value="pedIdKey_degExposure",
            type="line",
            idx_minor_x="pedestrianId",
            idx_data_series=plot_level,
            colors_data_series=colors,
            descriptions=d,
            fill_minor_x_vals=[11],
            marker=MarkerDict(size=1),
            show_xticks="minor",
        )
    )

    d = d_base.copy()
    d.update(title=sub_titles[1])
    sensitivity_plot.add_subplot(
        SensitivityPlot(
            main_value="pedIdKey_degExposure_max",
            type="bar",
            idx_data_series=plot_level,
            colors_data_series=colors,
            descriptions=d,
            legend=legend,
        )
    )

    d = d_base.copy()
    d.update(title=sub_titles[2])
    sensitivity_plot.add_subplot(
        SensitivityPlot(
            main_value="pedIdKey_degExposure_mean_>_0",
            type="bar",
            idx_data_series=plot_level,
            colors_data_series=colors,
            descriptions=d,
        )
    )

    d = d_base.copy()
    d.update(title=sub_titles[3])
    sensitivity_plot.add_subplot(
        SensitivityPlot(
            main_value="pedIdKey_degExposure_count_>_0",
            type="bar",
            idx_data_series=plot_level,
            colors_data_series=colors,
            descriptions=d,
        )
    )

    sensitivity_plot.make_plot(
        figsize=[
            rcParams["figure.figsize"][0],
            rcParams["figure.figsize"][0] / ar_wide,
        ]
    )

    fig3 = sensitivity_plot.get_fig()
    for ax in fig3.axes:
        ax.plot(xstop, ystop, color="r", lw=0.5, ls="--")

    sensitivity_plot.figure.show()

    sensitivity_plot.save(
        parent_dir=path2dumped_experiment,
        name="st_convergence_3_" + avrg,
        file_extension=file_ext,
    )
