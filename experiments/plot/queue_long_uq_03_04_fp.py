import os

import matplotlib
import numpy as np
import pandas as pd

import vimuq
from experiments.plot.imports import qoi_str
from vimuq.experiments.plot.plot import (
    PlotDataFrame,
    SamplePlot,
    ForwardUQPlot,
    BasicPlot,
)
from vimuq.uq.uq_experiment import UQExperiment, Stage

import matplotlib.pyplot as plt

from vimuq.util.math import round_up_to_xth_place

if __name__ == "__main__":

    # ---------------------------------
    # Experiment
    # ---------------------------------
    path2output = os.path.join("..", "output")
    experiment_id = "queue_long_uq_03_04"
    avrgs = ["A"]

    file_ext = "pdf"

    # color range (opacity 0 ... 1)
    col_range = [0.4, 1]

    for avrg in avrgs:
        # ---------------------------------
        # Aggregate data
        # ---------------------------------
        path2dumped_experiment = vimuq.get_experiment_dir_name(
            dir_name=os.path.join(path2output, experiment_id), suffix=""
        )

        exp = UQExperiment.read_experiment(
            path2dumped_experiment, stage=Stage.POST_ANALYSIS, avrg_technique=avrg
        )

        exp_df = PlotDataFrame.uq_output_dict_to_df(uq_output_dict=exp.uq_output)

        # ---------------------------------
        # Plot samples
        # ---------------------------------
        # the samples for avrg == A and D are the same
        if False and avrg == "A":
            label_map = {
                "halfLife": "Half-life",
                "initialRadius": "Initial radius",
                "airDispersionFactor": "Dispersion factor",
            }

            plot = SamplePlot(experiment=exp, label_map=label_map)
            plot.make(marker_size=2)
            plot.figure.subplots_adjust(wspace=0.35, hspace=0.35)
            plot.save(
                parent_dir=path2dumped_experiment,
                name="samples",
                file_extension=file_ext,
            )

        # prepare data
        # fill missing values
        arrays = [[11], ["pedIdKey_degExposure"], [pd.NA]]

        fill_index = pd.MultiIndex.from_arrays(
            arrays, names=("pedestrianId", "qoi", "rank")
        )
        fill_cols = exp_df.columns
        fill_df = pd.DataFrame(data=None, index=fill_index, columns=fill_cols)

        exp_df = pd.concat([exp_df, fill_df])

        # ---------------------------------
        # Interval plot: qoi vs. exposure plot
        # ---------------------------------
        ci_plot_props = {
            "pedIdKey_degExposure": {
                "x": "pedestrianId",
                "sort_x_by": "pedestrianId",
                "x_axis": {
                    "label": qoi_str["pos"],
                    "lim": [1, 21],
                },
                "y": ("degreeOfExposure-PID8", "mean"),
                "y_fill": [
                    ("degreeOfExposure-PID8", "10%"),
                    ("degreeOfExposure-PID8", "90%"),
                ],
                "y_axis": {
                    "label": "Exposure",
                    "lim": [0, 3000],
                    "tick_format": ["sci", (0, 0)],
                },
                "legend": "Mean, CI $90\%$",
            },
            "pedIdKey_degExposure_rank": {
                "x": "rank",
                "sort_x_by": "rank",
                "x_axis": {
                    "label": qoi_str["rank"],
                    "lim": [1, 20],
                },
                "y": ("degreeOfExposure-PID8", "mean"),
                "y_fill": [
                    ("degreeOfExposure-PID8", "10%"),
                    ("degreeOfExposure-PID8", "90%"),
                ],
                "y_axis": {
                    "label": "Exposure",
                    "lim": [0, 3000],
                    "tick_format": ["sci", (0, 0)],
                },
                "legend": "Mean, CI $90\%$",
            },
        }

        ci_plot = ForwardUQPlot(df=exp_df, axis_properties=ci_plot_props, n_cols=2)
        ci_plot.make_plot(fig_rel_height=0.75, type="line")
        ci_plot.figure.subplots_adjust(wspace=0.35, bottom=0.19)

        ci_plot.save(
            parent_dir=path2dumped_experiment,
            name="line_mean_ci90_doe_by_id_and_rank" + "_" + avrg,
        )

        # ---------------------------------
        # PDF/hist: qoi vs. counts plot
        # ---------------------------------
        scalar_qois = [k for k, v in exp.sim_output.items() if v.index.nlevels == 1]
        output = pd.concat(
            [
                (exp.sim_output[qoi].rename(columns={"degreeOfExposure-PID8": qoi}))
                for qoi in scalar_qois
            ],
            axis=1,
        )

        hist_plot_props = {
            "pedIdKey_degExposure_max": {
                "bins": 100,
                "x_axis": {
                    "label": qoi_str["max"],
                    "lim": [
                        0,
                        round_up_to_xth_place(
                            output["pedIdKey_degExposure_max"].max(), place=3
                        ),
                    ],
                },
                "y_axis": {
                    "label": "Counts",
                },
            },
            "pedIdKey_degExposure_count_>_0": {
                "bins": np.arange(
                    output["pedIdKey_degExposure_count_>_0"].min() - 1,
                    output["pedIdKey_degExposure_count_>_0"].max() + 3,
                )
                - 0.5,  # shift bins to the left by half a bin
                "x_axis": {
                    "label": qoi_str["counts"],
                },
                "y_axis": {
                    "label": "Counts",
                },
            },
            "pedIdKey_degExposure_mean_>_0": {
                "bins": 100,
                "x_axis": {
                    "label": qoi_str["mean"],
                    "lim": [
                        0,
                        round_up_to_xth_place(
                            output["pedIdKey_degExposure_mean_>_0"].max()
                        ),
                    ],
                },
                "y_axis": {
                    "label": "Counts",
                },
            },
            "pedIdKey_degExposure_sum": {
                "bins": 100,
                "x_axis": {
                    "label": "Sum exposure",
                    "lim": [
                        0,
                        round_up_to_xth_place(output["pedIdKey_degExposure_sum"].max()),
                    ],
                },
                "y_axis": {
                    "label": "Counts",
                },
            },
        }

        n_cols = 2

        hist_plot = ForwardUQPlot(
            df=output, axis_properties=hist_plot_props, n_cols=n_cols
        )
        hist_plot.make_plot(type="hist")
        hist_plot.figure.subplots_adjust(wspace=0.35, hspace=0.35)
        hist_plot.save(
            parent_dir=path2dumped_experiment,
            name="hist_doe_max_count_mean_sum" + "_" + avrg,
        )

        # ---------------------------------
        # PDF/hist and kernel density estimator qoi vs. counts plot
        # ---------------------------------
        kde_plot_props = hist_plot_props.copy()
        for _, v in kde_plot_props.items():
            v["y_axis"]["label"] = "Rel.~frequency"
            v["y_axis"]["tick_format"] = ["sci", (0, 0)]

        kde_plot = ForwardUQPlot(
            df=output, axis_properties=hist_plot_props, n_cols=n_cols
        )
        kde_plot.make_plot(type="hist", hist_density=True)
        kde_plot.make_plot(type="kde", skip_qoi=["pedIdKey_degExposure_count_>_0"])
        kde_plot.figure.subplots_adjust(wspace=0.35, hspace=0.35)
        kde_plot.save(
            parent_dir=path2dumped_experiment,
            name="kde_doe_max_count_mean_sum" + "_" + avrg,
        )

        # ---------------------------------
        # Summary of the most interesting subplots
        # ---------------------------------
        n_cols = 2
        summary_plot = BasicPlot()

        summary_plot.figure, axes_s = plt.subplots(nrows=2, ncols=n_cols)
        axes = axes_s.flatten()

        # subplot 1
        qoi = "pedIdKey_degExposure_rank"
        group = exp_df.groupby(["qoi"]).get_group(name=qoi).copy()
        ForwardUQPlot.make_line_subplot(
            ax=axes[0], group=group, props=ci_plot_props[qoi]
        )

        # subplot 2
        qoi = "pedIdKey_degExposure_max"
        ForwardUQPlot.make_hist_subplot(
            ax=axes[1], data=output[qoi], props=hist_plot_props[qoi]
        )

        # subplot 3
        qoi = "pedIdKey_degExposure_count_>_0"
        ForwardUQPlot.make_hist_subplot(
            ax=axes[2], data=output[qoi], props=hist_plot_props[qoi]
        )

        # subplot 4
        qoi = "pedIdKey_degExposure_mean_>_0"
        ForwardUQPlot.make_hist_subplot(
            ax=axes[3], data=output[qoi], props=hist_plot_props[qoi]
        )

        summary_plot.figure.subplots_adjust(wspace=0.35, hspace=0.35)
        summary_plot.save(
            parent_dir=path2dumped_experiment,
            name="summary_uq" + "_" + avrg,
        )

        # ---------------------------------
        # Convergence for summary of the most interesting subplots / compare
        # results for different sample sizes
        # ---------------------------------
        n_cols = 2
        n_samples = [250, 500, 1000]
        colors = [
            matplotlib.colors.to_rgb(x)
            for x in matplotlib.cm.Blues(
                np.linspace(col_range[0], col_range[1], len(n_samples))
            )
        ]
        colors = ["tab:blue", "tab:orange", "tab:green"]

        conf_plot = BasicPlot()
        conf_plot.figure, axes_s = plt.subplots(nrows=2, ncols=n_cols)
        axes = axes_s.flatten()

        # subplot 1
        qoi = "pedIdKey_degExposure_rank"
        for n, color in zip(n_samples, colors):

            # ---- evaluate statistics for sample sizes n ----
            data = exp.sim_output[qoi].copy()
            data = data[data.index.get_level_values("id") < n]

            keep_idx = "id"
            groupby_idx = list(data.index.names)
            groupby_idx.remove(keep_idx)
            data_ = data.reset_index(level=groupby_idx)

            if len(groupby_idx) > 0:
                data_ = data_.groupby(groupby_idx)
            temp_stats = data_.describe(
                percentiles=[0.05, 0.1, 0.25, 0.5, 0.75, 0.9, 0.95]
            )

            if len(groupby_idx) == 0:
                temp_stats = pd.DataFrame(
                    data=temp_stats.unstack().values,
                    index=pd.MultiIndex.from_product(
                        [temp_stats.columns.to_list(), temp_stats.index.values]
                    ),
                ).T

            statistics = temp_stats
            # ---- done with evaluating statistics for sample sizes n ----

            properties = ci_plot_props[qoi].copy()
            properties["legend"] = ci_plot_props[qoi]["legend"] + f" for $n={n}$"
            ForwardUQPlot.make_line_subplot(
                ax=axes[0], group=statistics, props=properties, color=color
            )

        # subplot 2, 3, 4
        qois = [
            "pedIdKey_degExposure_max",
            "pedIdKey_degExposure_count_>_0",
            "pedIdKey_degExposure_mean_>_0",
        ]

        for i, qoi in enumerate(qois):
            for n, color in zip(n_samples, colors):
                properties = hist_plot_props[qoi].copy()
                properties["legend"] = f"$n={str(n)}$"
                properties["y_axis"]["label"] = "Cum.~rel.~frequency"
                properties["y_axis"]["tick_format"] = ["sci", (0, 0)]
                if qoi == "pedIdKey_degExposure_count_>_0":
                    ForwardUQPlot.make_hist_subplot(
                        ax=axes[i + 1],
                        data=output[qoi][0:n],
                        props=properties,
                        density=True,
                        color=color,
                        alpha=0.2,
                        cumulative=True,
                    )
                else:
                    ForwardUQPlot.make_kde_subplot(
                        ax=axes[i + 1],
                        data=output[qoi][0:n],
                        props=properties,
                        x_num=500,
                        color=color,
                        cumulative=True,
                    )

        conf_plot.figure.subplots_adjust(wspace=0.35, hspace=0.35)
        conf_plot.save(
            parent_dir=path2dumped_experiment,
            name="summary_uq_250-1000" + "_" + avrg,
        )

    print()
