import errno
import math
import os
import pickle as pkl
import warnings
from itertools import cycle

import matplotlib.pyplot as plt
import pandas as pd

from vimuq.experiments.plot.plot import Plot
from experiments.plot.imports import (
    ar_wide_hd,
    qoi_str,
)
from vimuq.uq.uq_experiment import UQExperiment

from matplotlib.pyplot import *


def save_fig(experiment_path, filename, plot, scenario=None, dpi="figure"):
    if scenario:
        path2dir = os.path.join(experiment_path, scenario, Plot._FIG_DIR)
    else:
        path2dir = os.path.join(experiment_path, Plot._FIG_DIR)
    if not os.path.exists(path2dir):
        os.makedirs(path2dir)
    path2file = os.path.join(path2dir, filename)
    plot.savefig(path2file, dpi=dpi)


def get_data(experiment_path, scenario, filename="result.pkl"):
    path_to_dir = os.path.join(experiment_path, scenario, UQExperiment.SUBDIR)
    path_to_file = os.path.join(path_to_dir, filename)
    if os.path.exists(path_to_file):
        with open(path_to_file, "rb") as inp:
            df = pkl.load(inp)
    else:
        raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), path_to_file)
    return list(df.values())[0]


def evaluate_close_contact(
    experiment_path, scenario, ids_label_map, save_output=True, file_ext="pdf"
):
    """
    Creates and saves a plot for exposure over time for the close contact
    scenario.
    scenario:       List of scenario names
    ids_label_map:  defines which samples / parameter variations are plotted
                    and how they are labelled in the plot;
                    dict with keys (ids of the samples to be considered) and
                    values (corresponding description that appears in the
                    legend)

    Returns reference exposure, i.e. the degree of exposure at time step
    t_ref_value.
    """

    # get data
    df = get_data(experiment_path=experiment_path, scenario=scenario)
    df.reset_index("id", inplace=True)

    # convert time steps to seconds
    df.reset_index(level="timeStep", inplace=True)
    sim_time_step_length = 0.4
    # shift time to 0, multiply with time step width
    df["time"] = (
        df["timeStep"]
        .subtract(df["timeStep"].values.min())
        .multiply(sim_time_step_length)
    )

    # drop irrelevant data
    # - data for various runs
    df.reset_index("run_id", inplace=True)
    if df["run_id"].drop_duplicates().values.size > 1:
        warnings.warn(
            "Results for more than one repetition present. "
            "I drop all but repetition 0."
        )
        df = df[df["run_id"] == 0]
    # - data for not required ids
    df = df[df["id"].isin(list(ids_label_map.keys()))]

    # add column for labels
    df["label"] = df["id"]
    if id == [0]:
        df.loc[df["label"] == 0, "label"] = "__nolegend__"
    else:
        for k, v in ids_label_map.items():
            df["label"].replace(k, v, inplace=True)

    # df column name of column with degree of exposure values
    doe = "absorbedPathogenLoad-PID5"

    # create plot
    page_width = 3.48642  # inches (8,85553 cm)
    fig, ax = plt.subplots(
        figsize=[
            page_width,
            page_width / ar_wide_hd,
        ]
    )
    use_y_linscale = ids_label_map.__len__() == 1

    groups = df.groupby(["id"])
    for name, grp in groups:
        ax = grp.plot(
            ax=ax,
            x="time",
            y=doe,
            label=grp.label.drop_duplicates().values[0],
        )

    # add horizontal lines for reference value
    ref_exposures = pd.DataFrame()
    t_ref_value = 10 * 60  # time in seconds
    for name, grp in groups:
        e_ = grp[
            (grp.time >= t_ref_value - sim_time_step_length / 2)
            & (grp.time <= t_ref_value + sim_time_step_length / 2)
        ][doe].values[0]
        ax.plot([0, t_ref_value], [e_, e_], color="k", ls=":", lw=1)
        ref_exposures_ = pd.DataFrame([{"t": t_ref_value, "exposure": e_, "id": name}])
        ref_exposures = pd.concat([ref_exposures, ref_exposures_])

    # add vertical line for reference value
    e_max = ref_exposures["exposure"].values.max()
    ax.plot([t_ref_value, t_ref_value], [0, e_max], color="k", ls=":", lw=1)

    # figure descriptions
    ax.set_xlabel("Time in s")
    ax.set_ylabel("Exposure")
    ax.set_xlim([0, math.ceil(df.time.values.max())])

    # y axis: scale, ticks, etc.
    # linear scale if only one data series is plotted
    if use_y_linscale:
        y_lim = [0, math.ceil(df[doe].values.max() * 2) / 2]
    # otherwise log scale
    else:
        ax.set_yscale("log")
        y_lim = [
            10 ** (math.floor(math.log10(ref_exposures["exposure"].values.min())) - 2),
            10 ** math.ceil(math.log10(df[doe].values.max())),
        ]
    ax.set_ylim(y_lim)

    # add description to horizontal reference lines
    x_offset = 10
    y_offset = 2
    for name, group in ref_exposures.groupby(["id"]):
        ref_exp = group["exposure"].values[0]
        if not use_y_linscale:
            y_offset = y_offset * 10 ** math.floor(math.log10(ref_exp))
        ax.text(
            x=x_offset + 0,
            y=ref_exp * 1.1,
            s="$E_{" + str(name + 1) + "}$",
            ha="left",
            va="bottom",
        )

    # legend
    if use_y_linscale:
        ax.get_legend().remove()
    else:
        ax.legend(loc="lower right")
    fig.subplots_adjust(left=0.15, bottom=0.2, right=0.95, top=0.95)
    if save_output:
        # save fig
        ids = "-".join(str(id) for id in ids_label_map.keys())
        filename = f"degree_of_exposure_time_ids{ids}." + file_ext
        save_fig(
            experiment_path=experiment_path,
            scenario=scenario,
            filename=filename,
            dpi=300,
            plot=plt,
        )

        # write txt file
        txt = os.path.join(
            experiment_path,
            scenario,
            Plot._FIG_DIR,
            "degree_of_exposure_reference_values.txt",
        )
        ref_exposures.to_string(buf=txt, float_format="{:0.4f}".format)

    return ref_exposures


def evaluate_doe_rank(
    experiment_path,
    scenarios,
    id_xlabel_map,
    ref,
    legend_loc,
    id_title_map=None,
    plot_type="individual",
    mean_threshold=None,
    average_repetitions_by_rank=False,
    file_ext="pdf",
):
    """
    Generates and saves a plot for the degree of exposure over the rank of the
    exposure.
    Plot consists of subplots:
    column-wise: the scenario
    row-wise: the sample / parameter variation

    scenarios:      List of scenario names
    id_xlabel_map:  defines which samples / parameter variations are plotted
                    and how they are labelled in the plot;
                    dict with keys (ids of the samples to be considered) and
                    values (corresponding description that appears as
                    description for each row)
    ref:            reference exposure (obtained from the close contact scenario
                    or any other value). Will appear as horizontal line.
    legend_loc:     defines the indices of the subplots in which a legend is
                    inserted
    id_title_map:   Dictionary defining the title for each column of subplots
    plot_type:      "individual", "mean"
    mean_threshold: None or float.
                    If mean_threshold is float and plot_type=="mean", the
                    function "mean" is applied to all values that are greater
                    than mean_threshold. Other values are not taken into account
                    for calculating the mean value. Typically, mean_threshold=0.
                    If mean_threshold==None and plot_type=="mean", the mean
                    takes into account all values.
                    Otherwise, no effect.
    average_repetitions_by_rank:
                    If True, averages the simulation result for multiple runs by
                    rank of exposure. That is, the highest exposure in run 0, 1,
                    ... is averaged. The second highest exposure in run 0,...
                    is averaged, etc.
    file_ext:       file extension, e.g. png, pdf, ...
    """

    df = pd.DataFrame()
    # aggregate data
    for scenario in scenarios:
        df_ = get_data(experiment_path=experiment_path, scenario=scenario)
        df_.insert(loc=0, column="scenario", value=scenario)
        df = pd.concat([df, df_])
    df.reset_index("id", inplace=True)

    # additional column to save the original order of scenarios
    df["order"] = df["scenario"]

    # rename scenarios:
    # this is how they appear in the figure
    scenarios_ = scenarios
    if id_title_map is None:
        replace_k_by_v = {
            "single_run_": "",
            "_long": "",
            "_": " ",
            "area": "room\n",
            "queue": "queue\n",
            "queue\ning": "queue",
        }
    else:
        replace_k_by_v = id_title_map

    for k, v in replace_k_by_v.items():
        df["scenario"].replace(k, v, regex=True, inplace=True)
        scenarios_ = [s_.replace(k, v) for s_ in scenarios_]
    df["scenario"] = df["scenario"].apply(lambda x: x[0].capitalize() + x[1:])
    scenarios_ = [s_.capitalize() for s_ in scenarios_]
    scenarios_ = [x for x in scenarios_ if "distancing" not in x]

    # split scenario type and measure into two separate columns
    keyword = "distancing"
    df["measure"] = df["scenario"]
    df.loc[df["measure"].str.contains(keyword), "measure"] = "distancing"
    df.loc[~df["measure"].str.contains(keyword), "measure"] = "normal"
    keep_vals = df["scenario"].drop_duplicates().values.tolist()
    keep_vals = [val for val in keep_vals if keyword not in val]
    for val in keep_vals:
        df.loc[df["scenario"].str.contains(val), "scenario"] = val

    df.reset_index("run_id", inplace=True)

    # drop irrelevant data
    doe = "degreeOfExposure-PID8"

    # instead of pedestrians, we consider only the rank
    indices = df.keys().tolist()
    [indices.remove(x) for x in [doe]]
    groups = df.groupby(indices)
    groups_ = pd.DataFrame()
    for name, group in groups:
        group.sort_values(by=doe, ascending=False, inplace=True)
        group["rank"] = np.arange(start=1, stop=group[doe].values.size + 1)
        groups_ = pd.concat([groups_, group])
    groups_.reset_index(level="pedestrianId", drop=True, inplace=True)
    df = groups_

    # - data for various runs
    if (
        df["run_id"].drop_duplicates().values.size > 1
        and not average_repetitions_by_rank
    ):
        warnings.warn(
            "Results for more than one repetition present. "
            "I drop all but repetition 0."
        )
        df = df[df["run_id"] == 0]

    elif average_repetitions_by_rank:
        # take the average by rank
        indices = df.keys().tolist()
        [indices.remove(x) for x in [doe, "run_id"]]
        mean_doe = df.groupby(indices)[doe].mean()
        df_ = mean_doe.to_frame()

        # restore the same structure as the original dataframe and overwrite
        # the old one
        df_.reset_index(level=indices, inplace=True)
        df = df_

    # remove data for not required ids
    df = df[df["id"].isin(list(id_xlabel_map.keys()))]

    # plot
    subpolt_rows = len(id_xlabel_map.keys())
    subplot_cols = len(keep_vals)
    sharex = "all"
    page_width = 7.5  # inches (19.05 cm)
    fig, axes = plt.subplots(
        nrows=subpolt_rows,
        ncols=subplot_cols,
        sharex=sharex,
        sharey="row",
        figsize=[page_width, page_width / ar_wide_hd],
    )

    bar_width = 1

    # define colors
    col_range = [0.6, 1]
    n_level_vals = df["measure"].drop_duplicates().values.size
    base_colors = [matplotlib.cm.Blues, matplotlib.cm.Oranges, matplotlib.cm.Greens]
    colors = []
    for base_color in base_colors:
        colors += [
            matplotlib.colors.to_rgb(x)
            for x in base_color(np.linspace(col_range[1], col_range[0], n_level_vals))
        ] * subplot_cols

    color_cycler = cycle(colors)

    x_ticks = np.arange(1, 22)
    x_tick_labels = [1, 21]
    x_tick_labels = [f"${str(x)}$" if x in x_tick_labels else "" for x in x_ticks]

    groups = df.groupby(["id", "scenario"])
    keys = groups.groups.keys()

    # sort keys such that they appear in the same order as scenarios
    keys_sorted = []
    for id in id_xlabel_map.keys():
        for s in scenarios_:
            keys_sorted.append((id, s))
    # check that all keys are actually in keys_sorted
    if not all([kk in keys_sorted for kk in keys]):
        raise ValueError
    if not all([kk in keys for kk in keys_sorted]):
        raise ValueError

    for i, ax, grp_idx in zip(range(keys.__len__()), fig.axes, keys_sorted):
        id = grp_idx[0]
        group = groups.get_group(name=grp_idx)

        # introduce dummy data (nan values)
        for m in df["measure"].drop_duplicates().values:
            if m not in group["measure"].values:
                dummy_group = group.copy()
                dummy_group["measure"] = m
                dummy_group[doe] = np.nan
                group = pd.concat([group, dummy_group.copy()])
        grp = group.groupby("measure", sort=False)
        for name, g in grp:
            corr = 0  # correction of the x-axis (needed when combining
            # pandas.plot and pyplot.plot; these two seem not to match)
            color = next(color_cycler)

            # plot one bar for each rank
            if plot_type == "individual":
                ax = g.plot.bar(
                    ax=ax,
                    x="rank",
                    y=doe,
                    rot=0,
                    label=name.capitalize(),
                    color=color,
                    width=bar_width,
                    legend=None,
                    use_index=False,
                )
                corr = 1  # I don't know why, but this shift to the right is necessary
                ax.set_xticks(x_ticks - 1)
                ax.set_xlim(
                    [
                        x_ticks[0] - corr - bar_width / 2,
                        x_ticks[-1] - corr + bar_width / 2,
                    ]
                )
                ax.axes.tick_params(axis="x", which="major", length=1.5)

            # plot one bar for mean of ranks
            elif plot_type == "mean":
                if mean_threshold is None:
                    g_mean = g
                else:
                    g_mean = g[(g[doe] > mean_threshold) | (g[doe].isna())]

                x_min = g_mean["rank"].values.min()
                x_max = g_mean["rank"].values.max()
                width = x_max - x_min + bar_width
                x_mean = (x_max - x_min) / 2 + x_min
                y_mean = g_mean[doe].mean()
                ax.bar(
                    x=x_mean,
                    height=y_mean,
                    width=width,
                    color=color,
                    label=name.capitalize(),
                )
                ax.set_xticks(x_ticks)
                ax.set_xlim([x_ticks[0] - bar_width / 2, x_ticks[-1] + bar_width / 2])

            ax.set_xticklabels(x_tick_labels)
            ax.set_xlabel(None)

        # add vertical lines indicating the reference values
        ref_exposure = ref[ref["id"] == id]["exposure"]
        ax.plot(
            list(ax.get_xlim()),
            [ref_exposure, ref_exposure],
            color="k",
            ls=":",
            lw=1,
            label="Close cont.",
        )

        # legend
        if i in legend_loc:  # not in upper right subplot
            ax.legend(loc="upper right")

        # horizontal titles
        if i in list(range(subplot_cols)):  # not in first row of subplots
            ax.title.set_text(grp_idx[1])

        # scientific notation of y axis
        ax.ticklabel_format(axis="y", style="sci", scilimits=(-2, 2))

    # vertical titles
    row_title = "out"  # "out"
    id_ = 0
    for i, ax in enumerate(axes.flat):
        if row_title == "out" and i % subplot_cols == 0:
            x = -11
            y = np.mean(ax.get_ylim())
            ha = "center"
            va = "center"
            rot = 90
            text = id_xlabel_map[id_]
            ax.text(x=x, y=y, ha=ha, va=va, s=text, rotation=rot)
        elif row_title == "in":
            x = ax.get_xlim()[1] * 0.95
            y = ax.get_ylim()[1] * 0.9
            ha = "right"
            va = "top"
            rot = 0
            text = id_xlabel_map[id_]
            ax.text(x=x, y=y, ha=ha, va=va, s=text, rotation=rot)

        if i % subplot_cols == 0:
            id_ += 1

    # x and y labels
    fig.supxlabel(qoi_str["rank2"], x=0.575)
    fig.supylabel("Exposure")
    plt.subplots_adjust(
        left=0.17,
        right=0.97,
        bottom=0.125,
        hspace=0.33,
        wspace=0.13,
    )

    # save fig
    info = ""
    if plot_type != "individual":
        info = "_" + plot_type
    if mean_threshold is not None:
        info += str(mean_threshold)

    ids = "-".join(str(x) for x in id_xlabel_map.keys())
    filename = "degree_of_exposure_rank" + info + "_ids" + ids + "." + file_ext
    save_fig(experiment_path=experiment_path, filename=filename, plot=plt, dpi=300)


def evaluate_walkway_exposure(
    experiment_path, scenarios, ref_exposures, file_formats=["txt"]
):
    doe = "degreeOfExposure-PID8"

    df = pd.DataFrame()
    # aggregate data
    for scenario in scenarios:
        df_ = get_data(experiment_path=experiment_path, scenario=scenario)
        df_.insert(loc=0, column="scenario", value=scenario)
        df = pd.concat([df, df_])
    df.reset_index("id", inplace=True)

    # ToDo process also multiple runs if required
    if df.index.get_level_values("run_id").drop_duplicates().size > 1:
        warnings.warn(
            "Results for more than one repetition present. "
            "I drop all but repetition 0."
        )
        df.reset_index("run_id", inplace=True)
        df = df[df["run_id"] == 0]

    scenario_str = "Scenario"
    run_str = "Repetition"

    max_str = "max"
    max_rel_str = "rel. max in %"
    mean_str = "mean"
    sum_str = "sum"
    count_str = "counts"
    count_rel_str = "rel. counts in %"

    replace_k_by_v = {"single_run_": "", "_": " " ""}
    for k, v in replace_k_by_v.items():
        df["scenario"].replace(k, v, regex=True, inplace=True)

    # create summary of data
    summary = pd.DataFrame()
    groups = df.groupby(["scenario", "id"])
    for name, group in groups:
        e_ref = ref_exposures[ref_exposures["id"] == name[1]]["exposure"].values[0]

        summary_ = pd.DataFrame()
        summary_[scenario_str] = [name[0].capitalize()]
        summary_[run_str] = [name[1] + 1]
        summary_[max_str] = [group[doe].max()]
        summary_[max_rel_str] = [group[doe].max() / e_ref * 100]
        summary_[mean_str] = [group[group[doe] > 0][doe].mean()]
        summary_[sum_str] = [group[doe].sum()]
        summary_[count_str] = [group[group[doe] > 0][doe].count()]
        summary_[count_rel_str] = [
            group[group[doe] > 0][doe].count()
            / df.index.get_level_values("pedestrianId").drop_duplicates().size
            * 100
        ]

        summary = pd.concat([summary, summary_])

    summary.set_index([scenario_str, run_str], inplace=True)
    s_ = summary.index.get_level_values(level=scenario_str).drop_duplicates()
    if s_.size == 1:
        summary.reset_index(scenario_str, drop=True, inplace=True)
    summary = summary.T

    # write output
    summary_path = os.path.join(experiment_path, UQExperiment.SUBDIR)
    if not os.path.exists(summary_path):
        os.makedirs(summary_path, exist_ok=False)

    if "txt" in file_formats:
        # write txt file
        txt = os.path.join(summary_path, "degree_of_exposure_walkway_scenarios.txt")
        summary.to_string(buf=txt, encoding="UTF-8", float_format="{:0.4f}".format)

    if "tex" in file_formats:
        # write latex table
        short_cap = "Simulation results for: " + ", ".join(s_.tolist())
        long_cap = "long cap"
        filename = "degree_of_exposure_walkway_scenarios_table.tex"
        tex = os.path.join(summary_path, filename)
        summary.to_latex(
            buf=tex,
            caption=(short_cap, long_cap),
            encoding="utf-8",
            float_format="{:0.2f}".format,
        )

        tex_main = os.path.join(summary_path, "main.tex")
        doc = (
            "\\documentclass[10pt,letterpaper]{article}"
            "\\usepackage{booktabs}\\begin{document}"
            "\\input{" + filename + "}"
            "\\end{document}"
        )
        with open(tex_main, "w") as f:
            print(doc, file=f)
