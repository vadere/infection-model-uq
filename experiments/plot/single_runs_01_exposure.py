# Evaluates the following quantities of interest for several scenarios.
# - individual degree of exposure (DOE)
# - max DOE
# - sum DOE
# - mean DOE (here, mean includes only those agents whose DOE is greater than 0)
#
# The close contact scenario is evaluated separately.

import os

from experiments.plot.single_runs import (
    evaluate_close_contact,
    evaluate_doe_rank,
    evaluate_walkway_exposure,
)
from vimuq.experiments.plot.plot import BasicPlot
from experiments.plot.imports import (
    relPath2output,
    thesis,
    ar_default,
)

from matplotlib.pyplot import *


if __name__ == "__main__":
    # ---------------------------------
    # Settings
    # ---------------------------------

    experiment_dir = "single_runs_01"
    experiment_path = os.path.join(relPath2output, experiment_dir)

    # ---------------------------------
    # General plot settings
    # ---------------------------------

    # use same font size as in sensitivity plots
    BasicPlot().apply_vimuq_rcparams(style="vimuq1")
    rcParams["figure.figsize"] = [thesis["fig.oneup"], thesis["fig.oneup"] / ar_default]

    # ---------------------------------
    # Evaluate close contact scenario
    # ---------------------------------

    ids_label_map = {
        0: "Average case (mean sample interval)",
        1: "Extreme case \\textit{a} (maximum aerosol concentration)",
        2: "Extreme case \\textit{b} (maximum spatial aerosol spread)",
    }

    ref_exposures = evaluate_close_contact(
        experiment_path=experiment_path,
        scenario="single_run_close_contact",
        ids_label_map=ids_label_map,
    )

    # ---------------------------------
    # Evaluate queue and waiting area scenarios
    # ---------------------------------

    service_scenarios = [
        "single_run_queue_long",
        "single_run_queue_long_distancing",
        "single_run_waiting_area_line",
        "single_run_waiting_area_line_distancing",
        "single_run_waiting_area_block",
        "single_run_waiting_area_block_distancing",
        "single_run_waiting_area_mushroom",
    ]

    id_xlabel_map = {
        0: "Average case\n(mean sample\ninterval)",
        1: "Extreme case \\textit{a}\n(maximum aerosol\nconcentration)",
        2: "Extreme case \\textit{b}\n(maximum spatial\naerosol spread)",
    }

    evaluate_doe_rank(
        experiment_path=experiment_path,
        scenarios=service_scenarios,
        id_xlabel_map=id_xlabel_map,
        ref=ref_exposures,
        legend_loc=[3],
        average_repetitions_by_rank=True,
    )

    evaluate_doe_rank(
        experiment_path=experiment_path,
        scenarios=service_scenarios,
        id_xlabel_map=id_xlabel_map,
        ref=ref_exposures,
        plot_type="mean",
        mean_threshold=0,
        legend_loc=[3],
    )

    # ---------------------------------
    # Evaluate walkway scenarios
    # ---------------------------------
    walkway_scenarios = [
        "single_run_walkway_wide",
        "single_run_walkway_single_file",
    ]

    evaluate_walkway_exposure(
        experiment_path=experiment_path,
        scenarios=walkway_scenarios,
        ref_exposures=ref_exposures,
    )
