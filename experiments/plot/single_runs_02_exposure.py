# Evaluates the following quantities of interest for several scenarios.
# - individual degree of exposure (DOE)
# - max DOE
# - sum DOE
# - mean DOE (here, mean includes only those agents whose DOE is greater than 0)
#
# The close contact scenario is evaluated separately.

import os
from experiments.plot.imports import (
    relPath2output,
)

from single_runs import (
    evaluate_close_contact,
    evaluate_doe_rank,
    evaluate_walkway_exposure,
)
from vimuq.experiments.plot.plot import BasicPlot

if __name__ == "__main__":
    # ---------------------------------
    # Settings
    # ---------------------------------

    experiment_dir = "single_runs_02"
    experiment_path = os.path.join(relPath2output, experiment_dir)

    # ---------------------------------
    # General plot settings
    # ---------------------------------

    # use same font size as in sensitivity plots
    BasicPlot().apply_vimuq_rcparams(style="vimuq1")

    # ---------------------------------
    # Evaluate close contact scenario
    # ---------------------------------

    ids_label_map = {
        0: "Avg.~sample interval",
        1: "Max.~aerosol concentration",
        2: "Max.~aerosol spread",
    }

    ref_exposures = evaluate_close_contact(
        experiment_path=experiment_path,
        scenario="single_run_close_contact",
        ids_label_map=ids_label_map,
        file_ext="pdf",
    )

    # ---------------------------------
    # Evaluate queue and waiting area scenarios
    # ---------------------------------

    service_scenarios = [
        "single_run_waiting_area_queueing",
        "single_run_waiting_area_mushroom",
        "single_run_queue_long",
        "single_run_queue_long_distancing",
        "single_run_waiting_area_line",
        "single_run_waiting_area_line_distancing",
        "single_run_waiting_area_block",
        "single_run_waiting_area_block_distancing",
    ]

    id_xlabel_map = {
        0: "Avg.~sample\ninterval",
        1: "Max.~aerosol\nconcentration",
        2: "Max.~aerosol\nspread",
    }

    replace_k_by_v = {
        "single_run_": "",
        "waiting_area_queueing": "Self-org.~coop-\nerative queue",
        "waiting_area_mushroom": "Self-org.~com-\npetitive queue\n",
        "queue_long": "Single-file\nqueue\n",
        "waiting_area_line": "Seating in\nline layout\n",
        "waiting_area_block": "Seating in\nblock layout\n",
        "_": " ",
    }

    evaluate_doe_rank(
        experiment_path=experiment_path,
        scenarios=service_scenarios,
        id_xlabel_map=id_xlabel_map,
        id_title_map=replace_k_by_v,
        ref=ref_exposures,
        legend_loc=[4, 9, 14],
        average_repetitions_by_rank=True,
        file_ext="pdf",
    )

    evaluate_doe_rank(
        experiment_path=experiment_path,
        scenarios=service_scenarios,
        id_xlabel_map=id_xlabel_map,
        id_title_map=replace_k_by_v,
        ref=ref_exposures,
        plot_type="mean",
        mean_threshold=0,
        legend_loc=[4],
        average_repetitions_by_rank=True,
        file_ext="pdf",
    )

    # ---------------------------------
    # Evaluate walkway scenarios
    # ---------------------------------
    walkway_scenarios = [
        "single_run_walkway_wide",
        "single_run_walkway_single_file",
    ]

    evaluate_walkway_exposure(
        experiment_path=experiment_path,
        scenarios=walkway_scenarios,
        ref_exposures=ref_exposures,
        file_formats=["txt"],
    )

    # this goes extra because we don't want single_file in the latex table
    evaluate_walkway_exposure(
        experiment_path=experiment_path,
        scenarios=["single_run_walkway_wide"],
        ref_exposures=ref_exposures,
        file_formats=["tex"],
    )
