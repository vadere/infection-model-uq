# Evaluates and plots the distances between the agents in the queue scenarios
# single_run_queue_long and single_run_queue_long_distancing.
# Computation of the mutual distances takes into account several repetitions
# (same parameters but different seeds).
# This evaluation estimates the distances via the method
# - "delta_x": (max x-position - min x-position) / number of agents in the queue
# - "arithmetic": mean arithmetic distance between all agents in the queue

import decimal
import math
import os
import warnings
from itertools import cycle
from imports import ar_wide_hd

import pandas as pd
import pickle as pkl
import matplotlib.pyplot as plt

from experiments.plot.imports import relPath2output
from vimuq.experiments.plot.plot import Plot, BasicPlot
from vimuq.uq.model import Model

from matplotlib.pyplot import *


def get_mean_dist_per_t_step(df, method, rep, t):
    """df is a group, grouped by tStep"""

    peds = df["pedestrianId"].drop_duplicates().values
    n_peds = peds.size
    dist_t = np.nan
    if n_peds > 1:
        if method == "arithmetic":
            dist_t = get_arithmetic_mean(df=df, peds=peds, rep=rep, t=t)
        elif method == "delta_x":
            dist_t = get_mean_x_dist(df=df, peds=peds)

    return dist_t


def get_mean_x_dist(df, peds, x_key="startX-PID1"):
    n_peds = peds.size
    total_dist = abs(df[x_key].max() - df[x_key].min())
    return total_dist / (n_peds - 1)


def get_arithmetic_mean(df, peds, rep, t, x_key="startX-PID1", y_key="startY-PID1"):
    dist_t_step = []
    for ped in peds[:-1]:
        p1 = df[df.pedestrianId == ped]
        p2 = df[df.pedestrianId == ped + 1]
        if (p2.__len__() == 0) | (p2.__len__() == 0):
            warnings.warn(
                f"Repetition {rep}, time step {t}: "
                f"Pedestrian with ID {ped + 1} cannot be found, "
                f"while pedestrian with ID {ped} can. "
                f"I replace the distance with np.nan."
            )
            dist_t_step.append(np.nan)
        else:
            dx_sq = abs(p1[x_key].values[0] - p2[x_key].values[0]) ** 2
            dy_sq = abs(p1[y_key].values[0] - p2[y_key].values[0]) ** 2
            dist_t_step.append(math.sqrt(dx_sq + dy_sq))
        return np.mean(dist_t_step)  # alternatively, use np.nanmean?


if __name__ == "__main__":
    n_repetitions = 10

    scenario1 = {
        "name": "single_run_queue_long",
        "legend": "Normal",
        "expected_dist": 0.9,
    }
    scenario2 = {
        "name": "single_run_queue_long_distancing",
        "legend": "Distancing",
        "expected_dist": 1.5,
    }
    scenarios = [scenario1, scenario2]

    dists_scenarios = pd.DataFrame()

    # ---------------------------------
    # General plot settings
    # ---------------------------------
    lines = ["--", "-.", ":"]
    line_cycler = cycle(lines)
    colors = ["tab:blue", "tab:orange"]
    col_cycler = cycle(colors)
    col_cycler2 = cycle(colors)
    t1_ = []
    t2_ = []

    BasicPlot().apply_vimuq_rcparams(style="vimuq1")

    y_ticks = np.array([0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5])

    page_width = 3.48642  # inches (8,85553 cm)
    fig, ax = plt.subplots(figsize=[page_width, page_width / ar_wide_hd])
    fig.subplots_adjust(left=0.15, bottom=0.2, right=0.95, top=0.95)

    method = "delta_x"  # "arithmetic"
    filename = "queue_distances_" + method + ".pkl"
    path_to_experiment_out = os.path.join(
        relPath2output, "single_runs_queue_distancing_calibration_01"
    )
    path_to_data = os.path.join(path_to_experiment_out, filename)

    if os.path.exists(path_to_data):
        with open(path_to_data, "rb") as inp:
            dists_scenarios = pkl.load(inp)
    else:
        for scenario in scenarios:
            data = pd.DataFrame()
            mean_rep_time_dist = []

            dists_reps = pd.DataFrame()
            for rep in range(n_repetitions):
                t_queue_start = None

                output_path = os.path.join(
                    path_to_experiment_out,
                    scenario["name"],
                    Model.OUTPUT_SUBDIR,
                    "vadere_output",
                    "0_" + str(rep) + "_output",
                )
                file = "postvis.traj"

                file_path = os.path.join(output_path, file)
                data = pd.read_csv(file_path, header=1, sep=" ")

                # determine time step width delta_t in which the results are evaluated
                groups = data.groupby("pedestrianId")
                maxs_delta_t = []
                for _, group in groups:
                    group.sort_values("simTime", inplace=True)
                    sim_times = group["simTime"].drop_duplicates().to_numpy()
                    max_delta_t = (sim_times[1:] - sim_times[0:-1]).min()
                    maxs_delta_t.append(max_delta_t)
                # delta_t should be somewhere between 0.1 and 1; better not greater than dt
                # choose not too many digits after the decimal (<2 recommended)
                dt = np.min(maxs_delta_t)
                delta_t = 0.5

                digits = abs(decimal.Decimal(str(delta_t)).as_tuple().exponent)
                if (delta_t % 1) == 0:
                    digits = 0

                t_start = math.floor(data.simTime.values.min())
                t_stop = data.simTime.values.max()
                n_t_steps = int(math.ceil((t_stop - t_start) / delta_t))
                t_stop = t_start + n_t_steps * delta_t
                # this yields exact numbers for t_steps (linspace does not)
                t_steps = np.array(range(n_t_steps + 1)) * delta_t + t_start

                # Create a data frame data_ that contains equidistant time steps and maps
                # column simTime to tStep. Depending on the value of delta_t, it may occur
                # that we get more tStep values than actually recorded time steps. In that
                # case, we simply copy the most recent value (row) to the subsequent
                # artificially introduced tStep(s).
                data.insert(loc=1, column="tStep", value=np.nan)
                data["tStep"] = (
                    data["simTime"].divide(delta_t).round(digits - 1).multiply(delta_t)
                )
                t_steps = t_steps.round(digits)
                groups = data.groupby(["pedestrianId"])
                data_ = pd.DataFrame
                for _, group in groups:
                    t_steps_per_ped = group.tStep.drop_duplicates().values.tolist()
                    fill_t_steps = np.setdiff1d(t_steps, t_steps_per_ped)
                    fill_t_steps = fill_t_steps[
                        fill_t_steps >= group.tStep.drop_duplicates().min()
                    ]
                    fill_t_steps = fill_t_steps[
                        fill_t_steps < group.tStep.drop_duplicates().max()
                    ]
                    missing_data = pd.DataFrame({"tStep": fill_t_steps})
                    group = pd.concat([group, missing_data.copy()])
                    group.sort_values("tStep", ignore_index=True, inplace=True)
                    group.fillna(method="ffill", inplace=True)
                    if data_.empty:
                        data_ = group.copy()
                    else:
                        data_ = pd.concat([data_, group.copy()])
                data_.sort_values(
                    by=["tStep", "pedestrianId"], ignore_index=True, inplace=True
                )
                data_ = data_[data_.tStep.isin(t_steps)]

                # Calculate distances
                dists = pd.DataFrame
                groups = data_.groupby(["tStep"])
                dist = pd.DataFrame
                n = 0
                for time_step, group in groups:
                    # log process
                    if n in (np.linspace(0, 1, 5) * groups.ngroups).round(0):
                        print(
                            f"repetition {rep}: {round(n / groups.ngroups * 100, 0)}%"
                        )
                    n += 1

                    dist_t_step = get_mean_dist_per_t_step(
                        df=group, method=method, rep=rep, t=time_step
                    )
                    n_peds = group["pedestrianId"].drop_duplicates().values.size
                    # if n_peds > 1:
                    #     dist_t_step = abs(group["startX-PID1"].max() - group["startX-PID1"].min()) / (n_peds - 1)
                    # else:
                    #     dist_t_step = np.nan
                    dist_df = pd.DataFrame({"tStep": time_step, "dist": [dist_t_step]})
                    if dist.empty:
                        dist = dist_df
                    else:
                        dist = pd.concat([dist, dist_df.copy()])

                    # determine time step at which all peds are standing in line
                    condition1 = (
                        group["pedestrianId"].drop_duplicates().values.max() == 21
                    )
                    total_dist = abs(
                        group["startX-PID1"].max() - group["startX-PID1"].min()
                    )
                    buffer_dist = 2
                    total_expected_dist = (
                        scenario["expected_dist"] * n_peds + buffer_dist
                    )
                    condition2 = total_dist < total_expected_dist
                    if condition1 and condition2 and t_queue_start is None:
                        t_queue_start = time_step

                # dist.insert(loc=1, column="method", value=method)
                if dists.empty:
                    dists = dist.copy()
                else:
                    dists = pd.concat([dists, dist.copy()])

                # get all the spawn and leaving times
                groups = data_.groupby(["pedestrianId"])
                t_in = []
                t_out = []
                for _, group in groups:
                    t_in.append(group["tStep"].min())
                    t_out.append(group["tStep"].max())

                # time interval where we have a queue
                # manual definition of the time when all peds actually queue
                t1 = t_queue_start
                t2 = t_out[-2]

                # calculate mean dist for current repetition; take only values into
                # account where queue is fully developed, i.e. t \in [t1, t2]
                dists.reset_index(drop=True, inplace=True)
                mean_idx1 = dists[dists.tStep == t1].index.values[0]
                mean_idx2 = dists[dists.tStep == t2].index.values[0] + 1
                mean_dists = dists.iloc[mean_idx1:mean_idx2]["dist"].mean()

                dists["distPlot"] = dists["dist"]
                dists.distPlot.iloc[0:mean_idx1] = np.nan
                dists.distPlot.iloc[mean_idx2 + 1 :] = np.nan

                dists.insert(loc=0, column="meanTimeDist", value=np.nan)
                dists.meanTimeDist.iloc[mean_idx1:mean_idx2] = mean_dists
                dists.insert(loc=0, column="t1", value=t1)
                dists.insert(loc=0, column="t2", value=t2)
                dists.insert(loc=0, column="repetition", value=rep)

                if dists_reps.empty:
                    dists_reps = dists.copy()
                else:
                    dists_reps = pd.concat([dists_reps, dists.copy()])

            # calculate mean distance over all repetitions for current scenario
            mean_rep_time_dist = dists_reps.meanTimeDist.drop_duplicates().mean(
                skipna=True
            )
            dists_reps.insert(loc=0, column="meanRepTimeDist", value=mean_rep_time_dist)
            dists_reps.insert(loc=0, column="scenario", value=scenario["name"])

            if dists_scenarios.empty:
                dists_scenarios = dists_reps.copy()
            else:
                dists_scenarios = pd.concat([dists_scenarios, dists_reps.copy()])

        with open(path_to_data, "wb") as outp:
            pkl.dump(dists_scenarios, outp, pkl.HIGHEST_PROTOCOL)

    # plot
    log_msg = (
        "Mean over all repetitions per scenario and over time steps"
        "/ time interval in which the agents actually queue up:\n"
    )
    scenario_groups = dists_scenarios.groupby(["scenario"])
    for s_name, s_group in scenario_groups:
        log_msg += s_name + ": "
        repetition_groups = s_group.groupby(["repetition"])

        c1 = next(col_cycler)
        c2 = next(col_cycler2)

        if "distancing" in s_name:
            s_label = "Distancing"
        else:
            s_label = "Normal"

        # plot individual repetitions
        for r_name, r_group in repetition_groups:
            if r_name == 1:
                label = s_label + " (repetitions $r=10$)"
            else:
                label = "__nolegend__"
            ax = r_group.plot(
                ax=ax,
                x="tStep",
                y="distPlot",
                color=c1,
                lw=0.4,
                ls="-",
                alpha=0.5,
                label=label,
            )

        # Plot time series of mean (mean over all repetitions)
        # since ds = s_group.groupby("tStep", as_index=True)["distPlot"]
        #   .mean(skipna=False)
        # does not work properly, we make a short workaround:
        ts_group = s_group.groupby("tStep")
        ds = pd.DataFrame()
        for t, grp in ts_group:
            ds_ = pd.DataFrame(
                [{"tStep": t, "meanRepDist": grp["distPlot"].mean(skipna=False)}]
            )
            ds = pd.concat([ds, ds_])
        # end of workaround

        ax = ds.plot(
            ax=ax,
            x="tStep",
            y="meanRepDist",
            color=c1,
            lw=1.25,
            ls="-",
            label=s_label + " mean",
        )

        # plot mean of mean time series
        ax.plot(
            [
                s_group.t1.drop_duplicates().values.max(),
                s_group.t2.drop_duplicates().values.min(),
            ],
            [
                s_group.meanRepTimeDist.drop_duplicates().values[0],
                s_group.meanRepTimeDist.drop_duplicates().values[0],
            ],
            lw=0.75,
            ls="--",
            color="k",
            label="__nolegend__",  # s_label + " mean (repetitions, time)",
        )
        log_msg += str(s_group.meanRepTimeDist.drop_duplicates().values[0]) + "\n\n"

    ax.set_yticks(y_ticks)
    x_delta = 200
    x1 = math.floor(dists_scenarios.t1.values.min() / x_delta) * x_delta
    x2 = math.ceil(dists_scenarios.t2.values.max() / x_delta) * x_delta
    x_ticks = list(range(x1, x2, x_delta))
    ax.set_xlim([x1, 2100])
    ax.set_xticks(x_ticks)
    ax.set_ylim([min(y_ticks), max(y_ticks)])
    plt.xlabel("Time in s")
    plt.ylabel("Distance in m")
    plt.legend(loc="upper left")

    path2dir = os.path.join(path_to_experiment_out, Plot._FIG_DIR)
    if not os.path.exists(path2dir):
        os.makedirs(path2dir)
    path2file = os.path.join(path2dir, "queue_distances_" + method + ".pdf")
    plt.savefig(path2file)

    # write mean distances to log file
    info_file = os.path.join(path2dir, "queue_distances_" + method + "_values.txt")
    with open(info_file, "w") as f:
        print(log_msg, file=f)
