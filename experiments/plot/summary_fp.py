# Plots a summary of the forward propagation results for the experiments defined
# by experiment_ids.
# TODO Refactoring / rewrite this draft but use classes etc.

import numpy as np
import pandas as pd
from scipy import stats

from vimuq.experiments.plot.plot import ForwardUQPlot


def get_default_if_not_in_keys(key, dictionary, default=None):
    value = default
    if key in dictionary.keys():
        value = dictionary[key]
    return value


def get_kernel(x, properties, num=500):
    kernel = stats.gaussian_kde(dataset=x)

    # get x points from xlim
    x_lower = x.min()
    x_upper = x.max()
    if "x_axis" in properties.keys():
        if "lim" in properties["x_axis"].keys():
            x_lower = properties["x_axis"]["lim"][0]
            x_upper = properties["x_axis"]["lim"][1]
    xx = np.linspace(x_lower, x_upper, num=num)

    # estimate y data with kde
    return xx, kernel.evaluate(xx)


def fill_values(x, y, fill_x, fill_y=np.nan, sort=True):
    x = np.append(x, fill_x)
    if sort:
        x = np.sort(x)

    index = np.where(x == fill_x)[0][0]
    y = np.insert(arr=y, obj=index, values=fill_y)
    return x, y


def get_marker_props(marker, line_color):
    if isinstance(marker, list):
        style = marker[0]
        face = marker[1]
        edge = marker[2]
    else:
        style = marker
        face = line_color
        edge = "none"

    return style, face, edge


def get_summary_plot(
    plot,
    props_subplots,
    data_frame,
    data_series,
    ref_id,
    lw_by_runs=0.1,
    marker_size=3,
    marker_edge_width=0.5,
    exp_key="experiment",
    average_key="avrg",
    label_key="label",
    marker_key="marker",
    color_key="color",
    alpha_edge=0.3,
    alpha_face=0.25,
):
    if np.size(plot.figure.axes) > 1:
        subplot_iter = enumerate(plot.axes.flatten())
    else:
        subplot_iter = enumerate([plot.axes])
    for subplot_idx, ax in subplot_iter:
        p = props_subplots[subplot_idx]

        data = data_frame.copy()
        data = data[data.index.isin(values=[p["qoi"]], level="qoi")]

        where_summary_data = data.index.isin(values=[np.nan], level="id")
        # plot based on summary statistics data
        if p["type"] in ["line", "bar"]:
            data = data[where_summary_data]
        # plot based on simulation data
        elif p["type"] in ["hist", "kde"]:
            data = data[~where_summary_data]

            # ToDo apply equivalent to averagerepetitions for all avrg == "B"
            # average uq results before uq methods are applied
            # in most cases, "B" does not make sense (Use A, C, D instead)
        else:
            UserWarning(f"Cannot select data. Specify 'type'.")

        if "x" in p.keys():
            if p["x"] not in data.columns:
                data.reset_index(p["x"], inplace=True)
        if "y" in p.keys():
            if p["y"] not in data.columns:
                data.reset_index(p["y"], inplace=True)

        by_label = data.groupby([label_key])
        # I do not use by_label.plot(...) because then I cannot specify the
        # the legend entries / I cannot be sure in which order the lines are
        # created.
        target_groups = [k for k, v in data_series.items()]

        # reference group / this will be treated differently
        if ref_id != None:
            ref_group_id = ref_id  # remove "Close contact"
            ref_group = data_series[ref_group_id][label_key]
            target_groups.remove(ref_group_id)

        # optional: filter data / consider only selected experiments
        if "experiments" in p.keys():
            target_groups = [
                k for k, v in data_series.items() if v[exp_key] in p["experiments"]
            ]

        for i, target_group in enumerate(target_groups):
            name = data_series[target_group][label_key]
            group = by_label.get_group(name)
            x = None
            y = None
            # get x and y data
            if "x" in p.keys():
                x = group[p["x"]]
            if "y" in p.keys():
                y = group[p["y"]]

            # plot other settings
            c = group[color_key].values.any()

            if p["type"] == "bar":
                total_width = 0.8
                bar_width = total_width / by_label.ngroups
                x_shift = (by_label.ngroups / 2 - 0.5) * total_width
                x_shift = x_shift + i * bar_width
                x = x - x_shift

                # add error range / CI / min max range
                if "y_err" in p.keys():
                    y_err = np.array(
                        [group[p["y_err"][0]].values, group[p["y_err"][1]].values]
                    )
                    ax.bar(
                        x=x, height=y, width=bar_width, label=name, color=c, yerr=y_err
                    )
                else:
                    ax.bar(x=x, height=y, width=bar_width, label=name, color=c)

            elif p["type"] == "line":
                plot_by_run = False
                if "run_id" in x.index.names:
                    if (
                        x.index.get_level_values(level="run_id")
                        .drop_duplicates()
                        .shape[0]
                        > 1
                    ):
                        plot_by_run = True
                if plot_by_run:
                    x_ = x.groupby(["run_id"])
                    y_ = y.groupby(["run_id"])
                    for n in x_.groups.keys():
                        gx = x_.get_group(n)
                        gy = y_.get_group(n)
                        if not n == 0:
                            name = "__nolegend__"
                        if "fill_x" in p.keys():
                            gx, gy = fill_values(
                                x=gx.values, y=gy.values, fill_x=p["fill_x"]
                            )
                        if marker_key in data_series[target_group].keys():
                            style, face, edge = get_marker_props(
                                marker=data_series[target_group][marker_key],
                                line_color=c,
                            )
                        else:
                            style = None
                            face = "none"
                            edge = "none"
                        ax.plot(
                            gx,
                            gy,
                            label=name,
                            color=c,
                            lw=lw_by_runs,
                            marker=style,
                            markersize=marker_size,
                            markeredgewidth=marker_edge_width,
                            markerfacecolor=face,
                            markeredgecolor=edge,
                        )

                else:
                    x_copy = x.copy()
                    if "fill_x" in p.keys():
                        x, y = fill_values(
                            x=x_copy.values, y=y.values, fill_x=p["fill_x"]
                        )
                    if marker_key in data_series[target_group].keys():
                        style, face, edge = get_marker_props(
                            marker=data_series[target_group][marker_key], line_color=c
                        )
                    else:
                        style = None
                        face = "none"
                        edge = "none"

                    ax.plot(
                        x,
                        y,
                        label=name,
                        color=c,
                        marker=style,
                        markersize=marker_size,
                        markeredgewidth=marker_edge_width,
                        markerfacecolor=face,
                        markeredgecolor=edge,
                    )

                    # add error range / CI / min max range
                    if "y_err" in p.keys():
                        y1 = group[p["y_err"][0]]
                        y2 = group[p["y_err"][1]]
                        if "fill_x" in p.keys():
                            _, y1 = fill_values(
                                x=x_copy.values, y=y1.values, fill_x=p["fill_x"]
                            )
                            _, y2 = fill_values(
                                x=x_copy.values, y=y2.values, fill_x=p["fill_x"]
                            )

                        facecolor = list(c)
                        facecolor.append(alpha_face)
                        edgecolor = list(c)
                        edgecolor.append(alpha_edge)
                        ax.fill_between(
                            x=x,
                            y1=y1,
                            y2=y2,
                            facecolor=facecolor,
                            edgecolor=edgecolor,
                            lw=0.1,
                            label="__nolegend__",
                        )

            elif p["type"] == "hist":
                bins = get_default_if_not_in_keys(key="bins", dictionary=p)
                if group[average_key].drop_duplicates().values == "C":
                    indices = []
                    for i in x.index.names:
                        idx_vals = (
                            x.index.get_level_values(level=i).drop_duplicates().tolist()
                        )
                        if pd.isna(idx_vals).all():  # if all index values are nan
                            # do nothing
                            indices
                        else:
                            indices.append(i)
                    indices.remove("run_id")  # because we want to average over this
                    x_ = x.groupby(indices).mean()
                    ax.hist(
                        x_,
                        label=name,  # + " (mean)",
                        color=c,
                        bins=bins,
                        cumulative=get_default_if_not_in_keys(
                            key="cumulative", dictionary=p, default=False
                        ),
                        histtype=get_default_if_not_in_keys(
                            key="style", dictionary=p, default="bar"
                        ),
                    )
                elif group[average_key].drop_duplicates().values == "D":
                    for n, x_ in x.groupby(["run_id"]):
                        if not n == 0:
                            name = "__nolegend__"
                        ax.hist(
                            x_,
                            label=name,
                            color=c,
                            bins=bins,
                            cumulative=get_default_if_not_in_keys(
                                key="cumulative", dictionary=p, default=False
                            ),
                            histtype=get_default_if_not_in_keys(
                                key="style", dictionary=p, default="bar"
                            ),
                        )
                else:
                    ax.hist(
                        x,
                        label=name,
                        color=c,
                        bins=bins,
                        cumulative=get_default_if_not_in_keys(
                            key="cumulative", dictionary=p, default=False
                        ),
                        histtype=get_default_if_not_in_keys(
                            key="style", dictionary=p, default="bar"
                        ),
                    )

            elif p["type"] == "kde":
                line_width = None
                if group[average_key].drop_duplicates().values in ["C", "D"]:
                    x_ = x.groupby(["run_id"])
                    y_kde_estimate = pd.DataFrame()
                    for n, g in x_:
                        xx, temp = get_kernel(g, p)
                        temp = pd.DataFrame(
                            {
                                "kde": temp,
                                "run_id": np.ones(temp.shape, dtype=int) * int(n),
                                "idx": np.arange(temp.size),
                            }
                        )
                        y_kde_estimate = pd.concat([y_kde_estimate, temp])
                    if group[average_key].drop_duplicates().values == "C":
                        y_kde_estimate = y_kde_estimate.groupby(["idx"]).mean()
                        name = name  # + " (mean)"
                    else:
                        y_kde_estimate = y_kde_estimate.set_index(
                            ["run_id", "idx"]
                        ).unstack(level="run_id")
                        name = name + f" ($r={str(x_.ngroups)}$)"
                        name = [name] + ["__nolegend__"] * (x_.ngroups - 1)
                        line_width = lw_by_runs
                    y_kde_estimate = y_kde_estimate["kde"].to_numpy()
                else:
                    xx, y_kde_estimate = get_kernel(x, p)

                # calculate cumulative kde / cdf
                if get_default_if_not_in_keys(
                    key="cumulative", dictionary=p, default=False
                ):
                    y_kde_estimate = y_kde_estimate.cumsum()

                if marker_key in data_series[target_group].keys():
                    style, face, edge = get_marker_props(
                        marker=data_series[target_group][marker_key], line_color=c
                    )
                else:
                    style = None
                    face = "none"
                    edge = "none"

                ax.plot(
                    xx,
                    y_kde_estimate,
                    label=name,
                    color=c,
                    lw=line_width,
                    marker=style,
                    markersize=marker_size,
                    markeredgewidth=marker_edge_width,
                    markerfacecolor=face,
                    markeredgecolor=edge,
                    markevery=50,
                )

        # add reference values from close contact scenario
        if ref_id != None:
            ref = data_frame[
                data_frame.index.isin(values=[p["qoi"]], level="qoi")
            ].copy()
            ref = ref[where_summary_data]
            ref_by_label = ref.groupby([label_key])
            group = ref_by_label.get_group(ref_group)
            if p["qoi"] in ["pedIdKey_degExposure_rank", "pedIdKey_degExposure"]:
                # ax.plot(
                #     p["x_axis"]["lim"],
                #     [group[("degreeOfExposure-PID8", "mean")]] * 2,
                #     color=group[color_key].values.any(),
                #     ls=":",
                #     lw=0.7,
                # )
                ax.plot(
                    p["x_axis"]["lim"],
                    [group[("degreeOfExposure-PID8", "50%")]] * 2,
                    color=group[color_key].values.any(),
                    ls=":",
                    lw=0.7,
                    zorder=0,
                )
            if p["qoi"] in [
                "pedIdKey_degExposure_max",
                "pedIdKey_degExposure_mean_>_0",
            ]:
                # ax.plot(
                #     [group[("degreeOfExposure-PID8", "mean")]] * 2,
                #     p["y_axis"]["lim"],
                #     color=group[color_key].values.any(),
                #     ls=":",
                #     lw=0.7,
                # )
                ax.plot(
                    [group[("degreeOfExposure-PID8", "50%")]] * 2,
                    p["y_axis"]["lim"],
                    color=group[color_key].values.any(),
                    ls=":",
                    lw=0.7,
                    zorder=0,
                )

        if "legend" in p.keys():
            ldg = ax.legend(loc=p["legend"])
            for h in ldg.legendHandles:
                h._markersize = marker_size
        ForwardUQPlot.set_axis_properties(ax=ax, properties=p, axis="x_axis")
        ForwardUQPlot.set_axis_properties(ax=ax, properties=p, axis="y_axis")

    return plot
