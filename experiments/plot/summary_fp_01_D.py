# Plots a summary of the forward propagation results for the experiments defined
# by experiment_ids.
# TODO Refactoring / rewrite this draft but use classes etc.
import string

import matplotlib
import numpy as np
import pandas as pd

import vimuq
from experiments.plot.imports import ar_wide, ar_default, qoi_str
from vimuq.experiments.plot.plot import PlotDataFrame, BasicPlot
from vimuq.uq.uq_experiment import UQExperiment, Stage
from summary_fp import get_summary_plot

import matplotlib.pyplot as plt
from matplotlib import *
from matplotlib.collections import PolyCollection

if __name__ == "__main__":
    path2output = os.path.join("..", "output")

    exp_key = "experiment"
    label_key = "label"
    color_key = "color"
    average_key = "avrg"
    marker_key = "marker"

    # color range (opacity 0 ... 1)
    opac = [0.95, 0.35]
    colors = [
        matplotlib.colors.to_rgb(x)
        for x in matplotlib.cm.GnBu(np.linspace(opac[0], opac[1], 4))
    ]
    # colors = matplotlib.cm.get_cmap("tab10").colors
    # colors = [matplotlib.cm.get_cmap('tab20c').colors[x] for x in range(0,16,4)]
    [print(matplotlib.colors.to_hex(c)) for c in colors]
    # markers
    marker_size = 3

    data_series = {
        0: {
            exp_key: "waiting_area_mushroom_uq_01_02",
            label_key: "Self-org.~competitive queue ($r=10$)",
            color_key: colors[0],
            average_key: "D",  # either C or D; Don't choose A here because the
            # results per pedestrian can vary greatly from repetition to
            # repetition (for each seed).
            # B often does not make sense depending on the qoi.
            marker_key: ["s", colors[0], colors[0]],
        },
        1: {
            exp_key: "queue_long_uq_03_04",
            label_key: "Single-file queue ($r=3$)",
            color_key: colors[1],
            average_key: "D",  # either A or C (should not make large
            # difference here because the simulation results per pedestrian
            # should be relatively independent of the seed)
            marker_key: ["D", colors[1], colors[1]],
        },
    }

    # ---------------------------------
    # Aggregate data
    # ---------------------------------
    uq = pd.DataFrame()
    sim = pd.DataFrame()

    for _, v in data_series.items():
        path2dumped_experiment = vimuq.get_experiment_dir_name(
            dir_name=os.path.join(path2output, v[exp_key]), suffix=""
        )

        exp = UQExperiment.read_experiment(
            path2dumped_experiment,
            stage=Stage.POST_ANALYSIS,
            avrg_technique=v[average_key],
        )

        # collect summary data
        temp = PlotDataFrame.uq_output_dict_to_df(uq_output_dict=exp.uq_output)
        temp = PlotDataFrame.add_index(df=temp, index_name=exp_key, value=v[exp_key])
        temp.insert(loc=0, column=label_key, value=v[label_key])
        temp.insert(loc=0, column=color_key, value=[v[color_key]] * temp.shape[0])
        temp.insert(loc=0, column=average_key, value=np.nan)
        uq = PlotDataFrame.concat_unequal_dataframes(uq, temp)

        # collect simulation data (required for histograms, KDE, etc.)
        for qoi, df in exp.sim_output.items():
            required_qois = [
                "pedIdKey_degExposure_rank",
                "pedIdKey_degExposure",
            ]
            if qoi in required_qois:
                df_ = df.copy()
                df_.insert(loc=0, column="qoi", value=qoi)
                df_.insert(loc=0, column=exp_key, value=v[exp_key])
                df_.insert(loc=0, column=label_key, value=v[label_key])
                df_.insert(loc=0, column=color_key, value=[v[color_key]] * df_.shape[0])
                df_.insert(loc=0, column=average_key, value=v[average_key])
                sim = PlotDataFrame.concat_unequal_dataframes(sim, df_)
    sim.set_index(keys=["qoi", exp_key], append=True, inplace=True)
    sim.columns = pd.MultiIndex.from_arrays([sim.columns, ["", "", "", "sim_output"]])

    uq.set_index(keys=[label_key, color_key], append=True, inplace=True)
    sim.set_index(keys=[label_key, color_key], append=True, inplace=True)
    uq_, sim_ = PlotDataFrame.harmonize_indices(uq, sim, value=pd.NA)

    exp_df = pd.concat([uq_, sim_])
    exp_df.reset_index(level=[label_key, color_key], inplace=True)

    by_experiment = exp_df.groupby([exp_key])

    # ---------------------------------
    # Summary plot
    # ---------------------------------
    # 1) Line plot of rank(-Exposure per agent) vs. exposure
    p_rank = {
        "qoi": "pedIdKey_degExposure_rank",
        "type": "line",
        "x": "rank",
        "y": ("degreeOfExposure-PID8", "50%"),  # ("degreeOfExposure-PID8", "mean"),
        "y_err": [("degreeOfExposure-PID8", "25%"), ("degreeOfExposure-PID8", "75%")],
        "x_axis": {
            "label": qoi_str["rank2"],
            "lim": [1, 21],
        },
        "y_axis": {
            "label": "Exposure",
            "tick_format": ["sci", (0, 0)],
            "lim": [0, 3000],
        },
        "legend": "upper right",
        "experiments": [data_series[0][exp_key], data_series[1][exp_key]],
    }

    p_pos = {
        "qoi": "pedIdKey_degExposure",
        "type": "line",
        "x": "pedestrianId",
        "y": ("degreeOfExposure-PID8", "50%"),  # ("degreeOfExposure-PID8", "mean"),
        "y_err": [("degreeOfExposure-PID8", "25%"), ("degreeOfExposure-PID8", "75%")],
        "x_axis": {
            "label": qoi_str["pos2"],
            "lim": [1, 21],
        },
        "y_axis": {
            "label": "Exposure",
            "tick_format": ["sci", (0, 0)],
            "lim": [0, 3000],
        },
        "legend": "upper right",
        "experiments": [data_series[1][exp_key]],
        "fill_x": 11,
    }

    # Define the order of the subplots:
    p_subplots = {
        0: p_rank,
        # 1: p_pos,
    }

    page_width = 3.48761
    # page_width = 7.22433
    p1 = BasicPlot()
    p1.figure, p1.axes = plt.subplots(
        nrows=1,
        ncols=1,
        figsize=[
            page_width,
            page_width / ar_default,
        ],
    )

    p1 = get_summary_plot(
        plot=p1,
        data_frame=exp_df,
        data_series=data_series,
        props_subplots=p_subplots,
        marker_size=marker_size,
        ref_id=None,
        lw_by_runs=0.5,
    )

    p1.figure.subplots_adjust(
        left=0.14,
        bottom=0.17,
        right=0.95,
        top=0.92,
    )

    # for n, ax in enumerate(p1.figure.get_axes()):
    #     ax.text(-0.175, 1.075, string.ascii_lowercase[n] + ")", transform=ax.transAxes)

    p1.figure.show()

    # Save results
    filename = os.path.basename(__file__)
    path = os.path.join(path2output, filename.split("_D")[0])
    if not os.path.exists(path):
        os.makedirs(path, exist_ok=False)

    p1.save(parent_dir=path, name=filename.split(".")[0] + "_kde")
