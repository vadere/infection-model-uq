import os

import pandas as pd

import vimuq
from vimuq.experiments.plot.plot import PlotDataFrame
from vimuq.uq.uq_experiment import UQExperiment, Stage


def aggregate_data(path2output, experiments, avrg_key):

    df = pd.DataFrame()
    for experiment_id, settings in experiments.items():
        path2dumped_experiment = vimuq.get_experiment_dir_name(
            dir_name=os.path.join(path2output, experiment_id), suffix=""
        )

        experiment = UQExperiment.read_experiment(
            path2dumped_experiment,
            stage=Stage.POST_ANALYSIS,
            suffix="",
            avrg_technique=settings[avrg_key],
        )

        temp = PlotDataFrame.uq_output_dict_to_df(uq_output_dict=experiment.uq_output)
        for k, v in settings.items():
            temp = PlotDataFrame.add_index(df=temp, index_name=k, value=v)

        df = PlotDataFrame.concat_unequal_dataframes(df, temp)

    return df


def make_output_dir(path2output, filename):
    path = os.path.join(path2output, filename.split(".")[0])
    if not os.path.exists(path):
        os.makedirs(path, exist_ok=False)
    return path


def rename_parameters(df):
    # replace parameter values and set column "param" to index
    parameter_abbr = {
        "halfLife": "Half-life",  # "Half life",
        "initialRadius": "Init.~radius",  # "Init. radius",
        "airDispersionFactor": "Disp.~factor",  # "Air disp. factor",
    }
    for key, value in parameter_abbr.items():
        df.replace({"param": key}, value, inplace=True)
