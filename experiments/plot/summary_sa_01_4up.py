import os

from experiments.plot.summary_sa import (
    aggregate_data,
    make_output_dir,
    rename_parameters,
)
from vimuq.experiments.plot.plot import (
    SensitivityPlot,
    Plot,
    MarkerDict,
    SubplotDescriptions,
    LegendDict,
)

from experiments.plot.imports import qoi_str

import matplotlib.pyplot as plt
from matplotlib.pyplot import *
from matplotlib.container import BarContainer

if __name__ == "__main__":
    path = os.path.join("..", "output")
    filename = "summary_sa_01"  # os.path.basename(__file__)
    path2summary = make_output_dir(path2output=path, filename=filename)

    file_ext = "pdf"

    plots = [
        "id",
        "rank",
        # "max",
        "mean",
        "counts",
    ]

    avrg_key = "avrg"
    scenario_key = "scenario"
    index_key = "index"
    legend_key = "legend"

    experiments = {
        "waiting_area_mushroom_uq_01_03": {
            avrg_key: "C",
            scenario_key: "Self-org.~competitive queue",  # ($r=10$)",
            legend_key: "Self-org.~competitive queue",  # ($r=10$)",
            index_key: 0,
        },
        "queue_long_uq_03_03": {
            avrg_key: "A",
            scenario_key: "Single-file queue",  # ($r=3$)",
            legend_key: "Single-file queue",  # ($r=3$)",
            index_key: 1,
        },
        "waiting_area_line_uq_01_02": {
            avrg_key: "A",
            scenario_key: "Seating, line layout",
            legend_key: "Seating, line layout",
            index_key: 2,
        },
        "waiting_area_block_uq_01_01": {
            avrg_key: "A",
            scenario_key: "Seating, block layout",
            legend_key: "Seating, block layout",
            index_key: 3,
        },
    }

    df = aggregate_data(path2output=path, experiments=experiments, avrg_key=avrg_key)
    rename_parameters(df)
    df.set_index(keys="param", drop=True, append=True, inplace=True)

    # color range (opacity 0 ... 1)
    opac = [0.4, 1]
    colors = [
        matplotlib.colors.to_rgb(x)
        for x in matplotlib.cm.Blues(
            np.linspace(opac[0], opac[1], experiments.items().__len__())
        )
    ]
    # colors = matplotlib.cm.get_cmap("tab10").colors
    [print(f"{c[0]*255}, {c[1]*255}, {c[2]*255}") for c in colors]

    # markers
    markers = [MarkerDict(marker=m, size=2) for m in ["s", "D", "o", "^"]]

    # ---------------------------------
    # Create plot - results for different scenarios
    # ---------------------------------
    series_level = scenario_key  # the level/column in df that we iterate
    # through when plotting several curves per subplot

    sens_indices = [
        ("ST", "Total sensitivity index", "st"),
        ("S1", "$1\\textsuperscript{st}$ order sensitivity index", "s1"),
    ]

    for idx in sens_indices:
        d_base = SubplotDescriptions(yticks=[0, 1], ylim=[0, 1], ylabel=idx[1])
        sensitivity_plot = Plot(
            data=df,
            title=None,
            n_subplot_cols=2,
        )

        # Define the legend
        ldg = {v[scenario_key]: v[legend_key] for v in experiments.values()}
        ldg = LegendDict(entrydict=ldg)

        filename_str = ""

        if "rank" in plots:
            scenarios_rank = [
                "waiting_area_mushroom_uq_01_03",
                "waiting_area_block_uq_01_01",
            ]
            val_sel = [experiments[s][scenario_key] for s in scenarios_rank]
            val_sel_idx = [
                v["index"] for v in experiments.values() if v[scenario_key] in val_sel
            ]
            ldg2 = {v: v for v in val_sel}

            d = d_base.copy()
            d.update(title=qoi_str["rank1"])
            filename_str += "_rank"
            sensitivity_plot.add_subplot(
                SensitivityPlot(
                    data_col=idx[0],
                    main_value="pedIdKey_degExposure_rank",
                    type="line",
                    idx_minor_x="rank",
                    idx_data_series=series_level,
                    data_series_value_selection=val_sel,
                    colors_data_series=[colors[i] for i in val_sel_idx],
                    descriptions=d,
                    fill_minor_x_vals=[21],
                    marker=[markers[i] for i in val_sel_idx],
                    show_xticks="minor",
                    legend=LegendDict(entrydict=ldg2, location="upper left"),
                    plot_conf_interval=True,
                    xticks_width=0.85,
                )
            )
        if "id" in plots:
            scenarios_id = ["queue_long_uq_03_03", "waiting_area_line_uq_01_02"]
            val_sel = [experiments[s][scenario_key] for s in scenarios_id]
            val_sel_idx = [
                v["index"] for v in experiments.values() if v[scenario_key] in val_sel
            ]
            ldg2 = {v: v for v in val_sel}

            d = d_base.copy()
            d.update(title=qoi_str["pos1"])
            filename_str += "_id"
            sensitivity_plot.add_subplot(
                SensitivityPlot(
                    data_col=idx[0],
                    main_value="pedIdKey_degExposure",
                    type="line",
                    idx_minor_x="pedestrianId",
                    idx_data_series=series_level,
                    data_series_value_selection=val_sel,
                    colors_data_series=[colors[i] for i in val_sel_idx],
                    descriptions=d,
                    fill_minor_x_vals=[11],
                    marker=[markers[i] for i in val_sel_idx],
                    show_xticks="minor",
                    legend=LegendDict(entrydict=ldg2, location="upper left"),
                    plot_conf_interval=True,
                    xticks_width=0.85,
                )
            )

        if "max" in plots:
            d = d_base.copy()
            d.update(title=qoi_str["max"])
            filename_str += "_max"
            sensitivity_plot.add_subplot(
                SensitivityPlot(
                    data_col=idx[0],
                    main_value="pedIdKey_degExposure_max",
                    type="bar",
                    idx_data_series=series_level,
                    colors_data_series=colors,
                    descriptions=d,
                    plot_conf_interval=True,
                    xticks_width=0.85,
                )
            )

        if "mean" in plots:
            d = d_base.copy()
            d.update(title=qoi_str["mean"])
            filename_str += "_mean0"
            sensitivity_plot.add_subplot(
                SensitivityPlot(
                    data_col=idx[0],
                    main_value="pedIdKey_degExposure_mean_>_0",
                    type="bar",
                    idx_data_series=series_level,
                    colors_data_series=colors,
                    descriptions=d,
                    plot_conf_interval=True,
                    xticks_width=0.85,
                )
            )

        if "counts" in plots:
            d = d_base.copy()
            d.update(title=qoi_str["counts"])
            filename_str += "_count0"
            sensitivity_plot.add_subplot(
                SensitivityPlot(
                    data_col=idx[0],
                    main_value="pedIdKey_degExposure_count_>_0",
                    type="bar",
                    idx_data_series=series_level,
                    colors_data_series=colors,
                    descriptions=d,
                    plot_conf_interval=True,
                    xticks_width=0.85,
                )
            )

        page_width = 5.19685  # inches (13.2 cm)
        page_width = page_width + 0.3937  # plus 1 cm
        fig_size = [
            page_width,
            4.8,  # inches
        ]

        sensitivity_plot.make_plot(figsize=fig_size)

        # Add legend outside of subplots
        ax = sensitivity_plot.figure.get_axes()[3]
        bars = [i for i in ax.containers if isinstance(i, BarContainer)]

        labels = ldg["entrydict"].values()
        bars = bars[: len(labels)]
        handles = [b.patches[0] for b in bars]

        # ax.legend(handles=handles, labels=labels, bbox_to_anchor=(1, -0.2), ncol=len(labels), columnspacing=2)
        figlegend = False
        if figlegend:
            plt.figlegend(
                handles, labels, loc="lower center", ncol=len(labels), columnspacing=2
            )

        # annotations
        ax = sensitivity_plot.figure.get_axes()[0]
        ax.tick_params(which="both", direction="out")
        ax.annotate(
            text="Pedestrian with\nmax.~exposure",
            xy=(ax.get_xticks(minor=True)[0], 0),
            xytext=(ax.get_xticks(minor=True)[0] + 0.1, 0.25),
            va="center",
            ha="left",
            arrowprops=dict(
                arrowstyle="->",
                connectionstyle="arc3",
                shrinkA=0,
                shrinkB=2,
                lw=0.75,
                relpos=(0, 0.5),
            ),
            fontsize="small",
        )

        ax = sensitivity_plot.figure.get_axes()[1]
        ax.tick_params(which="both", direction="out")
        ax.annotate(
            text="Pedestrian at\nfirst position",
            xy=(ax.get_xticks(minor=True)[0], 0),
            xytext=(ax.get_xticks(minor=True)[0] + 0.1, 0.25),
            va="center",
            ha="left",
            arrowprops=dict(
                arrowstyle="->",
                connectionstyle="arc3",
                shrinkA=0,
                shrinkB=2,
                lw=0.75,
                relpos=(0, 0.5),
            ),
            fontsize="small",
        )

        # sensitivity_plot.save(
        #     parent_dir=path2summary,
        #     name=f"{idx[2]}_by_scenario{filename_str}",
        #     file_extension=file_ext,
        # )

        # modified sensitivity plot
        subplot_map = {
            0: {
                "xlabel": qoi_str["rank3"],
                "show_minor_x": True,
                "n_data_series": len(scenarios_rank),
                "n_xticks": 21,
            },
            1: {
                "xlabel": qoi_str["pos2"],
                "show_minor_x": True,
                "n_data_series": len(scenarios_id),
                "n_xticks": 21,
            },
        }
        sensitivity_plot.shift_xlabels_to_top(subplot_map=subplot_map)
        if figlegend:
            sensitivity_plot.adjust_margins_for_big_legend()
        else:
            sensitivity_plot.figure.subplots_adjust(
                left=0.075, bottom=0.02, right=0.96, top=0.9, hspace=0.7, wspace=0.25
            )

        sensitivity_plot.save(
            parent_dir=path2summary,
            name=f"{idx[2]}_by_scenario{filename_str}",
            file_extension=file_ext,
        )
