import os

from experiments.plot.summary_sa import (
    make_output_dir,
    aggregate_data,
    rename_parameters,
)
from vimuq.experiments.plot.plot import (
    SensitivityPlot,
    Plot,
    MarkerDict,
    SubplotDescriptions,
    LegendDict,
)

from experiments.plot.imports import ar_default, qoi_str

from matplotlib.pyplot import *
from matplotlib.container import BarContainer


if __name__ == "__main__":
    path = os.path.join("..", "output")
    filename = os.path.basename(__file__)
    path2summary = make_output_dir(path2output=path, filename=filename)

    file_ext = "pdf"

    plots = [
        "id",
        # "rank",
        # "max",
        "mean",
        # "counts",
    ]

    avrg_key = "avrg"
    scenario_key = "scenario"
    index_key = "index"
    legend_key = "legend"

    experiments = {
        "queue_long_uq_03_03": {
            avrg_key: "A",
            scenario_key: "half-life $\sim \mathcal{U}(600,\,10^4)\,$s",  # ($r=3$)",
            legend_key: "half-life $\sim \mathcal{U}(600,\,10^4)\,$s",  # ($r=3$)",
            index_key: 0,
        },
        "queue_long_uq_03_05": {
            avrg_key: "A",
            scenario_key: "$\log_{10}($half-life$)$ $\sim \mathcal{U}(2.78,\,4)\,$s",
            legend_key: "$\log_{10}($half-life$)$ $\sim \mathcal{U}(2.78,\,4)\,$s",
            index_key: 1,
        },
    }

    df = aggregate_data(path2output=path, experiments=experiments, avrg_key=avrg_key)
    rename_parameters(df)
    df.set_index(keys="param", drop=True, append=True, inplace=True)

    # color range (opacity 0 ... 1)
    opac = [0.4, 1]
    colors = [
        matplotlib.colors.to_rgb(x)
        for x in matplotlib.cm.Blues(
            np.linspace(opac[0], opac[1], experiments.items().__len__())
        )
    ]
    # colors = matplotlib.cm.get_cmap("tab10").colors
    [print(f"{c[0]*255}, {c[1]*255}, {c[2]*255}") for c in colors]

    # markers
    markers = [MarkerDict(marker=m, size=2) for m in ["s", "D"]]  # ,"o","^"]]

    # ---------------------------------
    # Create plot - results for different scenarios
    # ---------------------------------
    series_level = scenario_key  # the level/column in df that we iterate
    # through when plotting several curves per subplot

    sens_indices = [
        ("ST", "Total sensitivity index", "st"),
        ("S1", "$1\\textsuperscript{st}$ order sensitivity index", "s1"),
    ]

    for idx in sens_indices:
        d_base = SubplotDescriptions(yticks=[0, 1], ylim=[0, 1], ylabel=idx[1])
        sensitivity_plot = Plot(
            data=df,
            title=None,
            n_subplot_cols=2,
        )

        # Define the legend
        ldg = {v[scenario_key]: v[legend_key] for v in experiments.values()}
        ldg = LegendDict(entrydict=ldg)

        filename_str = ""

        if "id" in plots:
            scenarios = ["queue_long_uq_03_03", "queue_long_uq_03_05"]
            val_sel = [experiments[s][scenario_key] for s in scenarios]
            val_sel_idx = [
                v["index"] for v in experiments.values() if v[scenario_key] in val_sel
            ]
            ldg2 = {v: v for v in val_sel}

            d = d_base.copy()
            d.update(title=qoi_str["pos"])
            filename_str += "_id"
            sensitivity_plot.add_subplot(
                SensitivityPlot(
                    data_col=idx[0],
                    main_value="pedIdKey_degExposure",
                    type="line",
                    idx_minor_x="pedestrianId",
                    idx_data_series=series_level,
                    data_series_value_selection=val_sel,
                    colors_data_series=[colors[i] for i in val_sel_idx],
                    descriptions=d,
                    fill_minor_x_vals=[11],
                    marker=[markers[i] for i in val_sel_idx],
                    show_xticks="minor",
                    legend=ldg,
                    plot_conf_interval=True,
                )
            )

        if "max" in plots:
            d = d_base.copy()
            d.update(title=qoi_str["max"])
            filename_str += "_max"
            sensitivity_plot.add_subplot(
                SensitivityPlot(
                    data_col=idx[0],
                    main_value="pedIdKey_degExposure_max",
                    type="bar",
                    idx_data_series=series_level,
                    colors_data_series=colors,
                    descriptions=d,
                    plot_conf_interval=True,
                )
            )

        if "mean" in plots:
            d = d_base.copy()
            d.update(title=qoi_str["mean"])
            # (E_{p,\,0}), E_{p,\,0} = \{x \in E_{ped}:\, x > 0\}$"
            filename_str += "_mean0"
            sensitivity_plot.add_subplot(
                SensitivityPlot(
                    data_col=idx[0],
                    main_value="pedIdKey_degExposure_mean_>_0",
                    type="bar",
                    idx_data_series=series_level,
                    colors_data_series=colors,
                    descriptions=d,
                    legend=ldg,
                    plot_conf_interval=True,
                )
            )

        if "counts" in plots:
            d = d_base.copy()
            d.update(title=qoi_str["counts"])
            filename_str += "_count0"
            sensitivity_plot.add_subplot(
                SensitivityPlot(
                    data_col=idx[0],
                    main_value="pedIdKey_degExposure_count_>_0",
                    type="bar",
                    idx_data_series=series_level,
                    colors_data_series=colors,
                    descriptions=d,
                    plot_conf_interval=True,
                )
            )

        fig_size = [
            rcParams["figure.figsize"][0],
            rcParams["figure.figsize"][0] / ar_default,
        ]

        sensitivity_plot.make_plot(figsize=[fig_size[0], fig_size[1] / 2.25])

        # Add legend outside of subplots
        sensitivity_plot.figure.subplots_adjust(
            bottom=0.1, top=0.9, wspace=0.225, hspace=0.41, right=0.97, left=0.06
        )
        ax = sensitivity_plot.figure.get_axes()[1]
        bars = [i for i in ax.containers if isinstance(i, BarContainer)]

        labels = ldg["entrydict"].values()
        bars = bars[: len(labels)]
        handles = [b.patches[0] for b in bars]

        ax = sensitivity_plot.figure.get_axes()[0]
        ax.tick_params(which="both", direction="out")
        ax.annotate(
            text="Pedestrian at\nfirst position",
            xy=(ax.get_xticks(minor=True)[0], 0),
            xytext=(ax.get_xticks(minor=True)[0] + 0.1, 0.25),
            va="center",
            ha="left",
            arrowprops=dict(
                arrowstyle="->",
                connectionstyle="arc3",
                shrinkA=0,
                shrinkB=2,
                lw=0.75,
                relpos=(0, 0.5),
            ),
            fontsize="small",
        )
        # modified sensitivity plot
        subplot_map = {
            0: {
                "xlabel": qoi_str["rank3"],
                "show_minor_x": True,
                "n_data_series": 2,
                "n_xticks": 21,
            }
        }
        sensitivity_plot.shift_xlabels_to_top(subplot_map=subplot_map)

        sensitivity_plot.save(
            parent_dir=path2summary,
            name=f"{idx[2]}_by_scenario{filename_str}",
            file_extension=file_ext,
            dpi=600,
        )
