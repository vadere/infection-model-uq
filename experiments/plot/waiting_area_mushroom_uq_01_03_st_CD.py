# Plots total sensitivity indices (SI)
# - for averaged SI (A)
# - (currently not considered, but possible: for non averaged (D), runs r=0,1,2)

import os
import string

from matplotlib.pyplot import *
from experiments.plot.imports import ar_wide, qoi_str, ar_default

import vimuq
from vimuq.experiments.plot.plot import (
    PlotDataFrame,
    SubplotDescriptions,
    Plot,
    SensitivityPlot,
    MarkerDict,
)
from vimuq.uq.uq_experiment import UQExperiment, Stage


if __name__ == "__main__":
    # ---------------------------------
    # Experiment
    # ---------------------------------
    path2output = os.path.join("..", "output")
    experiment_id = "waiting_area_mushroom_uq_01_03"
    avrgs = [
        # "A",  # option "A" would only make sense (if at all) for quantities
        # that consider all agents but not individuals
        "C",
        "D",
    ]

    file_ext = "pdf"

    # sub titles
    sub_titles = [
        "",  # qoi_str["rank"],
        "",  # qoi_str["max"],
        "",  # qoi_str["mean"],
        "",  # qoi_str["counts"],
    ]

    for avrg in avrgs:
        # ---------------------------------
        # Aggregate data
        # ---------------------------------
        path2dumped_experiment = vimuq.get_experiment_dir_name(
            dir_name=os.path.join(path2output, experiment_id), suffix=""
        )

        exp = UQExperiment.read_experiment(
            path2dumped_experiment, stage=Stage.POST_ANALYSIS, avrg_technique=avrg
        )

        exp_df = PlotDataFrame.uq_output_dict_to_df(uq_output_dict=exp.uq_output)

        # replace parameter values and set column "param" to index
        parameter_abbr = {
            "halfLife": "Half-life",  # "Half life",
            "initialRadius": "Init.~radius",  # "Init. radius",
            "airDispersionFactor": "Disp.~factor",  # "Air disp. factor",
        }
        for key, value in parameter_abbr.items():
            exp_df.replace({"param": key}, value, inplace=True)

        exp_df.set_index(keys="param", drop=True, append=True, inplace=True)

        d_base = SubplotDescriptions(yticks=[0, 1], ylim=[0, 1], ylabel="$S_{T}$")

        # ---------------------------------
        # Create plot
        # ---------------------------------
        sensitivity_plot = Plot(
            data=exp_df,
            # title="Degree of exposure",
            n_subplot_cols=2,
        )

        # Define index and color for one/multiple data sets in one subplot
        if "run_id" in sensitivity_plot.data.index.names:
            lvl = "run_id"  # for avrg D
            n_colors = (
                sensitivity_plot.data.index.get_level_values("run_id")
                .drop_duplicates()
                .size
            )
            # colors = [
            #     matplotlib.colors.to_rgb(x)
            #     for x in matplotlib.cm.Blues(np.linspace(0.4, 1, n_colors))
            # ]

            # three times the same color is on purpose, color matches with color
            # code for the competitive queue / mushroom scenario in
            # summary_sa_01.py
            colors = [matplotlib.colors.to_rgb(matplotlib.cm.GnBu(0.95))] * n_colors

        else:
            lvl = None  # for avrg A
            colors = matplotlib.cm.get_cmap("tab10").colors

        # Define subplots
        d = d_base.copy()
        d.update(title=sub_titles[0])
        sensitivity_plot.add_subplot(
            SensitivityPlot(
                main_value="pedIdKey_degExposure_rank",
                type="line",
                idx_minor_x="rank",
                idx_data_series=lvl,
                colors_data_series=colors,
                descriptions=d,
                fill_minor_x_vals=[21],
                marker=MarkerDict(),
                show_xticks="minor",
                xticks_width=0.85,
            )
        )

        d = d_base.copy()
        d.update(title=sub_titles[1])
        sensitivity_plot.add_subplot(
            SensitivityPlot(
                main_value="pedIdKey_degExposure_max",
                type="bar",
                idx_data_series=lvl,
                colors_data_series=colors,
                descriptions=d,
                xticks_width=0.85,
            )
        )

        d = d_base.copy()
        d.update(title=sub_titles[2])
        sensitivity_plot.add_subplot(
            SensitivityPlot(
                main_value="pedIdKey_degExposure_mean_>_0",
                type="bar",
                idx_data_series=lvl,
                colors_data_series=colors,
                descriptions=d,
                xticks_width=0.85,
            )
        )

        d = d_base.copy()
        d.update(title=sub_titles[3])
        sensitivity_plot.add_subplot(
            SensitivityPlot(
                main_value="pedIdKey_degExposure_count_>_0",
                type="bar",
                idx_data_series=lvl,
                colors_data_series=colors,
                descriptions=d,
                xticks_width=0.85,
            )
        )

        sensitivity_plot.make_plot(
            figsize=[
                rcParams["figure.figsize"][0],
                rcParams["figure.figsize"][0] / ar_wide,
            ]
        )

        ax = sensitivity_plot.figure.get_axes()[0]
        ax.tick_params(which="both", direction="out")

        sensitivity_plot.figure.subplots_adjust(
            bottom=0.05, top=0.9, wspace=0.25, hspace=0.65, right=0.98, left=0.06
        )

        # modified sensitivity plot
        if avrg == "C":
            n_data_series = 1
        if avrg == "D":
            n_data_series = 10

        subplot_map = {
            0: {
                "xlabel": qoi_str["rank3"],
                "show_minor_x": True,
                "n_data_series": n_data_series,
                "n_xticks": 21,
            }
        }
        sensitivity_plot.shift_xlabels_to_top(subplot_map=subplot_map)

        axes = sensitivity_plot.figure.get_axes()
        for ax in axes:
            # set y-legend
            ax.set_ylabel("Total sensitivity index")

        # add items a), b), c), d)
        for n, ax in enumerate(sensitivity_plot.figure.get_axes()):
            ax.text(
                -0.125, 1.15, string.ascii_lowercase[n] + ")", transform=ax.transAxes
            )

        sensitivity_plot.save(
            parent_dir=path2dumped_experiment,
            name="st_degree_of_exposure_" + avrg,
            file_extension=file_ext,
        )

        # calculate min max ranges obtained for each qoi for repeated runs
        if avrg == "D":

            groups = exp_df.groupby(by="qoi")

            # exposure by position in the queue
            qoi = "pedIdKey_degExposure_rank"
            g = groups.get_group(qoi)
            g = g[["ST"]]
            g.reset_index(["pedestrianId", "qoi"], drop=True, inplace=True)

            min = g.groupby(["param", "rank"]).min()
            max = g.groupby(["param", "rank"]).max()
            mean = g.groupby(["param", "rank"]).mean()

            diff = max.subtract(min, axis="index", level=["param", "rank"])
            diff.rename(columns={"ST": "max-min"}, inplace=True)

            diff["mean-min"] = mean.subtract(min, axis="index", level=["param"])
            diff["max-mean"] = max.subtract(mean, axis="index", level=["param"])

            repetitions = g.index.get_level_values("run_id").drop_duplicates().__len__()

            print("\n\n---------------------------------------------------")
            print(f"Max difference in {str(repetitions)} repetitions of {qoi}:")
            print("---------------------------------------------------")
            print(diff.groupby(by="param").max())
            #                     ST
            # param
            # Disp.~factor  0.612247 --> large difference, so I check whether this is an exception:
            # Half-life     0.052880
            # Init.~radius  0.720687 --> large difference, so I check whether this is an exception:
            print(
                diff.groupby("param")
                .get_group("Disp.~factor")
                .sort_values("max-min", ascending=False)
                .dropna()
            )
            print(
                diff.groupby("param")
                .get_group("Init.~radius")
                .sort_values("max-min", ascending=False)
                .dropna()
            )

            # maximum exposure, average exposure, e > 0, number of exposures, e > 0
            for qoi in [
                "pedIdKey_degExposure_max",
                "pedIdKey_degExposure_mean_>_0",
                "pedIdKey_degExposure_count_>_0",
            ]:
                g = groups.get_group(qoi)
                g = g[["ST"]]
                g.reset_index(["rank", "qoi", "pedestrianId"], drop=True, inplace=True)
                repetitions = (
                    g.index.get_level_values("run_id").drop_duplicates().__len__()
                )
                min = g.groupby(["param"]).min()
                max = g.groupby(["param"]).max()

                diff = max.subtract(min, axis="index", level=["param"])
                print("\n\n---------------------------------------------------")
                print(f"Max difference in {str(repetitions)} repetitions of {qoi}:")
                print("---------------------------------------------------")
                print(diff.groupby(by="param").max())
