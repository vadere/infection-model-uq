# Plots three different plots to evaluate the convergence of the total
# sensitivity indices for each simulation run, r=0,1,2 (averaging technique D).
# - plot 1: results for each sample size n (n=128, ..., 4096)
# - plot 2: results for sample size n_{i+1} subtracted from the results for
#           sample size n_{i} (absolute value)
# - plot 3: results for sample size n_{i+1} subtracted from the results for
#           sample size n=4096 (absolute value)

import os
import pandas as pd
import math as m

from matplotlib.pyplot import *

import vimuq
from vimuq.experiments.plot.plot import (
    PlotDataFrame,
    SubplotDescriptions,
    Plot,
    SensitivityPlot,
    LegendDict,
    MarkerDict,
)
from vimuq.uq.uq_experiment import UQExperiment, Stage


if __name__ == "__main__":
    # ---------------------------------
    # Experiment
    # ---------------------------------
    path2output = os.path.join("..", "output")
    experiment_id = "ishigami_sa_01"
    avrg = "D"
    n_samples = [
        "16",
        "32",
        "64",
        "128",
        "256",
        "512",
        "1024",
        "2048",
        "4096",
    ]

    # red, horizontal line (stopping criterion for convergence)
    xstop = [-0.5, 2.5]
    ystop = [0.01, 0.01]

    # color range (opacity 0 ... 1)
    col_range = [0.4, 1]

    file_ext = "pdf"

    # ---------------------------------
    # Aggregate data
    # ---------------------------------
    agg_level = "n_samples"
    path2dumped_experiment = vimuq.get_experiment_dir_name(
        dir_name=os.path.join(path2output, experiment_id), suffix=""
    )
    exp_df = pd.DataFrame
    for a in n_samples:
        exp = UQExperiment.read_experiment(
            path2dumped_experiment,
            stage=Stage.POST_ANALYSIS,
            suffix="",
            n_samples=a,
            avrg_technique=avrg,
        )

        temp = PlotDataFrame.uq_output_dict_to_df(uq_output_dict=exp.uq_output)
        temp = PlotDataFrame.add_index(df=temp, index_name=agg_level, value=a)
        exp_df = PlotDataFrame.concat_unequal_dataframes(exp_df, temp)

    exp_df.set_index(keys="param", drop=True, append=True, inplace=True)

    # ---------------------------------
    # Calculate measure for convergence (Difference previous dataset)
    # ---------------------------------
    conv_groupby = "n_samples"
    conv_idx = "difference"
    exp_g = exp_df.groupby(by=conv_groupby)
    conv1 = pd.DataFrame
    conv2 = pd.DataFrame
    n_ref = n_samples[-1]
    n_ref_log2 = int(m.log2(int(n_ref)))
    ref = exp_g.get_group(n_ref).reset_index(level=conv_groupby, drop=True).copy()
    for n in range(len(n_samples) - 1):
        temp1 = (
            exp_g.get_group(n_samples[n])
            .reset_index(level=conv_groupby, drop=True)
            .copy()
        )
        temp2 = (
            exp_g.get_group(n_samples[n + 1])
            .reset_index(level=conv_groupby, drop=True)
            .copy()
        )

        # calc convergence measure 1
        diff1 = temp1.subtract(temp2).abs()

        diff1.insert(loc=0, column=conv_idx, value=f"conv_{n_samples[n]}")
        diff1.set_index(conv_idx, append=True, drop=True, inplace=True)

        conv1 = PlotDataFrame.concat_unequal_dataframes(conv1, diff1)

        # calc convergence measure 2
        diff2 = temp1.subtract(ref).abs()

        diff2.insert(loc=0, column=conv_idx, value=f"conv_{n_samples[n]}")
        diff2.set_index(conv_idx, append=True, drop=True, inplace=True)

        conv2 = PlotDataFrame.concat_unequal_dataframes(conv2, diff2)

    # ---------------------------------
    # Create plot - results for various sample sizes
    # ---------------------------------
    plot_level = "n_samples"
    n_level_vals = exp_df.index.get_level_values(plot_level).drop_duplicates().__len__()

    d_base = SubplotDescriptions(yticks=[0, 1], ylim=[0, 1], ylabel="$S_{T}$")

    sensitivity_plot = Plot(
        data=exp_df,
        title="Total sensitivity for different sample sizes $n$, $n_{skip}=2^{"
        + str(n_ref_log2)
        + "}$",
        n_subplot_cols=1,
    )

    # Legend
    n_samples_log2 = [int(m.log2(int(i))) for i in n_samples]
    n_samples_dict = dict(zip(n_samples, n_samples_log2))
    legend = LegendDict(
        title="Sample size $n = 2^a$, where a:",
        ncols=n_level_vals,
        entrydict=n_samples_dict,
    )

    colors = [
        matplotlib.colors.to_rgb(x)
        for x in matplotlib.cm.Blues(
            np.linspace(col_range[0], col_range[1], n_level_vals)
        )
    ]

    # Define subplots
    d = d_base.copy()
    # d.update(title="")
    sensitivity_plot.add_subplot(
        SensitivityPlot(
            main_value="Y",
            type="bar",
            idx_data_series=plot_level,
            colors_data_series=colors,
            descriptions=d,
            legend=legend,
        )
    )

    sensitivity_plot.make_plot()

    sensitivity_plot.figure.show()

    sensitivity_plot.save(
        parent_dir=path2dumped_experiment,
        name="st_convergence_1_" + avrg,
        file_extension=file_ext,
    )

    # ---------------------------------
    # Create plot - convergence 1
    # ---------------------------------
    plot_level = conv_idx
    n_level_vals = conv1.index.get_level_values(plot_level).drop_duplicates().__len__()

    d_base = SubplotDescriptions(
        yticks=[0, ystop[0], 0.1],
        ylim=[0, 0.1],
        ylabel="$\mid S_{T}(n_1) - S_{T}(n_2) \mid$",
    )

    sensitivity_plot = Plot(
        data=conv1,
        title="Difference of $S_{T}$, where $n_1=2^a$, $n_2=2^{a+1}$, $n_{skip}=2^{"
        + str(n_ref_log2)
        + "}$",
        n_subplot_cols=1,
    )

    # Legend
    ldg = conv1.index.get_level_values(level=conv_idx).drop_duplicates().to_list()
    ldg_log2 = [int(m.log2(int(i.split(sep="_")[1]))) for i in ldg]
    ldg_dict = dict(zip(ldg, ldg_log2))
    legend = LegendDict(
        title="Sample size $n_1=2^a$, where a:",
        ncols=n_level_vals,
        entrydict=ldg_dict,
    )

    colors = [
        matplotlib.colors.to_rgb(x)
        for x in matplotlib.cm.Blues(
            np.linspace(col_range[0], col_range[1], n_level_vals)
        )
    ]

    # Define subplots
    d = d_base.copy()
    # d.update(title="")
    sensitivity_plot.add_subplot(
        SensitivityPlot(
            main_value="Y",
            type="bar",
            idx_data_series=plot_level,
            colors_data_series=colors,
            descriptions=d,
            legend=legend,
        )
    )

    sensitivity_plot.make_plot()

    fig2 = sensitivity_plot.get_fig()
    for ax in fig2.axes:
        ax.plot(xstop, ystop, color="r", lw=0.5, ls="--")

    sensitivity_plot.figure.show()

    sensitivity_plot.save(
        parent_dir=path2dumped_experiment,
        name="st_convergence_2_" + avrg,
        file_extension=file_ext,
    )

    # ---------------------------------
    # Create plot - convergence 3
    # ---------------------------------
    plot_level = conv_idx
    n_level_vals = conv2.index.get_level_values(plot_level).drop_duplicates().__len__()

    d_base = SubplotDescriptions(
        yticks=[
            0,
            ystop[0],
            0.1,
        ],
        ylim=[0, 0.1],
        ylabel="$\mid S_{T}(n_1) - S_{T}(n_2) \mid$",
    )

    sensitivity_plot = Plot(
        data=conv2,
        title="Difference of $S_{T}$, where $n_1=2^a$, $n_2=2^{"
        + str(n_ref_log2)
        + "}$, $n_{skip}=2^{"
        + str(n_ref_log2)
        + "}$",
        n_subplot_cols=1,
    )

    # Legend
    ldg = conv2.index.get_level_values(level=conv_idx).drop_duplicates().to_list()
    ldg_log2 = [int(m.log2(int(i.split(sep="_")[1]))) for i in ldg]
    ldg_dict = dict(zip(ldg, ldg_log2))
    legend = LegendDict(
        title="Sample size $n_1=2^a$, where a:",
        ncols=n_level_vals,
        entrydict=ldg_dict,
    )

    colors = [
        matplotlib.colors.to_rgb(x)
        for x in matplotlib.cm.Blues(
            np.linspace(col_range[0], col_range[1], n_level_vals)
        )
    ]

    # Define subplots
    d = d_base.copy()
    # d.update(title="")
    sensitivity_plot.add_subplot(
        SensitivityPlot(
            main_value="Y",
            type="bar",
            idx_data_series=plot_level,
            colors_data_series=colors,
            descriptions=d,
            legend=legend,
        )
    )

    sensitivity_plot.make_plot()

    fig3 = sensitivity_plot.get_fig()
    for ax in fig3.axes:
        ax.plot(xstop, ystop, color="r", lw=0.5, ls="--")

    sensitivity_plot.figure.show()

    sensitivity_plot.save(
        parent_dir=path2dumped_experiment,
        name="st_convergence_3_" + avrg,
        file_extension=file_ext,
    )
