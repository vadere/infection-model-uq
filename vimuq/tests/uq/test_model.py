import shutil
import unittest
import os

from suqc import PostScenarioChangesBase

from vimuq.tests import _get_head_path
from vimuq.uq.model import Model, VadereModel, PyModel
from vimuq.uq.parameter_space import ParameterSpace


class TestModel(unittest.TestCase):
    def test__check_path_exists_true(self):
        path = os.path.join(".", "exist")
        os.makedirs(path, exist_ok=False)
        self.assertTrue(Model._check_path_exists(path))
        shutil.rmtree(path)

    def test__check_path_exists_throws_error(self):
        path = os.path.join(".", "not", "exist")
        self.assertRaises(FileNotFoundError, Model._check_path_exists, path=path)


class TestVadereModel(unittest.TestCase):
    def setUp(self):
        cwd = os.getcwd()
        path2project = _get_head_path(cwd, "infection-model-uq")
        path2tests_vadere = os.path.join(
            path2project, "vimuq", "tests", "model", "vadere"
        )

        self._path2scenario = os.path.join(
            path2tests_vadere, "scenarios", "test_queue_uq_fast.scenario"
        )
        self._path2model = os.path.join(
            path2tests_vadere, "simulator", "test_vadere-console.txt"
        )

    def test___init___rng(self):
        """Check if two model instances get the 'same' random number generator
        in the sense that the resulting random numbers are the same.
        """

        model1 = VadereModel(
            parameter_space=ParameterSpace(),
            quantities_of_interest=["qoi1"],
            path2model=self._path2model,
            path2scenario=self._path2scenario,
        )
        model2 = VadereModel(
            parameter_space=ParameterSpace(),
            quantities_of_interest=["qoi2"],
            path2model=self._path2model,
            path2scenario=self._path2scenario,
        )

        self.assertEqual(model1.rng.__getstate__(), model2.rng.__getstate__())

    def test__set_repetitions_seeds(self):
        repetitions = 10**5
        model = VadereModel(
            parameter_space=ParameterSpace(),
            quantities_of_interest=["qoi2"],
            path2model=self._path2model,
            path2scenario=self._path2scenario,
            repetitions=repetitions,
        )
        model._set_repetitions_seeds()
        min_signed_int32 = -2147483648
        max_signed_int32 = 2147483647

        self.assertEqual(len(model.repetitions_seeds), repetitions)
        self.assertTrue(min(model.repetitions_seeds) > min_signed_int32)
        self.assertTrue(max(model.repetitions_seeds) < max_signed_int32)
        self.assertTrue(all(isinstance(n, int) for n in model.repetitions_seeds))

    def test_apply_suqc_rand_number_per_repetition_change(self):
        repetitions = 10
        model = VadereModel(
            parameter_space=ParameterSpace(),
            quantities_of_interest=["qoi2"],
            path2model=self._path2model,
            path2scenario=self._path2scenario,
            repetitions=repetitions,
        )
        model.apply_suqc_rand_number_per_repetition()
        scenario_changes_keys = model.suqc_post_changes._apply_scenario_changes.keys()
        rnd_numbers = model.suqc_post_changes._apply_scenario_changes[
            "random_number"
        ]._fixed_randnr

        self.assertTrue("random_number" in scenario_changes_keys)
        self.assertEqual(len(rnd_numbers), repetitions)

    def test_apply_suqc_rand_number_per_repetition_rng(self):
        repetitions1 = 10
        repetitions2 = repetitions1 + 100

        model1 = VadereModel(
            parameter_space=ParameterSpace(),
            quantities_of_interest=["qoi2"],
            path2model=self._path2model,
            path2scenario=self._path2scenario,
            repetitions=repetitions1,
        )
        model1.apply_suqc_rand_number_per_repetition()
        rnd1 = model1.suqc_post_changes._apply_scenario_changes[
            "random_number"
        ]._fixed_randnr

        model2 = VadereModel(
            parameter_space=ParameterSpace(),
            quantities_of_interest=["qoi2"],
            path2model=self._path2model,
            path2scenario=self._path2scenario,
            repetitions=repetitions2,
        )
        model2.apply_suqc_rand_number_per_repetition()
        rnd2 = model2.suqc_post_changes._apply_scenario_changes[
            "random_number"
        ]._fixed_randnr

        self.assertEqual(rnd1[0:repetitions1], rnd2[0:repetitions1])

    def test__init__post_scenario_change(self):
        expected = PostScenarioChangesBase(apply_default=True)
        model1 = VadereModel(
            parameter_space=ParameterSpace(),
            quantities_of_interest=["qoi"],
            path2model=self._path2model,
            path2scenario=self._path2scenario,
        )
        model1.apply_suqc_rand_number_per_repetition()

        model2 = VadereModel(
            parameter_space=ParameterSpace(),
            quantities_of_interest=["qoi"],
            path2model=self._path2model,
            path2scenario=self._path2scenario,
        )

        self.assertEqual(
            model2.suqc_post_changes._apply_scenario_changes.keys(),
            expected._apply_scenario_changes.keys(),
        )


class TestPyModel(unittest.TestCase):
    def setUp(self):
        cwd = os.getcwd()
        path2project = _get_head_path(cwd, "infection-model-uq")
        path2tests = os.path.join(path2project, "vimuq", "tests")

        self._path = os.path.join(path2tests, "model", "pymodel", "ishigami.py")

    def test_convert_os_path_to_module(self):
        module_str = PyModel._convert_os_path_to_module(self._path)
        expected = "vimuq.tests.model.pymodel.ishigami"

        self.assertEqual(module_str, expected)

    def test_evaluate_out_format(self):
        dummy_p_space = ParameterSpace()
        qoi = "test_qoi"

        model = PyModel(dummy_p_space, [qoi], self._path)

        samples = [
            {"x1": i * 0.314, "x2": i * 0.314, "x3": i * 0.314} for i in range(-10, 11)
        ]

        _, out = model.evaluate(samples)
        check_sum = out[qoi].to_numpy().sum()

        self.assertTrue("id" in out[qoi].index.names)
        self.assertEqual(out[qoi].to_numpy().shape, (len(samples), 1))
        self.assertAlmostEqual(check_sum, 70.03434808713871, places=8)


if __name__ == "__main__":
    unittest.main()
